/** 
 * @brief It implements the game interface and all the associated callbacks
 * for each command
 * 
 * @file game.c
 * @author Profesores PPROG, Group 1
 * @version 3.0
 * @date 5-04-2018 
 * @copyright GNU Public License
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/game.h"
#include "../include/game_management.h"

/*It defines the number of callbacks to (unkwon, exit, following, previous, take, drop, roll, right, left, move, check, save, load, use, turnon, turnoff, etc)*/
#define N_CALLBACK 25

/*It defines the type Game as a struct. */
struct _Game {
    Player* player_location;
    Object* object_location[MAX_OBJ];
    Space* spaces[MAX_SPACES + 1];
    Link* links[MAX_LINKS];
    Die* die;
    Command* last_cmd;
    Rule* last_rule;
    STATUS callback_status;
    STATUS preavious_status;
    char description[WORD_SIZE + 1];
};

/*
   Define the function type for the callbacks
 */
typedef STATUS(*callback_fn)(Game* game);

/**
   List of callbacks for each command in the game 
 */

/*It receives a Game*, 
returns OK if receives Game* or ERROR if not
@author Profesores PPROG, updated by Nicolás Serrano */
STATUS game_callback_unknown(Game* game);

/*It receives a Game*, 
returns OK if receives Game* or ERROR if not	
@author Profesores PPROG, updated by Nicolás Serrano */
STATUS game_callback_exit(Game* game);

/*It receives a Game*, 
sets player location at south of actual location, returns OK if everything worked correctly or ERROR if not		
@author Profesores PPROG, updated by Nicolás Serrano */
STATUS game_callback_following(Game* game);

/*It receives a Game*, 
sets player location at north of actual location, returns OK if everything worked correctly or ERROR if not	 
@author Profesores PPROG, updated by Nicolás Serrano */
STATUS game_callback_previous(Game* game);

/*It receives a Game*, 
if the player any object is at the same location as player saves the object ID and object location is errased	
@author Miguel Luque and Nicolás Serrano */
STATUS game_callback_take(Game* game);

/*It receives a Game*, 
if player has an object, sets the object at the same location as the player and errases object ID saved in player	
@author Miguel Luque and Nicolás Serrano */
STATUS game_callback_drop(Game* game);

/*It receives a Game*, 
it links the instruction that rolls the die with its command	
@author Miguel Luque*/
STATUS game_callback_roll(Game* game);

/*It receives a Game*, 
sets player location at east of actual location 
@author Nicolás Serrano */
STATUS game_callback_left(Game* game);


/*It receives a Game*, 
sets player location at west of actual location 
@author Nicolás Serrano */
STATUS game_callback_right(Game* game);

/*It receives a Game*, 
sets player location to the one selected 
@author Nicolás Serrano */
STATUS game_callback_move(Game* game);

/*It receives a Game*
get object or space description
@author Javier Sanmiguel and Nicolas Serrano*/
STATUS game_callback_examine(Game* game);

/*It receives a Game*
save game
@author Nicolas Serrano*/
STATUS game_callback_save(Game* game);

/*It receives a Game*
load game
@author Nicolas Serrano*/
STATUS game_callback_load(Game* game);

/*It receives a Game*
sets player location over actual location
@author Nicolas Serrano*/
STATUS game_callback_up(Game* game);

/*It receives a Game*
sets player location under actual location
@author Nicolas Serrano*/
STATUS game_callback_down(Game* game);

/*It receives a Game*
opens a link using an object
@author Miguel Luque*/
STATUS game_callback_use(Game* game);

/*It receives a Game*
takes you to a random place of the map
@author Miguel Luque*/
STATUS game_callback_teleport(Game* game);

/*It receives a Game*,
switches on the object selected
@author Mario Lopez*/
STATUS game_callback_turnon(Game* game);

/*It receives a Game*,
switches off the object selected
@author Mario Lopez*/
STATUS game_callback_turnoff(Game* game);

/*It receives a Game*
takes you to the jail if the guard sees you
@author Miguel Luque*/
STATUS game_callback_busted(Game* game);

/*It receives a Game*
takes you out of the laberynth
@author Miguel Luque*/
STATUS game_callback_path(Game* game);

/*It receives a Game*
makes you find a random object
@author Miguel Luque*/
STATUS game_callback_find(Game* game);

/*It receives a Game*
makes you lose a random object
@author Miguel Luque*/
STATUS game_callback_lose(Game* game);

/*It receives a Game*
makes you be able to see in the day but not in the night
@author Miguel Luque*/
STATUS game_callback_night(Game* game);

static callback_fn game_callback_fn_list[N_CALLBACK] = {
    game_callback_unknown,
    game_callback_exit,
    game_callback_following,
    game_callback_previous,
    game_callback_take,
    game_callback_drop,
    game_callback_roll,
    game_callback_left,
    game_callback_right,
    game_callback_move,
    game_callback_examine,
    game_callback_save,
    game_callback_load,
    game_callback_use,
    game_callback_turnon,
    game_callback_turnoff,
    game_callback_up,
    game_callback_down,
    game_callback_unknown,
    game_callback_teleport,
    game_callback_busted,
    game_callback_path,
    game_callback_find,
    game_callback_lose,
    game_callback_night
};

/*
   Private functions
 */

/*It receives a Game*, 
retuns the ID of an space or NO_ID if and error occurs.	
@author Profesores PPROG */
Id game_get_space_id_at(Game* game, int position);

/*It receives a Game*, 
sets the ID of the space where player is, returns OK if	 everything worked correctly or ERROR if not	
@author Profesores PPROG, updated by Nicolás Serrano */
STATUS game_set_player_location(Game* game, Id id);

/*It receives a Game*, 
sets the ID of the space where object is, returns OK if everything worked correctly or ERROR if not	
@author Profesores PPROG, updated by Nicolás Serrano reupdated by Miguel Luque*/
STATUS game_set_object_location(Object * o, Id id);

/*It receives a Game*, 
Get a random space of the game
@author Miguel Luque*/
Space* game_get_random_space(Game* game);

/*It receives a Game*, 
Get a random object of the game
@author Miguel Luque*/
Object* game_get_random_object(Game* game);

/*
   Game interface implementation
 */

Game* game_create() {
    int i;
    Game* game;

    game = (Game*) malloc(sizeof (Game));
    if (!game) {
        return NULL;
    }

    game->die = die_create(STD_ID);
    if (!game->die) {
        return NULL;
    }

    for (i = 0; i < MAX_SPACES; i++) {
        game->spaces[i] = NULL;
    }
    for (i = 0; i < MAX_LINKS; i++) {
        game->links[i] = NULL;
    }
    for (i = 0; i < MAX_OBJ; i++) {
        game->object_location[i] = NULL;
    }
    game->player_location = NULL;
    game->callback_status = ERROR;
    game->preavious_status = ERROR;
    game->last_cmd = NULL;
    game->last_rule = NULL;
    game->description[0] = '\0';
    return game;
}

STATUS game_create_from_file(Game* game, char* filename) {
    if (!game)
        return ERROR;
    if (game_management_load_spaces(game, filename) == ERROR)
        return ERROR;
    if (game_management_load_links(game, filename) == ERROR)
        return ERROR;
    if (game_management_load_objects(game, filename) == ERROR)
        return ERROR;
    if (game_management_load_player(game, filename) == ERROR)
        return ERROR;
    game_get_space_id_at(game, 0);
    return OK;
}

STATUS game_destroy(Game* game) {
    int i = 0;
    die_destroy(game->die);
    for (i = 0; (i < MAX_SPACES) && (game->spaces[i] != NULL); i++) {
        space_destroy(game->spaces[i]);
    }
    for (i = 0; (i < MAX_LINKS) && (game->links[i] != NULL); i++) {
        link_destroy(game->links[i]);
    }
    player_destroy(game->player_location);
    for (i = 0; i < MAX_OBJ; i++) {
        if (game->object_location[i] != NULL) {
            object_destroy(game->object_location[i]);
        }
    }
    command_destroy(game->last_cmd);
    game_rules_destroy(game->last_rule);
    free(game);
    return OK;
}

STATUS game_add_space(Game* game, Space* space) {
    int i = 0;

    if (space == NULL) {
        return ERROR;
    }

    while ((i < MAX_SPACES) && (game->spaces[i] != NULL)) {
        i++;
    }

    if (i >= MAX_SPACES) {
        return ERROR;
    }

    game->spaces[i] = space;

    return OK;
}

STATUS game_add_link(Game* game, Link* link) {
    int i = 0;

    if (!game || !link) {
        return ERROR;
    }

    while ((i < MAX_LINKS) && (game->links[i] != NULL)) {
        i++;
    }

    if (i >= MAX_LINKS) {
        return ERROR;
    }

    game->links[i] = link;

    return OK;
}

STATUS game_add_object(Game* game, Object* object) {
    int i = 0;

    if (object == NULL) {
        return ERROR;
    }

    while ((i < MAX_OBJ) && (game->object_location[i] != NULL)) {

        i++;
    }

    if (i >= MAX_OBJ) {
        return ERROR;
    }

    game->object_location[i] = object;

    return OK;
}

STATUS game_add_player(Game* game, Player* player) {

    if (!player) {
        return ERROR;
    }

    game->player_location = player;

    return OK;
}

Id game_get_space_id_at(Game* game, int position) {

    if (position < 0 || position >= MAX_SPACES) {
        return NO_ID;
    }

    return space_get_id(game->spaces[position]);
}

Space* game_get_space(Game* game, Id id) {
    int i = 0;

    if (id == NO_ID) {
        return NULL;
    }

    for (i = 0; i < MAX_SPACES && game->spaces[i] != NULL; i++) {
        if (id == space_get_id(game->spaces[i])) {
            return game->spaces[i];
        }
    }

    return NULL;
}

Space* game_get_random_space(Game* game) {
    int i = 0;
    for (i = 0; i < MAX_SPACES && game->spaces[i] != NULL; i++) {
    }
    i--;
    if (i < 0) return NULL;
    return game->spaces[die_random_num(0, i)];
}

Object* game_get_object(Game* game, int array_position) {
    if (!game || array_position < 0) {
        return NULL;
    }

    return game->object_location[array_position];
}

Object* game_get_random_object(Game* game) {
    int i = 0;
    for (i = 0; i < MAX_OBJ && game->object_location[i] != NULL; i++) {
    }
    i--;
    if (i < 0) return NULL;
    return game->object_location[die_random_num(0, i)];
}

Player* game_get_player(Game* game) {
    if (!game) {
        return NULL;
    }

    return game->player_location;
}

Link* game_get_link(Game* game, Id id) {
    int i;

    if (!game) {
        return NULL;
    }
    for (i = 0; i < MAX_LINKS; i++) {
        if (link_get_id(game->links[i]) == id) {
            return game->links[i];
        }
    }
    return NULL;
}

Die* game_get_die(Game* game) {
    if (!game) {
        return NULL;
    }
    return game->die;
}

STATUS game_get_callback_status(Game* game) {
    if (!game) {
        return ERROR;
    }
    return game->callback_status;
}

STATUS game_get_preavious_status(Game* game) {
    if (!game) {
        return ERROR;
    }
    return game->preavious_status;
}

STATUS game_set_player_location(Game* game, Id id) {

    if (id == NO_ID) {
        return ERROR;
    }

    if (player_set_location(game->player_location, id) == ERROR) {
        return ERROR;
    }

    return OK;

}

STATUS game_set_object_location(Object* o, Id id) {

    if (id == NO_ID) {
        return ERROR;
    }

    if (object_set_location(o, id) == ERROR) {
        return ERROR;
    }

    return OK;
}

Id game_get_player_location(Game* game) {

    if (!game) {
        return NO_ID;
    }
    return player_get_location(game->player_location);
}

Id game_get_object_location(Object * o) {

    if (!o) {
        return NO_ID;
    }
    return object_get_location(o);
}

char* game_get_description(Game* game) {
    if (!game) {
        return NULL;
    }
    return game->description;
}

STATUS game_update(Game* game, Command* cmd) {
    command_destroy(game->last_cmd);
    game->last_cmd = command_copy(cmd);
    game->preavious_status = game->callback_status;
    game->callback_status = (*game_callback_fn_list[command_get_cmd(game->last_cmd)])(game);
    return OK;
}

STATUS game_rule_update(Game* game, Rule* rule) {
    if (!game || !rule) {
        return ERROR;
    }
    game_rules_destroy(game->last_rule);
    game->last_rule = game_rules_copy(rule);
    game_callback_fn_list[game_rules_get(game->last_rule)](game);
    return OK;
}

Command* game_get_last_command(Game* game) {
    return game->last_cmd;
}

Rule* game_get_last_rule(Game* game) {
    return game->last_rule;
}

void game_print_data(Game* game, FILE *f) {
    int i = 0;

    if (f && game) {

        for (i = 0; i < MAX_SPACES && game->spaces[i] != NULL; i++) {
            space_print(game->spaces[i], f);
        }
        for (i = 0; i < MAX_OBJ; i++) {
            if (game->object_location[i] != NULL) {
                object_print(game->object_location[i], f);
            }
        }

        for (i = 0; i < MAX_LINKS; i++) {
            if (game->links[i] != NULL) {
                link_print(game->links[i], f);
            }
        }
        player_print(game->player_location, f);
    }
}

BOOL game_is_over(Game* game) {
    if (!game) {
        return TRUE;
    }
    if (game_get_player_location(game) == 63 && game_get_callback_status(game) == ERROR) {
        return TRUE;
    }
    return FALSE;
}

/**
   Callbacks implementation for each action 
 */

STATUS game_callback_unknown(Game* game) {
    if (!game) {
        return ERROR;
    }
    return OK;
}

STATUS game_callback_exit(Game* game) {
    if (!game) {
        return ERROR;
    }
    return OK;
}

STATUS game_callback_following(Game* game) {
    int i;
    Id current_id = NO_ID;
    Id space_id = NO_ID;

    if (!game) {
        return ERROR;
    }

    space_id = game_get_player_location(game);
    if (space_id == NO_ID) {
        return ERROR;
    }

    for (i = 0; i < MAX_SPACES && game->spaces[i] != NULL; i++) {
        current_id = space_get_id(game->spaces[i]);
        if (current_id == space_id) {
            current_id = link_get_space_id(game_get_link(game, space_get_south(game->spaces[i])), 2);
            if ((current_id != NO_ID)&&(link_get_status(game_get_link(game, space_get_south(game->spaces[i]))) == OK)) {
                game_set_player_location(game, current_id);
                return OK;
            }
        }
    }

    return ERROR;
}

STATUS game_callback_previous(Game* game) {
    int i = 0;
    Id current_id = NO_ID;
    Id space_id = NO_ID;

    if (!game) {
        return ERROR;
    }

    space_id = game_get_player_location(game);

    if (NO_ID == space_id) {
        return ERROR;
    }

    for (i = 0; i < MAX_SPACES && game->spaces[i] != NULL; i++) {
        current_id = space_get_id(game->spaces[i]);
        if (current_id == space_id) {
            current_id = link_get_space_id(game_get_link(game, space_get_north(game->spaces[i])), 1);
            if ((current_id != NO_ID)&&(link_get_status(game_get_link(game, space_get_north(game->spaces[i]))) == OK)) {
                game_set_player_location(game, current_id);
                return OK;
            }
        }
    }
    return ERROR;
}

STATUS game_callback_take(Game* game) {
    int i;
    if (!game) {
        return ERROR;
    }

    if (inventory_full(player_get_inventory(game->player_location)) == TRUE) {
        return ERROR;
    }

    for (i = 0; i < MAX_OBJ; i++) {
        if (game->object_location[i] != NULL) {
            if (strcmp(object_get_name(game->object_location[i]), command_get_object(game->last_cmd)) == 0) {
                if (object_get_movable(game->object_location[i]) == TRUE) {
                    if (game_get_player_location(game) == game_get_object_location(game->object_location[i])) {
                        if (player_set_object(game->player_location, object_get_id(game->object_location[i])) == ERROR) {
                            return ERROR;
                        }

                        if (object_set_location(game->object_location[i], NO_ID) == ERROR) {
                            return ERROR;
                        }

                        if (object_set_moved(game->object_location[i], TRUE) == ERROR) {
                            return ERROR;
                        }
                        if (object_set_hidden(game->object_location[i], FALSE) == ERROR) {
                            return ERROR;
                        }
                        return OK;
                    }
                }
            }
        }
    }
    return ERROR;
}

STATUS game_callback_drop(Game* game) {
    int i;
    Id player_obj = NO_ID;

    if (!game) {
        return ERROR;
    }

    for (i = 0; i < MAXBACKPACK; i++) {
        player_obj = set_get_id(player_get_object(game_get_player(game)), i);
        if (player_obj != NO_ID) {
            if (strcmp(object_get_name(game_get_object(game, player_obj + NO_ID)), command_get_object(game->last_cmd)) == 0) {
                if (object_get_movable(game_get_object(game, player_obj + NO_ID)) == TRUE) {
                    if (object_set_location(game_get_object(game, player_obj + NO_ID), game_get_player_location(game)) == ERROR) {
                        return ERROR;
                    }
                    if (set_del_value(player_get_object(game->player_location), player_obj) == ERROR) {
                        return ERROR;
                    }
                    return OK;
                }
            }
        }
    }
    return ERROR;
}

STATUS game_callback_roll(Game* game) {
    if (!game) {
        return ERROR;
    }

    die_roll(game->die);
    return OK;
}

STATUS game_callback_left(Game* game) {
    int i = 0;
    Id current_id = NO_ID;
    Id space_id = NO_ID;

    if (!game) {
        return ERROR;
    }

    space_id = game_get_player_location(game);
    if (space_id == NO_ID) {
        return ERROR;
    }

    for (i = 0; i < MAX_SPACES && game->spaces[i] != NULL; i++) {
        current_id = space_get_id(game->spaces[i]);
        if (current_id == space_id) {
            current_id = link_get_space_id(game_get_link(game, space_get_east(game->spaces[i])), 1);
            if ((current_id != NO_ID)&&(link_get_status(game_get_link(game, space_get_east(game->spaces[i]))) == OK)) {
                game_set_player_location(game, current_id);
                return OK;
            }

        }
    }
    return ERROR;
}

STATUS game_callback_right(Game* game) {
    int i = 0;
    Id current_id = NO_ID;
    Id space_id = NO_ID;

    if (!game) {
        return ERROR;
    }

    space_id = game_get_player_location(game);
    if (space_id == NO_ID) {
        return ERROR;
    }

    for (i = 0; i < MAX_SPACES && game->spaces[i] != NULL; i++) {
        current_id = space_get_id(game->spaces[i]);
        if (current_id == space_id) {
            current_id = link_get_space_id(game_get_link(game, space_get_west(game->spaces[i])), 2);
            if ((current_id != NO_ID)&&(link_get_status(game_get_link(game, space_get_west(game->spaces[i]))) == OK)) {
                game_set_player_location(game, current_id);
                return OK;
            }
        }
    }
    return ERROR;
}

STATUS game_callback_move(Game* game) {
    STATUS status = ERROR;

    if (!game) {
        return status;
    }

    if (strcmp(command_get_object(game->last_cmd), "n") == 0 || strcmp(command_get_object(game->last_cmd), "north") == 0 || strcmp(command_get_object(game->last_cmd), "North") == 0) {
        status = game_callback_previous(game);
    } else if (strcmp(command_get_object(game->last_cmd), "s") == 0 || strcmp(command_get_object(game->last_cmd), "south") == 0 || strcmp(command_get_object(game->last_cmd), "South") == 0) {
        status = game_callback_following(game);
    } else if (strcmp(command_get_object(game->last_cmd), "e") == 0 || strcmp(command_get_object(game->last_cmd), "east") == 0 || strcmp(command_get_object(game->last_cmd), "East") == 0) {
        status = game_callback_right(game);
    } else if (strcmp(command_get_object(game->last_cmd), "w") == 0 || strcmp(command_get_object(game->last_cmd), "west") == 0 || strcmp(command_get_object(game->last_cmd), "West") == 0) {
        status = game_callback_left(game);
    } else if (strcmp(command_get_object(game->last_cmd), "u") == 0 || strcmp(command_get_object(game->last_cmd), "up") == 0 || strcmp(command_get_object(game->last_cmd), "Up") == 0) {
        status = game_callback_up(game);
    } else if (strcmp(command_get_object(game->last_cmd), "d") == 0 || strcmp(command_get_object(game->last_cmd), "down") == 0 || strcmp(command_get_object(game->last_cmd), "Down") == 0) {
        status = game_callback_down(game);
    }

    return status;
}

STATUS game_callback_examine(Game* game) {
    int i = 0, j = 0, counter = 0;
    Id current_id = NO_ID;
    Id space_id = NO_ID;
    Id obj_id = NO_ID;

    if (!game) {
        return ERROR;
    }

    if (strcmp(command_get_object(game->last_cmd), "space") == 0) {
        space_id = game_get_player_location(game);
        if (space_id == NO_ID) {
            return ERROR;
        }
    }

    for (i = 0; i < MAX_SPACES && game->spaces[i] != NULL; i++) {
        current_id = space_get_id(game->spaces[i]);
        if (current_id == space_id) {
            if (space_get_illumination(game->spaces[i]) == TRUE) {
                strcpy(game->description, space_get_description(game->spaces[i]));
                return OK;
            } else if (space_get_illumination(game->spaces[i]) == FALSE) {
                for (j = 0; j < MAXBACKPACK; j++) {
                    counter++;
                    obj_id = set_get_id(player_get_object(game_get_player(game)), j);
                    if (object_get_switchedOn(game_get_object(game, obj_id + NO_ID)) == TRUE) {
                        strcpy(game->description, space_get_description(game->spaces[i]));
                        return OK;
                    }
                }
            }
            if (counter == MAXBACKPACK) {
                strcpy(game->description, space_get_descriptioff(game->spaces[i]));
                return OK;
            }
        }
    }

    for (i = 0; i < MAX_OBJ; i++) {
        if (game->object_location[i] != NULL) {
            if (strcmp(object_get_name(game->object_location[i]), command_get_object(game->last_cmd)) == 0) {
                if (object_get_location(game->object_location[i]) == player_get_location(game->player_location)) {
                    if (object_get_moved(game->object_location[i]) == TRUE) {
                        strcpy(game->description, object_get_description2(game->object_location[i]));
                        return OK;

                    } else {
                        strcpy(game->description, object_get_description(game->object_location[i]));
                        return OK;
                    }
                } else {
                    for (j = 0; j < MAXBACKPACK; j++) {

                        if (set_get_id(player_get_object(game_get_player(game)), j) == object_get_id(game->object_location[i])) {

                            if (object_get_moved(game->object_location[i]) == TRUE) {
                                strcpy(game->description, object_get_description2(game->object_location[i]));
                                return OK;
                            } else {
                                strcpy(game->description, object_get_description(game->object_location[i]));
                                return OK;
                            }
                        }
                    }
                }
            }
        }
    }
    return ERROR;
}

STATUS game_callback_save(Game* game) {
    char root[WORD_SIZE] = "save/";
    if (!game) {
        return ERROR;
    }

    return game_management_save(game, strcat(root, command_get_object(game->last_cmd)));

}

STATUS game_callback_load(Game* game) {
    FILE* file = NULL;
    Game *load;
    char root[WORD_SIZE] = "save/";
    int i;

    if (!game) {
        return ERROR;
    }

    if (strcmp(command_get_object(game->last_cmd), "") == 0) {
        return ERROR;
    }
    file = fopen(strcat(root, command_get_object(game->last_cmd)), "r");
    if (file == NULL) {
        return ERROR;
    }
    fclose(file);

    load = game_create();
    game_management_load(load, root);
    load->last_cmd = command_copy(game->last_cmd);

    /*Destruimos el contenido de game*/
    die_destroy(game->die);
    for (i = 0; (i < MAX_SPACES) && (game->spaces[i] != NULL); i++) {
        space_destroy(game->spaces[i]);
    }
    for (i = 0; (i < MAX_LINKS) && (game->links[i] != NULL); i++) {
        link_destroy(game->links[i]);
    }
    player_destroy(game->player_location);
    for (i = 0; i < MAX_OBJ; i++) {
        if (game->object_location[i] != NULL) {
            object_destroy(game->object_location[i]);
        }
    }
    command_destroy(game->last_cmd);

    *game = *load;
    free(load);

    return OK;

}

STATUS game_callback_use(Game* game) {
    int i, j, k;
    for (i = 0; i < MAX_OBJ; i++) {
        if (game->object_location[i] != NULL) {
            if (strcmp(object_get_name(game->object_location[i]), command_get_object(game->last_cmd)) == 0) {
                for (k = 0; k < MAXBACKPACK; k++) {
                    if (set_get_id(inventory_get_ids(player_get_inventory(game_get_player(game))), k) == object_get_id(game->object_location[i])) {
                        if (strcmp("Vodka", command_get_object(game->last_cmd)) == 0) {
                            game_callback_teleport(game);
                            game_callback_find(game);
                            game_callback_lose(game);
                            return OK;
                        } else {
                            for (j = 0; j < MAX_LINKS; j++) {
                                if (game->links[j] != NULL) {

                                    if (strcmp(link_get_name(game->links[j]), command_get_parameter2(game->last_cmd)) == 0) {
				   	if (link_get_space_id(game->links[j] , 1) == game_get_player_location(game) || link_get_space_id(game->links[j] , 2) == game_get_player_location(game)){
                                            if (object_get_open(game->object_location[i]) == link_get_id(game->links[j])) {
                                                if (link_get_status(game->links[j]) == 0) {
                                                    link_set_status(game->links[j], 1);
                                                    if (set_del_value(player_get_object(game->player_location), object_get_id(game->object_location[i])) == OK) {
                                                        if (object_set_location(game->object_location[i], 105) == OK) {
                                                            return OK;
							}
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return ERROR;
}

STATUS game_callback_up(Game* game) {
    int i;
    Id current_id = NO_ID;
    Id space_id = NO_ID;

    if (!game) {
        return ERROR;
    }

    space_id = game_get_player_location(game);
    if (space_id == NO_ID) {
        return ERROR;
    }

    for (i = 0; i < MAX_SPACES && game->spaces[i] != NULL; i++) {
        current_id = space_get_id(game->spaces[i]);
        if (current_id == space_id) {
            current_id = link_get_space_id(game_get_link(game, space_get_up(game->spaces[i])), 2);
            if ((current_id != NO_ID)&&(link_get_status(game_get_link(game, space_get_up(game->spaces[i]))) == OK)) {
                game_set_player_location(game, current_id);
                return OK;
            }
        }
    }

    return ERROR;
}

STATUS game_callback_down(Game* game) {
    int i;
    Id current_id = NO_ID;
    Id space_id = NO_ID;

    if (!game) {
        return ERROR;
    }

    space_id = game_get_player_location(game);
    if (space_id == NO_ID) {
        return ERROR;
    }

    for (i = 0; i < MAX_SPACES && game->spaces[i] != NULL; i++) {
        current_id = space_get_id(game->spaces[i]);
        if (current_id == space_id) {
            current_id = link_get_space_id(game_get_link(game, space_get_down(game->spaces[i])), 1);
            if ((current_id != NO_ID)&&(link_get_status(game_get_link(game, space_get_down(game->spaces[i]))) == OK)) {
                game_set_player_location(game, current_id);
                return OK;
            }
        }
    }

    return ERROR;
}

STATUS game_callback_turnon(Game* game) {
    int i, j;
    if (!game) {
        return ERROR;
    }

    for (i = 0; i < MAX_OBJ; i++) {
        if (game->object_location[i] != NULL) {
            if (strcmp(object_get_name(game->object_location[i]), command_get_object(game->last_cmd)) == 0) {
                for (j = 0; j < MAXBACKPACK; j++) {

                    if (set_get_id(player_get_object(game_get_player(game)), j) == object_get_id(game->object_location[i])) {
                        if (object_get_switchedOn(game->object_location[i]) == TRUE) {
                            return ERROR;
                        } else {
                            object_set_switchedOn(game->object_location[i], TRUE);
                            return OK;
                        }
                    }
                }
            }
        }
    }
    return ERROR;
}

STATUS game_callback_turnoff(Game* game) {
    int i, j;
    if (!game) {
        return ERROR;
    }

    for (i = 0; i < MAX_OBJ; i++) {
        if (game->object_location[i] != NULL) {
            if (strcmp(object_get_name(game->object_location[i]), command_get_object(game->last_cmd)) == 0) {
                for (j = 0; j < MAXBACKPACK; j++) {

                    if (set_get_id(player_get_object(game_get_player(game)), j) == object_get_id(game->object_location[i])) {
                        if (object_get_switchedOn(game->object_location[i]) == FALSE) {
                            return ERROR;
                        } else {
                            object_set_switchedOn(game->object_location[i], FALSE);
                            return OK;
                        }
                    }
                }
            }
        }
    }
    return ERROR;
}

STATUS game_callback_teleport(Game* game) {
    if (!game) return ERROR;
    player_set_location(game->player_location, die_random_num(103, 104));
    return OK;
}

STATUS game_callback_busted(Game* game) {
    Id x;
    x = (player_get_location(game->player_location) - 52);
    if (x < 0) x = -x;
    if ((die_random_num(0, x + 8) == 0)&&(x < 4)) {
        player_set_location(game->player_location, 88);
        return OK;
    } else {
        return OK;
    }

}

STATUS game_callback_path(Game* game) {
    if (die_random_num(0, 9) == 0) {
        if ((player_get_location(game->player_location) < 80)&&(player_get_location(game->player_location) > 72)) {
            if ((command_get_cmd(game->last_cmd) == 2) || (command_get_cmd(game->last_cmd) == 3) || (command_get_cmd(game->last_cmd) == 7) || (command_get_cmd(game->last_cmd) == 8) || (command_get_cmd(game->last_cmd) == 9)) {
                player_set_location(game->player_location, 72);
                return OK;
            }
        }
    }
    return OK;
}

STATUS game_callback_find(Game* game) {
    int x, i;
    x = die_random_num(24, 27);
    if (!game) {
        return ERROR;
    }

    if (inventory_full(player_get_inventory(game->player_location)) == TRUE) {
        return ERROR;
    }
    if (player_set_object(game->player_location, x) == ERROR) {
        return ERROR;
    }
    for (i = 0; i < MAX_OBJ; i++) {
        if (game->object_location[i] != NULL) {
            if (object_get_id(game->object_location[i]) == x) {
                if (object_set_location(game->object_location[i], NO_ID) == ERROR) {
                    return ERROR;
                }
            }
        }
    }
    return OK;
}

STATUS game_callback_lose(Game* game) {
    int k, x;
    Id y;
    int i = 0;
    for (k = 0; k < MAXBACKPACK; k++) {
        if (set_get_id(inventory_get_ids(player_get_inventory(game_get_player(game))), k) != NO_ID) {
            i++;
        }
    }
    if (i > 0) {
        x = die_random_num(0, (i - 1));
        y = set_get_id(inventory_get_ids(player_get_inventory(game_get_player(game))), x);
        if (object_set_location(game_get_object(game, y + NO_ID), die_random_num(100, 104)) == ERROR) {
            return ERROR;
        }
        if (set_del_value(player_get_object(game->player_location), y) == ERROR) {
            return ERROR;
        }
        return OK;
    }
    return ERROR;
}

STATUS game_callback_night(Game* game) {
    int i;
    if (die_random_num(0, 12) == 0) {
        if (!game) return ERROR;
        for (i = 0; i < MAX_SPACES; i++) {
            if (game->spaces[i] != NULL) {
                if (space_get_id(game->spaces[i]) < 63) {
                    /*If day, set night*/
                    if (space_get_illumination(game->spaces[i]) == FALSE) {
                        space_set_illumination(game->spaces[i], TRUE);
                    }                        /*If night, set day*/
                    else {
                        space_set_illumination(game->spaces[i], FALSE);

                    }
                }
            }
        }
    }
    return OK;
}
