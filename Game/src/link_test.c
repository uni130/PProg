/** 
 * @brief It tests link module
 *
 * @file link_test.c
 * @author Miguel Luque
 * @version 1.0
 * @date 06-04-2018 
 * @copyright GNU Public License
 */

#include "../include/link_test.h"
#include <string.h>
#define MAX_TESTS 17

/** 
 * @brief Main function for SPACE unit tests. 
 * 
 * You may execute ALL or a SINGLE test
 *   1.- No parameter -> ALL test are executed 
 *   2.- A number means a particular test (the one identified by that number) 
 *       is executed
 *  
 */
int main(int argc, char** argv) {

    int test = 0;
    int all = 1;

    if (argc < 2) {
        printf("Running all test for module link:\n");
    } else {
        test = atoi(argv[1]);
        all = 0;
        printf("Running test %d:\t", test);
        if (test < 1 && test > MAX_TESTS) {
            printf("Error: unknown test %d\t", test);
            exit(EXIT_SUCCESS);
        }
    }

    if (all || test == 1) test1_link_create();
    if (all || test == 2) test1_link_get_space_id();
    if (all || test == 3) test2_link_get_space_id();
    if (all || test == 4) test1_link_get_id();
    if (all || test == 5) test2_link_get_id();
    if (all || test == 6) test1_link_get_name();
    if (all || test == 7) test2_link_get_name();
    if (all || test == 8) test1_link_get_status();
    if (all || test == 9) test2_link_get_status();
    if (all || test == 10) test1_link_set_status();
    if (all || test == 11) test2_link_set_status();
    if (all || test == 12) test1_link_set_spaces();
    if (all || test == 13) test2_link_set_spaces();
    if (all || test == 14) test1_link_set_id();
    if (all || test == 15) test2_link_set_id();
    if (all || test == 16) test1_link_set_name();
    if (all || test == 17) test2_link_set_name();

    PRINT_PASSED_PERCENTAGE;

    return 0;
}

void test1_link_create() {
    Link *l = NULL;
    l = link_create();
    PRINT_TEST_RESULT(l != NULL);
    link_destroy(l);
}

void test1_link_get_space_id() {
    Link *l = NULL;
    l = link_create();
    link_set_spaces(l, 1, 2);
    PRINT_TEST_RESULT(link_get_space_id(l, 1) == 1);
    link_destroy(l);
}

void test2_link_get_space_id() {
    Link *l = NULL;
    PRINT_TEST_RESULT(link_get_space_id(l, 1) == NO_ID);
}

void test1_link_get_id() {
    Link *l = NULL;
    l = link_create();
    link_set_id(l, 3);
    PRINT_TEST_RESULT(link_get_id(l) == 3);
    link_destroy(l);
}

void test2_link_get_id() {
    Link *l = NULL;
    PRINT_TEST_RESULT(link_get_id(l) == NO_ID);
}

void test1_link_get_name() {
    Link *l = NULL;
    l = link_create();
    link_set_name(l, "name");
    PRINT_TEST_RESULT(strcmp(link_get_name(l), "name") == 0);
    link_destroy(l);
}

void test2_link_get_name() {
    Link *l = NULL;
    PRINT_TEST_RESULT(link_get_name(l) == NULL);
}

void test1_link_get_status() {
    Link *l = NULL;
    l = link_create();
    link_set_status(l, OK);
    PRINT_TEST_RESULT(link_get_status(l) == OK);
    link_destroy(l);
}

void test2_link_get_status() {
    Link *l = NULL;
    l = link_create();
    link_set_status(l, ERROR);
    PRINT_TEST_RESULT(link_get_status(l) == ERROR);
    link_destroy(l);
}

void test1_link_set_status() {
    Link *l = NULL;
    l = link_create();
    PRINT_TEST_RESULT(link_set_status(l, OK) == OK);
    link_destroy(l);
}

void test2_link_set_status() {
    Link *l = NULL;
    PRINT_TEST_RESULT(link_set_status(l, OK) == ERROR);
}

void test1_link_set_spaces() {
    Link *l = NULL;
    l = link_create();
    PRINT_TEST_RESULT(link_set_spaces(l, 1, 2) == OK);
    link_destroy(l);
}

void test2_link_set_spaces() {
    Link *l = NULL;
    PRINT_TEST_RESULT(link_set_spaces(l, 1, 2) == ERROR);
}

void test1_link_set_id() {
    Link *l = NULL;
    l = link_create();
    PRINT_TEST_RESULT(link_set_id(l, 1) == OK);
    link_destroy(l);
}

void test2_link_set_id() {
    Link *l = NULL;
    PRINT_TEST_RESULT(link_set_id(l, 1) == ERROR);
}

void test1_link_set_name() {
    Link *l = NULL;
    l = link_create();
    PRINT_TEST_RESULT(link_set_name(l, "name") == OK);
    link_destroy(l);
}

void test2_link_set_name() {
    Link *l = NULL;
    PRINT_TEST_RESULT(link_set_name(l, "name") == ERROR);
}

