/** 
 * @brief It defines the game management
 * 
 * @file game_management_test.c
 * @author Nicolás Serrano
 * @version 3.0
 * @date 17-04-2018 
 * @copyright GNU Public License
 */

#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
#include "../include/game_management_test.h"
#include "../include/game_management.h"
#include "../include/inventory_test.h"

#include "../include/types.h"

#define MAX_TESTS 6

/** 
 * @brief Main function for INVENTORY unit tests. 
 * 
 * You may execute ALL or a SINGLE test
 *   1.- No parameter -> ALL test are executed 
 *   2.- A number means a particular test (the one identified by that number) 
 *       is executed
 *  
 */
int main(int argc, char** argv) {

    int test = 0;
    int all = 1;

    if (argc < 2) {
        printf("Running all test for module Game_management:\n");
    } else {
        test = atoi(argv[1]);
        all = 0;
        printf("Running test %d:\t", test);
        if (test < 1 && test > MAX_TESTS) {
            printf("Error: unknown test %d\t", test);
            exit(EXIT_SUCCESS);
        }
    }


    if (all || test == 1) test1_game_management_load_player();
    if (all || test == 2) test2_game_management_load_player();
    if (all || test == 3) test1_game_management_save();
    if (all || test == 4) test2_game_management_save();
    if (all || test == 5) test1_game_management_load();
    if (all || test == 6) test2_game_management_load();

    PRINT_PASSED_PERCENTAGE;

    return 0;
}

void test1_game_management_load_player() {
    Game *game;

    game = game_create();
    PRINT_TEST_RESULT(game_management_load_player(game, "doc/data.dat") == OK);
    game_destroy(game);
}

void test2_game_management_load_player() {
    PRINT_TEST_RESULT(game_management_load_player(NULL, "doc/data.dat") == ERROR);
}

void test1_game_management_save() {
    Game *game;

    game = game_create();
    game_create_from_file(game, "save/test2.dat");
    PRINT_TEST_RESULT(game_management_save(game, "save/test1.dat") == OK);
    game_destroy(game);
}

void test2_game_management_save() {
    PRINT_TEST_RESULT(game_management_load(NULL, "save/test1.dat") == ERROR);
}

void test1_game_management_load() {
    Game *game;

    game = game_create();
    PRINT_TEST_RESULT(game_management_load(game, "save/test2.dat") == OK);
    game_destroy(game);
}

void test2_game_management_load() {
    PRINT_TEST_RESULT(game_management_load(NULL, "save/test2.dat") == ERROR);
}

