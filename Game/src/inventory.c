/** 
 * @brief It defines TAD inventory
 * 
 * @file inventory.c
 * @author Mario Lopez
 * @version 1.0
 * @date 15-03-2018
 * @copyright GNU Public License
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/inventory.h"

struct _Inventory {
    Set* id;
    int maxObjects;
};

Inventory* inventory_create() {
    Inventory* newInvent = NULL;

    newInvent = (Inventory *) malloc(sizeof (Inventory));

    if (newInvent == NULL) {
        return NULL;
    }

    newInvent->id = set_create();
    if (!newInvent->id) {
        free(newInvent);
        return NULL;
    }

    newInvent->maxObjects = MAXBACKPACK;

    return newInvent;
}

STATUS inventory_destroy(Inventory* inventory) {
    if (!inventory) {
        return ERROR;
    }

    set_destroy(inventory->id);
    free(inventory);
    inventory = NULL;

    return OK;
}

STATUS inventory_set_id(Inventory* inventory, Id id) {
    if (!inventory || inventory_full(inventory) == TRUE) {
        return ERROR;
    }

    if (set_add_value(inventory->id, id) == ERROR) {
        return ERROR;
    }

    return OK;
}

STATUS inventory_set_maxObjects(Inventory* inventory, int num) {
    if (!inventory) {
        return ERROR;
    }
    if (num < 1) {
        return ERROR;
    }

    inventory->maxObjects = num;

    return OK;
}

Set* inventory_get_ids(Inventory* inventory) {
    if (!inventory)
        return NULL;
    return inventory->id;
}

int inventory_get_maxObjects(Inventory* inventory) {
    if (!inventory) {
        return -1;
    }

    return inventory->maxObjects;
}

BOOL inventory_full(Inventory* inventory) {
    int i, empty_inventory = 0;
    Id current_object;

    if (!inventory) {
        return FALSE;
    }
    for (i = 0; i < MAXBACKPACK; i++) {

        current_object = set_get_id(inventory->id, i);
        if (current_object != NO_ID) {
            empty_inventory++;
        }
    }
    if (empty_inventory == MAXBACKPACK) {
        return TRUE;
    }
    return FALSE;

}

STATUS inventory_print(Inventory* inventory) {
    if (set_print_values(inventory->id) == ERROR) {
        return ERROR;
    }
    printf("maximun number of objects on the backpack: %d", inventory->maxObjects);

    printf("Contains: ");
    set_print_values(inventory->id);

    return OK;

}
