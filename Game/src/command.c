/** 
 * @brief It implements the command interpreter
 * 
 * @file command.c
 * @author Profesores PPROG, Group 1
 * @version 3.0
 * @date 04-04-2018 
 * @copyright GNU Public License
 */

#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>
#include "../include/command.h"


/*Defines the maximun lenght of the array char*/
#define CMD_LENGHT 30 

struct _Command {
    T_Command cmd;
    char parameter[WORD_SIZE];
    char parameter2[WORD_SIZE];
    struct _Command* last_cmd;
};

/*List of words that affect command*/
char *cmd_to_str[N_CMD] = {"No command", "Unknown", "Exit", "Skip", "Previous", "Grasp", "Drop", "Roll", "Left", "Right", "Move", "Check", "Save", "Load", "Use", "Turnon", "Turnoff"};
/*List of letters that affect command*/
char *short_cmd_to_str[N_CMD] = {"", "", "e", "f", "p", "g", "d", "t", "i", "r", "m", "c", "s", "l", "u","+", "-"};

Command* command_ini() {
    Command *cmd, *aux;
    cmd = (Command*) malloc(sizeof (Command));
    if (!cmd) {
        return NULL;
    }

    cmd->cmd = NO_CMD;
    strcpy(cmd->parameter, " ");
    strcpy(cmd->parameter2, " ");
    aux = (Command*) malloc(sizeof (Command));
    if (!aux) {
        return NULL;
    }
    aux->last_cmd = NULL;
    aux->cmd = NO_CMD;
    strcpy(aux->parameter, " ");
    strcpy(aux->parameter2, " ");
    cmd->last_cmd = aux;

    return cmd;
}

void command_destroy(Command* cmd) {
	if(cmd){
		if(cmd->last_cmd){
			free(cmd->last_cmd);
		}
		free(cmd);
	}
}

STATUS command_get_user_input(Command* cmd) {
    char buff[WORD_SIZE] = "";
    char input[CMD_LENGHT] = "";
    char input2[CMD_LENGHT] = "";
    char input3[CMD_LENGHT] = "";
    char input4[CMD_LENGHT] = "";

    int i = UNKNOWN - NO_CMD + 1, num;

    if (!cmd) {
        return ERROR;
    }
    	

    fgets(buff, WORD_SIZE, stdin);
    num = sscanf(buff, "%s %s %s %s\n", input, input2, input3, input4);   
	if (num > 0) {
	cmd->last_cmd->cmd = cmd->cmd;
	strcpy(cmd->last_cmd->parameter, cmd->parameter);
	strcpy(cmd->last_cmd->parameter2, cmd->parameter2);
	cmd->last_cmd->last_cmd = NULL;
	
	cmd->cmd = NO_CMD;

        cmd->cmd = UNKNOWN;
        strcpy(cmd->parameter, "");
        strcpy(cmd->parameter2, "");
		

        while (cmd->cmd == UNKNOWN && i < N_CMD) {
            if (!strcasecmp(
                    input, short_cmd_to_str[i]
                    ) || !strcasecmp(input,
                    cmd_to_str[i])) {
                cmd->cmd = i + NO_CMD;
                if (num > 1) {
                    strcpy(cmd->parameter, input2);
                }
		if (num > 2){
			if(strcmp(input3,"with")==0){
				strcpy(cmd->parameter2, input4);
			printf("%s y %s\n", cmd->parameter2, input4);
			}
		}
             } else {
                i++;
            }
        }
        return OK;
    }
    cmd->last_cmd->cmd = cmd->cmd;
    strcpy(cmd->last_cmd->parameter, cmd->parameter);
    strcpy(cmd->last_cmd->parameter2, cmd->parameter2);
    cmd->last_cmd->last_cmd = NULL;

    cmd->cmd = UNKNOWN;
    strcpy(cmd->parameter, "");
    strcpy(cmd->parameter2, "");

    return OK;
}

T_Command command_get_cmd(Command* cmd) {
    if (!cmd) {
        return NO_CMD;
    }
    return cmd->cmd;
}

char* command_get_object(Command* cmd) {
    if (!cmd) {
        return NULL;
    }
    return cmd->parameter;
}

char* command_get_parameter2(Command* cmd) {
    if (!cmd) {
        return NULL;
    }
    return cmd->parameter2;
}

Command* command_copy(const Command *cmd) {
    Command *aux;
    if (!cmd) {
        return NULL;
    }

    aux = (Command*) malloc(sizeof (Command));
    if (!aux) {
        return NULL;
    }

    aux->cmd = command_get_cmd((Command *) cmd);
    strcpy(aux->parameter, command_get_object((Command *) cmd));
    strcpy(aux->parameter2, command_get_parameter2((Command *) cmd));
    aux->last_cmd = command_copy(cmd->last_cmd);
    return aux;
}

Command* command_set_command(Command* cmd, T_Command newCmd){
	if(!cmd) return NULL;
	cmd->cmd = newCmd;
	return cmd;

}

BOOL command_compare_to_last(Command*command){
	if(!command || !(command->last_cmd)){
		return FALSE;
	}

	if(command->cmd == command->last_cmd->cmd && strcmp(command->parameter,command->last_cmd->parameter) == 0 && strcmp(command->parameter2, command->last_cmd->parameter2) == 0){
		return TRUE;
	}
	return FALSE;
}
