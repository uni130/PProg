/** 
 * @brief It defines the TAD object
 * 
 * @file object.c
 * @author Group 1
 * @version 3.0 
 * @date 03-04-2018
 * @copyright GNU Public License
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/object.h"

/*It defines the struct of the TAD Object*/
struct _Object {
    Id id;
    char name[WORD_SIZE + 1];
    Id location;
    char description[WORD_SIZE + 1];
    char description2[WORD_SIZE + 1];
    BOOL movable;
    BOOL moved;
    BOOL hidden;
    Id open;
    BOOL illuminate;
    BOOL switchedOn;
};

Object* object_create(Id id) {

    Object *newObject = NULL;

    if (id == NO_ID)
        return NULL;

    newObject = (Object *) malloc(sizeof (Object));

    if (newObject == NULL) {
        return NULL;
    }
    newObject->id = id;

    newObject->name[0] = '\0';

    newObject->location = NO_ID;

    newObject->description[0] = '\0';
    
    newObject->description2[0] = '\0';

    return newObject;
}

STATUS object_destroy(Object* object) {
    if (!object) {
        return ERROR;
    }

    free(object);
    object = NULL;

    return OK;
}

const char * object_get_name(Object* object) {
    if (!object) {
        return NULL;
    }
    return object->name;
}

char * object_get_description(Object* object) {
    if (!object) {
        return NULL;
    }
    return object->description;
}

char * object_get_description2(Object* object) {
    if (!object) {
        return NULL;
    }
    return object->description2;
}

Id object_get_id(Object* object) {
    if (!object) {
        return NO_ID;
    }
    return object->id;
}

Id object_get_location(Object* object) {
    if (!object) {
        return NO_ID;
    }
    return object->location;
}

BOOL object_get_movable(Object* object){
    if (!object) {
        return FALSE;
    }
    return object->movable;
}

BOOL object_get_moved(Object* object){
    if (!object) {
        return FALSE;
    }
    return object->moved;
}

BOOL object_get_hidden(Object* object){
    if (!object) {
        return FALSE;
    }
    return object->hidden;
}

Id object_get_open(Object* object){
    if (!object) {
        return NO_ID;
    }
    return object->open;
}

BOOL object_get_illuminate(Object* object){
    if (!object) {
        return FALSE;
    }
    return object->illuminate;
}

BOOL object_get_switchedOn(Object* object){
    if (!object) {
        return FALSE;
    }
    return object->switchedOn;
}

STATUS object_set_name(Object* object, char* name) {
    if (!object || !name) {
        return ERROR;
    }

    if (!strcpy(object->name, name)) {
        return ERROR;
    }

    return OK;
}

STATUS object_set_description(Object* object, char* description) {
    if (!object || !description) {
        return ERROR;
    }

    if (!strcpy(object->description, description)) {
        return ERROR;
    }

    return OK;
}

STATUS object_set_description2(Object* object, char* description2) {
    if (!object || !description2) {
        return ERROR;
    }

    if (!strcpy(object->description2, description2)) {
        return ERROR;
    }

    return OK;
}

STATUS object_set_location(Object* object, Id id) {
    if (!object) {
        return ERROR;
    }
    object->location = id;

    return OK;
}

STATUS object_set_movable(Object* object, BOOL b){
    if (!object) {
        return ERROR;
    }
    object->movable = b;
    return OK;
}

STATUS object_set_moved(Object* object, BOOL b){
    if (!object) {
        return ERROR;
    }
    object->moved = b;
    return OK;
}

STATUS object_set_hidden(Object* object, BOOL b){
    if (!object) {
        return ERROR;
    }
    object->hidden = b;
    return OK;
}

STATUS object_set_open(Object* object, Id id){
    if (!object) {
        return ERROR;
    }
    object->open = id;
    return OK;
}

STATUS object_set_illuminate(Object* object, BOOL b){
    if (!object) {
        return ERROR;
    }
    object->illuminate = b;
    return OK;
}

STATUS object_set_switchedOn(Object* object, BOOL b){
    if (!object) {
        return ERROR;
    }
    object->switchedOn = b;
    return OK;
}

STATUS object_print(Object* object, FILE* f) {

    if (!object || !f) {
        return ERROR;
    }

    fprintf(f, "#o:%ld|%s|%ld|%d|%d|%d|%ld|%d|%d|%s|%s|\n", object->id, object->name, object->location, object->movable, object->moved, object->hidden, object->open, object->illuminate, object->switchedOn, object->description, object->description2);
    return OK;
}
