/** 
 * @brief It defines the game reader
 * 
 * @file game_reader.c
 * @author Group 1
 * @version 3.0
 * @date 04-04-2018 
 * @copyright GNU Public License
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/game_reader.h"

STATUS game_reader_load_spaces(Game* game, char* filename) {
    FILE* file = NULL;
    char line[WORD_SIZE] = "";
    char name[WORD_SIZE] = "";
    char description[WORD_SIZE] = "";
    char descriptioff[WORD_SIZE] = "";
    char f1[8];
    char f2[8];
    char f3[8];
    char* toks = NULL;
    Id id = NO_ID, north = NO_ID, east = NO_ID, south = NO_ID, west = NO_ID, up = NO_ID, down = NO_ID;
    Space* space = NULL;
    STATUS status = OK;
    BOOL illumination = TRUE;

    if (!filename) {
        return ERROR;
    }

    file = fopen(filename, "r");
    if (file == NULL) {
        return ERROR;
    }

    while (fgets(line, WORD_SIZE, file)) {
        if (strncmp("#s:", line, 3) == 0) {
            toks = strtok(line + 3, "|");
            id = atol(toks);
            toks = strtok(NULL, "|");
            strcpy(name, toks);
            toks = strtok(NULL, "|");
            north = atol(toks);
            toks = strtok(NULL, "|");
            east = atol(toks);
            toks = strtok(NULL, "|");
            south = atol(toks);
            toks = strtok(NULL, "|");
            west = atol(toks);
            toks = strtok(NULL, "|");
            up = atol(toks);
            toks = strtok(NULL, "|");
            down = atol(toks);
            toks = strtok(NULL, "|");
            illumination = atol(toks);
            toks = strtok(NULL, "|");
            strcpy(f1, toks);
            toks = strtok(NULL, "|");
            strcpy(f2, toks);
            toks = strtok(NULL, "|");
            strcpy(f3, toks);
            toks = strtok(NULL, "|");
            strcpy(description, toks);
            toks = strtok(NULL, "|");
            strcpy(descriptioff, toks);
#ifdef DEBUG 
            printf("Leido: %ld|%s|%ld|%ld|%ld|%ld|%ld|%ld|%s|%s|%s|%s\n", id, name, north, east, south, west, up, down f1, f2, f3, description, descriptioff);
#endif

            space = space_create(id);
            if (space != NULL) {
                space_set_name(space, name);
                space_set_north(space, north);
                space_set_east(space, east);
                space_set_south(space, south);
                space_set_west(space, west);
                space_set_up(space, up);
                space_set_down(space, down);
                space_set_illumination(space, illumination);
                space_set_gdesc(space, f1, f2, f3);
                space_set_description(space, description);
                space_set_descriptioff(space, descriptioff);
                game_add_space(game, space);
            }
        }
    }

    if (ferror(file)) {
        status = ERROR;
    }

    fclose(file);

    return status;
}

STATUS game_reader_load_objects(Game* game, char* filename) {
    FILE* file = NULL;
    char name[WORD_SIZE] = "";
    char line[WORD_SIZE] = "";
    char description[WORD_SIZE] = "";
    char* toks = NULL;
    Id id = NO_ID, location = NO_ID;
    Object* object = NULL;
    STATUS status = OK;

    if (!filename) {
        return ERROR;
    }

    file = fopen(filename, "r");
    if (file == NULL) {
        return ERROR;
    }

    while (fgets(line, WORD_SIZE, file)) {
        if (strncmp("#o:", line, 3) == 0) {
            toks = strtok(line + 3, "|");
            id = atol(toks);
            toks = strtok(NULL, "|");
            strcpy(name, toks);
            toks = strtok(NULL, "|");
            location = atol(toks);
            toks = strtok(NULL, "|");
            strcpy(description, toks);
#ifdef DEBUG 
            printf("Leido: %ld|%s|%ld|%s\n", id, name, location, description);
#endif
            object = object_create(id);
            if (object != NULL) {
                object_set_name(object, name);
                object_set_location(object, location);
                object_set_description(object, description);
                game_add_object(game, object);
            }
        }
    }

    if (ferror(file)) {
        status = ERROR;
    }

    fclose(file);

    return status;
}

STATUS game_reader_load_links(Game* game, char* filename) {
    FILE* file = NULL;
    char line[WORD_SIZE] = "";
    char name[WORD_SIZE] = "";
    char* toks = NULL;
    Id id = NO_ID, spaceid1 = NO_ID, spaceid2 = NO_ID;
    Link* link = NULL;
    STATUS linkstat = OK;
    STATUS status = OK;

    if (!filename) {
        return ERROR;
    }

    file = fopen(filename, "r");
    if (file == NULL) {
        return ERROR;
    }

    while (fgets(line, WORD_SIZE, file)) {
        if (strncmp("#l:", line, 3) == 0) {
            toks = strtok(line + 3, "|");
            id = atol(toks);
            toks = strtok(NULL, "|");
            strcpy(name, toks);
            toks = strtok(NULL, "|");
            spaceid1 = atol(toks);
            toks = strtok(NULL, "|");
            spaceid2 = atol(toks);
            toks = strtok(NULL, "|");
            linkstat = atoi(toks);
#ifdef DEBUG 
            printf("Leido: %ld|%s|%ld|%ld|%d|%d\n", id, name, spaceid1, spaceid2, linkstat);
#endif

            link = link_create();
            if (link != NULL) {
                link_set_id(link, id);
                link_set_name(link, name);
                link_set_spaces(link, spaceid1, spaceid2);
                link_set_status(link, linkstat);
                game_add_link(game, link);
            }
        }
    }

    if (ferror(file)) {
        status = ERROR;
    }

    fclose(file);

    return status;
}
