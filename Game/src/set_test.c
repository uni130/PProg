/** 
 * @brief Tests if the set works correctly
 * 
 * @file set.h
 * @author Nicolas Serrano
 * @version 1.0 
 * @date 15-02-2018
 * @copyright GNU Public License
 */

#include <stdlib.h>
#include <stdio.h>
#include "../include/set.h"

int main() {

    Set *s = NULL;
    int i;

    s = set_create();
    if (!s) {
        return -1;
    }

    /*Print all values inside Set, It is empty */
    if (set_print_values(s) == ERROR) {
        set_destroy(s);
        return -1;
    }
    /*In every loop, add a new Id to Set and print all the values inside, while Set is not full*/
    for (i = 0; i < MAX_ID; i++) {
        if (set_add_value(s, i) == ERROR) {
            set_destroy(s);
            return -1;
        }

    }
    if (set_print_values(s) == ERROR) {
        set_destroy(s);
        return -1;
    }

    /*Tries to add a new Id but it is full, should return ERROR*/
    if (set_add_value(s, 100) != ERROR) {
        set_destroy(s);
        return -1;
    }

    /*Deletes some Id from Set */
    if (set_del_value(s, 1) == ERROR) {
        set_destroy(s);
        return -1;
    }

    if (set_del_value(s, 5) == ERROR) {
        set_destroy(s);
        return -1;
    }

    if (set_del_value(s, 32) == ERROR) {
        set_destroy(s);
        return -1;
    }


    if (set_print_values(s) == ERROR) {
        set_destroy(s);
        return -1;
    }

    /*Tries to add an existing Id, should return ERROR*/
    if (set_add_value(s, 2) != ERROR) {
        set_destroy(s);
        return -1;
    }

    set_destroy(s);
    return 0;
}
