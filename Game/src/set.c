/** 
 * @brief It defines the TAD set
 * 
 * @file set.c
 * @author Group 1
 * @version 1.0 
 * @date 15-02-2018
 * @copyright GNU Public License
 */

#include <stdlib.h>
#include <stdio.h>
#include "../include/set.h"

struct _Set {
    Id set[MAX_ID];
    int top;
};

Set* set_create() {
    Set* s = NULL;
    int i;

    s = (Set*) malloc(sizeof (Set));
    if (!s) {
        return NULL;
    }

    for (i = 0; i < MAX_ID; i++) {
        s->set[i] = NO_ID;
    }

    s->top = -1;

    return s;
}

void set_destroy(Set * s) {
    free(s);
}

STATUS set_add_value(Set* s, Id id) {
    int i;

    if (!s || id == NO_ID || s->top >= MAX_ID - 1) {
        return ERROR;
    }

    for (i = 0; i <= s->top; i++) {
        if (id == s->set[i]) {
            return ERROR; /*The id is alredy in Set* */
        }
    }

    s->top++;
    s->set[s->top] = id;
    return OK;
}

STATUS set_del_value(Set* s, Id id) {
    int i;

    if (!s || id == NO_ID || s->top < 0) {
        return ERROR;
    }
    set_print_values(s);
    for (i = 0; i <= (s->top); i++) {
        if (id == s->set[i]) {
            s->set[i] = s->set[s->top];
            s->set[s->top] = NO_ID;
            s->top--;
            set_print_values(s);
            i = s->top; /*Exit from for*/
        }
    }
    return OK;
}

Id set_get_id(Set* s, int posArray) {
    if (!s || posArray < 0) {
        return NO_ID;
    }
    return s->set[posArray];
}

STATUS set_print_values(Set * s) {
    int i;

    if (!s) {
        return ERROR;
    }

    if (s->top == -1) {
        printf("Set is empty\n");
        return OK;
    }

    for (i = 0; i <= (s->top); i++) {
        printf("Id: %ld\n", s->set[i]);
    }
    return OK;
}
