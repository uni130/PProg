/** 
 * @brief It tests space module
 * 
 * @file space_test.c
 * @author Profesores Pprog updated by Miguel Luque
 * @version 1.0 
 * @date 06-04-2018
 * @copyright GNU Public License
 */

#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
#include "../include/space.h"
#include "../include/space_test.h"
#include "../include/test.h"
#include "../include/types.h"
#include "../include/set.h"
#define MAX_TESTS 51

/** 
 * @brief Main function for SPACE unit tests. 
 * 
 * You may execute ALL or a SINGLE test
 *   1.- No parameter -> ALL test are executed 
 *   2.- A number means a particular test (the one identified by that number) 
 *       is executed
 *  
 */
int main(int argc, char** argv) {

    int test = 0;
    int all = 1;

    if (argc < 2) {
        printf("Running all test for module Space:\n");
    } else {
        test = atoi(argv[1]);
        all = 0;
        printf("Running test %d:\t", test);
        if (test < 1 && test > MAX_TESTS) {
            printf("Error: unknown test %d\t", test);
            exit(EXIT_SUCCESS);
        }
    }


    if (all || test == 1) test1_space_create();
    if (all || test == 2) test2_space_create();
    if (all || test == 3) test1_space_set_name();
    if (all || test == 4) test2_space_set_name();
    if (all || test == 5) test3_space_set_name();
    if (all || test == 6) test1_space_set_north();
    if (all || test == 7) test2_space_set_north();
    if (all || test == 8) test1_space_set_south();
    if (all || test == 9) test2_space_set_south();
    if (all || test == 10) test1_space_set_east();
    if (all || test == 11) test2_space_set_east();
    if (all || test == 12) test1_space_set_west();
    if (all || test == 13) test2_space_set_west();
    if (all || test == 14) test1_space_set_up();
    if (all || test == 15) test2_space_set_up();
    if (all || test == 16) test1_space_set_down();
    if (all || test == 17) test2_space_set_down();
    if (all || test == 18) test1_space_get_name();
    if (all || test == 19) test2_space_get_name();
    if (all || test == 20) test1_space_get_objects();
    if (all || test == 21) test2_space_get_objects();
    if (all || test == 22) test1_space_get_north();
    if (all || test == 22) test2_space_get_north();
    if (all || test == 23) test1_space_get_south();
    if (all || test == 24) test2_space_get_south();
    if (all || test == 25) test1_space_get_east();
    if (all || test == 26) test2_space_get_east();
    if (all || test == 27) test1_space_get_west();
    if (all || test == 28) test2_space_get_west();
    if (all || test == 29) test1_space_get_up();
    if (all || test == 30) test2_space_get_up();
    if (all || test == 31) test1_space_get_down();
    if (all || test == 32) test2_space_get_down();
    if (all || test == 33) test1_space_get_id();
    if (all || test == 34) test2_space_get_id();
    if (all || test == 35) test1_space_set_description();
    if (all || test == 36) test2_space_set_description();
    if (all || test == 37) test1_space_get_description();
    if (all || test == 38) test2_space_get_description();
    if (all || test == 39) test1_space_set_descriptioff();
    if (all || test == 40) test2_space_set_descriptioff();
    if (all || test == 41) test1_space_get_descriptioff();
    if (all || test == 42) test2_space_get_descriptioff();
    if (all || test == 43) test1_space_set_gdesc();
    if (all || test == 44) test2_space_set_gdesc();
    if (all || test == 45) test1_space_get_gdesc();
    if (all || test == 46) test2_space_get_gdesc();
    if (all || test == 47) test1_space_set_illumination();
    if (all || test == 48) test2_space_set_illumination();
    if (all || test == 49) test3_space_set_illumination();
    if (all || test == 50) test1_space_get_illumination();
    if (all || test == 51) test2_space_get_illumination();
    PRINT_PASSED_PERCENTAGE;

    return 0;
}

void test1_space_create() {
    Space *s = NULL;
    s = space_create(5);
    PRINT_TEST_RESULT(s != NULL);
    space_destroy(s);
}

void test2_space_create() {
    Space *s;
    s = space_create(4);
    PRINT_TEST_RESULT(space_get_id(s) == 4);
    space_destroy(s);
}

void test1_space_set_name() {
    Space *s;
    s = space_create(5);
    PRINT_TEST_RESULT(space_set_name(s, "hola") == OK);
    space_destroy(s);
}

void test2_space_set_name() {
    Space *s = NULL;
    PRINT_TEST_RESULT(space_set_name(s, "hola") == ERROR);
}

void test3_space_set_name() {
    Space *s;
    s = space_create(5);
    PRINT_TEST_RESULT(space_set_name(s, NULL) == ERROR);
    space_destroy(s);
}

void test1_space_set_north() {
    Space *s;
    s = space_create(5);
    PRINT_TEST_RESULT(space_set_north(s, 4) == OK);
    space_destroy(s);
}

void test2_space_set_north() {
    Space *s = NULL;
    PRINT_TEST_RESULT(space_set_north(s, 4) == ERROR);
}

void test1_space_set_south() {
    Space *s;
    s = space_create(5);
    PRINT_TEST_RESULT(space_set_south(s, 4) == OK);
    space_destroy(s);
}

void test2_space_set_south() {
    Space *s = NULL;
    PRINT_TEST_RESULT(space_set_south(s, 4) == ERROR);
}

void test1_space_set_east() {
    Space *s;
    s = space_create(5);
    PRINT_TEST_RESULT(space_set_east(s, 4) == OK);
    space_destroy(s);
}

void test2_space_set_east() {
    Space *s = NULL;
    PRINT_TEST_RESULT(space_set_east(s, 4) == ERROR);
}

void test1_space_set_west() {
    Space *s;
    s = space_create(5);
    PRINT_TEST_RESULT(space_set_west(s, 4) == OK);
    space_destroy(s);
}

void test2_space_set_west() {
    Space *s = NULL;
    PRINT_TEST_RESULT(space_set_west(s, 4) == ERROR);
}

void test1_space_set_up() {
    Space *s;
    s = space_create(5);
    PRINT_TEST_RESULT(space_set_up(s, 4) == OK);
    space_destroy(s);
}

void test2_space_set_up() {
    Space *s = NULL;
    PRINT_TEST_RESULT(space_set_up(s, 4) == ERROR);
}

void test1_space_set_down() {
    Space *s;
    s = space_create(5);
    PRINT_TEST_RESULT(space_set_down(s, 4) == OK);
    space_destroy(s);
}

void test2_space_set_down() {
    Space *s = NULL;
    PRINT_TEST_RESULT(space_set_down(s, 4) == ERROR);
}

void test1_space_set_object() {
    Space *s;
    s = space_create(1);
    PRINT_TEST_RESULT(space_set_object(s, 1) == OK);
    space_destroy(s);
}

void test2_space_set_object() {
    Space *s = NULL;
    PRINT_TEST_RESULT(space_set_object(s, 1) == ERROR);
}

void test1_space_get_name() {
    Space *s;
    s = space_create(1);
    space_set_name(s, "adios");
    PRINT_TEST_RESULT(strcmp(space_get_name(s), "adios") == 0);
    space_destroy(s);
}

void test2_space_get_name() {
    Space *s = NULL;
    PRINT_TEST_RESULT(space_get_name(s) == NULL);

}

void test1_space_get_objects() {
    Space *s = NULL;
    s = space_create(1);
    PRINT_TEST_RESULT(space_get_objects(s) != NULL);
    space_destroy(s);
}

void test2_space_get_objects() {
    Space *s = NULL;
    PRINT_TEST_RESULT(space_get_objects(s) == NULL);
}

void test1_space_get_north() {
    Space *s;
    s = space_create(5);
    space_set_north(s, 4);
    PRINT_TEST_RESULT(space_get_north(s) == 4);
    space_destroy(s);
}

void test2_space_get_north() {
    Space *s = NULL;
    PRINT_TEST_RESULT(space_get_north(s) == NO_ID);
}

void test1_space_get_south() {
    Space *s;
    s = space_create(5);
    space_set_south(s, 2);
    PRINT_TEST_RESULT(space_get_south(s) == 2);
    space_destroy(s);
}

void test2_space_get_south() {
    Space *s = NULL;
    PRINT_TEST_RESULT(space_get_south(s) == NO_ID);
}

void test1_space_get_east() {
    Space *s;
    s = space_create(5);
    space_set_east(s, 1);
    PRINT_TEST_RESULT(space_get_east(s) == 1);
    space_destroy(s);
}

void test2_space_get_east() {
    Space *s = NULL;
    PRINT_TEST_RESULT(space_get_east(s) == NO_ID);
}

void test1_space_get_west() {
    Space *s;
    s = space_create(5);
    space_set_west(s, 6);
    PRINT_TEST_RESULT(space_get_west(s) == 6);
    space_destroy(s);
}

void test2_space_get_west() {
    Space *s = NULL;
    PRINT_TEST_RESULT(space_get_west(s) == NO_ID);
}

void test1_space_get_up() {
    Space *s;
    s = space_create(5);
    space_set_up(s, 6);
    PRINT_TEST_RESULT(space_get_up(s) == 6);
    space_destroy(s);
}

void test2_space_get_up() {
    Space *s = NULL;
    PRINT_TEST_RESULT(space_get_up(s) == NO_ID);
}

void test1_space_get_down() {
    Space *s;
    s = space_create(5);
    space_set_down(s, 6);
    PRINT_TEST_RESULT(space_get_down(s) == 6);
    space_destroy(s);
}

void test2_space_get_down() {
    Space *s = NULL;
    PRINT_TEST_RESULT(space_get_down(s) == NO_ID);
}

void test1_space_get_id() {
    Space *s;
    s = space_create(25);
    PRINT_TEST_RESULT(space_get_id(s) == 25);
    space_destroy(s);
}

void test2_space_get_id() {
    Space *s = NULL;
    PRINT_TEST_RESULT(space_get_id(s) == NO_ID);
}

void test1_space_set_description() {
    Space *s;
    s = space_create(5);
    PRINT_TEST_RESULT(space_set_description(s, "description") == OK);
    space_destroy(s);
}

void test2_space_set_description() {
    Space *s = NULL;
    PRINT_TEST_RESULT(space_set_description(s, "description") == ERROR);
}

void test1_space_get_description() {
    Space *s;
    s = space_create(5);
    space_set_description(s, "description");
    PRINT_TEST_RESULT(strcmp(space_get_description(s), "description") == 0);
    space_destroy(s);
}

void test2_space_get_description() {
    Space *s = NULL;
    PRINT_TEST_RESULT(space_get_description(s) == ERROR);
}

void test1_space_set_descriptioff() {
    Space *s;
    s = space_create(5);
    PRINT_TEST_RESULT(space_set_descriptioff(s, "description") == OK);
    space_destroy(s);
}

void test2_space_set_descriptioff() {
    Space *s = NULL;
    PRINT_TEST_RESULT(space_set_descriptioff(s, "description") == ERROR);
}

void test1_space_get_descriptioff() {
    Space *s;
    s = space_create(5);
    space_set_descriptioff(s, "description");
    PRINT_TEST_RESULT(strcmp(space_get_descriptioff(s), "description") == 0);
    space_destroy(s);
}

void test2_space_get_descriptioff() {
    Space *s = NULL;
    PRINT_TEST_RESULT(space_get_descriptioff(s) == ERROR);
}


void test1_space_set_gdesc() {
    Space *s;
    s = space_create(5);
    PRINT_TEST_RESULT(space_set_gdesc(s, "aaaaaaa", "bbbbbbb", "ccccccc") == OK);
    space_destroy(s);
}

void test2_space_set_gdesc() {
    Space *s = NULL;
    PRINT_TEST_RESULT(space_set_gdesc(s, "aaaaaaa", "bbbbbbb", "ccccccc") == ERROR);
}

void test1_space_get_gdesc() {
    Space *s;
    s = space_create(5);
    space_set_gdesc(s, "aaaaaaa", "bbbbbbb", "ccccccc");
    PRINT_TEST_RESULT(strcmp(space_get_gdesc_line(s, 0), "aaaaaaa") == 0);
    space_destroy(s);
}

void test2_space_get_gdesc() {
    Space *s = NULL;
    PRINT_TEST_RESULT(space_get_gdesc_line(s, 1) == ERROR);
}

void test1_space_set_illumination(){
    Space *s = NULL;
    s = space_create(5);
    PRINT_TEST_RESULT(space_set_illumination(s, FALSE) == OK);
    space_destroy(s);

}
void test2_space_set_illumination(){
    Space *s = NULL;
    PRINT_TEST_RESULT(space_set_descriptioff(s, FALSE) == ERROR);
    space_destroy(s);
}

void test3_space_set_illumination(){
    Space *s = NULL;
    PRINT_TEST_RESULT(space_set_descriptioff(s, ERROR) == ERROR);
    space_destroy(s);
}

void test1_space_get_illumination(){
    Space *s = NULL;
    s = space_create(1);
    PRINT_TEST_RESULT(space_get_illumination(s) == TRUE);
    space_destroy(s);

}
void test2_space_get_illumination(){
    Space *s = NULL;
    PRINT_TEST_RESULT(space_get_illumination(s) == FALSE);
}
