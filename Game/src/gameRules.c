#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>

#include "../include/gameRules.h"
char *posibilities[N_POS] = {"No rule","teleport"};
/*char *posibilities[N_POS] = {"No rule","teleport", "steal", "path", "restart", "inventorydrop", "night", "gold"};*/
struct _Rule {
    T_Rule rule;
};
Rule* rule_ini() {
    Rule* rule;
    rule = (Rule*) malloc(sizeof (Rule));
    if (!rule) {
        return NULL;
    }
    rule->rule = N_CMD;
    return rule;
}

void rule_destroy(Rule* rule) {
    free(rule);
}

STATUS rule_set_rule(Rule* rule,int i) {
    if(!rule)	return ERROR;
    if(i<0)	return ERROR;
    rule->rule = (i+N_CMD);
    return OK;
}
T_Rule get_rule(Rule* rule) {
    if (!rule) {
        return N_CMD;
    }
    return rule->rule;
}
Rule* rule_copy(const Rule *rule) {
    Rule *aux;
    if (!rule) {
        return NULL;
    }

    aux = (Rule*) malloc(sizeof (Rule));
    if (!aux) {
        return NULL;
    }

    aux->rule = get_rule((Rule *) rule);
    return aux;
}
