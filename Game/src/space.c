/** 
 * @brief It defines TAD space
 * 
 * @file space.c
 * @author Profesores PPROG, Group 1
 * @version 3.0 
 * @date 03-04-2018
 * @copyright GNU Public License
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/space.h"

/*It defines the struct of the TAD Space*/
struct _Space {
    Id id;
    char name[WORD_SIZE + 1];
    Id north;
    Id south;
    Id east;
    Id west;
    Id up;
    Id down;
    BOOL illumination;
    Set* objects;
    char gdesc[NROWS][NCOLS];
    char description[WORD_SIZE + 1];
    char descriptioff[WORD_SIZE + 1];
};

Space* space_create(Id id) {

    Space* newSpace = NULL;

    if (id == NO_ID)
        return NULL;

    newSpace = (Space *) malloc(sizeof (Space));

    if (newSpace == NULL) {
        return NULL;
    }
    newSpace->id = id;

    newSpace->name[0] = '\0';
    newSpace->gdesc[0][0] = '\0';
    newSpace->gdesc[1][0] = '\0';
    newSpace->gdesc[2][0] = '\0';
    newSpace->description[0] = '\0';
    newSpace->descriptioff[0] = '\0';
    newSpace->north = NO_ID;
    newSpace->south = NO_ID;
    newSpace->east = NO_ID;
    newSpace->west = NO_ID;
    newSpace->up = NO_ID;
    newSpace->down = NO_ID;
    newSpace->illumination = TRUE; 
  

    newSpace->objects = set_create();
    if (!newSpace->objects) {
        free(newSpace);
        return NULL;
    }

    return newSpace;
}

STATUS space_destroy(Space* space) {
    if (!space) {
        return ERROR;
    }

    set_destroy(space->objects);
    free(space);
    space = NULL;

    return OK;
}

STATUS space_set_name(Space* space, char* name) {
    if (!space || !name) {
        return ERROR;
    }

    if (!strcpy(space->name, name)) {
        return ERROR;
    }

    return OK;
}

STATUS space_set_description(Space* space, char* description) {
    if (!space || !description) {
        return ERROR;
    }

    if (!strcpy(space->description, description)) {
        return ERROR;
    }

    return OK;
}

STATUS space_set_descriptioff(Space* space, char* description) {
    if (!space || !description) {
        return ERROR;
    }

    if (!strcpy(space->descriptioff, description)) {
        return ERROR;
    }

    return OK;
}


STATUS space_set_north(Space* space, Id id) {
    if (!space || id == NO_ID) {
        return ERROR;
    }
    space->north = id;
    return OK;
}

STATUS space_set_south(Space* space, Id id) {
    if (!space || id == NO_ID) {
        return ERROR;
    }
    space->south = id;
    return OK;
}

STATUS space_set_east(Space* space, Id id) {
    if (!space || id == NO_ID) {
        return ERROR;
    }
    space->east = id;
    return OK;
}

STATUS space_set_west(Space* space, Id id) {
    if (!space || id == NO_ID) {
        return ERROR;
    }
    space->west = id;
    return OK;
}

STATUS space_set_up(Space* space, Id id) {
    if (!space || id == NO_ID) {
        return ERROR;
    }
    space->up = id;
    return OK;
}

STATUS space_set_down(Space* space, Id id) {
    if (!space || id == NO_ID) {
        return ERROR;
    }
    space->down = id;
    return OK;
}

STATUS space_set_illumination(Space* space, BOOL illumination){
    if(!space){
	return ERROR;
    }
    else if(illumination != FALSE && illumination != TRUE){
	return ERROR;
    }
    space->illumination = illumination;
    return OK;
}

STATUS space_set_gdesc(Space* space, char f1[NCOLS], char f2[NCOLS], char f3[NCOLS]) {
    if (!space) return ERROR;
    strcpy(space->gdesc[0], f1);
    strcpy(space->gdesc[1], f2);
    strcpy(space->gdesc[2], f3);
    return OK;
}

char* space_get_gdesc_line(Space* space, const int i) {
    if (!space) return NULL;
    return (space->gdesc[i]);
}

STATUS space_set_object(Space* space, Id id) {
    if (!space) {
        return ERROR;
    }

    if (set_add_value(space->objects, id) == ERROR) {
        return ERROR;
    }

    return OK;
}

const char * space_get_name(Space* space) {
    if (!space) {
        return NULL;
    }
    return space->name;
}

char* space_get_description(Space* space) {
    if (!space) {
        return NULL;
    }
    return space->description;
}

char* space_get_descriptioff(Space* space) {
    if (!space) {
        return NULL;
    }
    return space->descriptioff;
}


Id space_get_id(Space* space) {
    if (!space) {
        return NO_ID;
    }
    return space->id;
}

Id space_get_north(Space* space) {
    if (!space) {
        return NO_ID;
    }
    return space->north;
}

Id space_get_south(Space* space) {
    if (!space) {
        return NO_ID;
    }
    return space->south;
}

Id space_get_east(Space* space) {
    if (!space) {
        return NO_ID;
    }
    return space->east;
}

Id space_get_west(Space* space) {
    if (!space) {
        return NO_ID;
    }
    return space->west;
}

Id space_get_up(Space* space) {
    if (!space) {
        return NO_ID;
    }
    return space->up;
}

Id space_get_down(Space* space) {
    if (!space) {
        return NO_ID;
    }
    return space->down;
}

BOOL space_get_illumination(Space* space) {
    if (!space) {
        return FALSE;
    }
    return space->illumination;
}

Set* space_get_objects(Space* space) {
    Set* aux = NULL;

    if (!space) {
        return NULL;
    }

    aux = space->objects;
    return aux;
}

STATUS space_print(Space* space, FILE *f) {
    if (!space || !f) {
        return ERROR;
    }

    fprintf(f, "#s:%ld|%s|%ld|%ld|%ld|%ld|%ld|%ld|%d|%s|%s|%s|%s|%s|\n", space->id, space->name, space_get_north(space), space_get_east(space), space_get_south(space), space_get_west(space), space_get_up(space), space_get_down(space), space_get_illumination(space), space_get_gdesc_line(space, 0), space_get_gdesc_line(space, 1), space_get_gdesc_line(space, 2), space_get_description(space), space_get_descriptioff(space));

    return OK;
}

