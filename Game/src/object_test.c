/** 
 * @brief It tests object module
 * 
 * @file object_test.c
 * @author Mario Lopez
 * @version 1.0 
 * @updated 06-04-2018
 * @copyright GNU Public License
 */

#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
#include "../include/object_test.h"

#define MAX_TESTS 46

/** 
 * @brief Main function for object unit tests. 
 * 
 * You may execute ALL or a SINGLE test
 *   1.- No parameter -> ALL test are executed 
 *   2.- A number means a particular test (the one identified by that number) 
 *       is executed
 *  
 */
int main(int argc, char** argv) {

    int test = 0;
    int all = 1;

    if (argc < 2) {
        printf("Running all test for module object:\n");
    } else {
        test = atoi(argv[1]);
        all = 0;
        printf("Running test %d:\t", test);
        if (test < 1 && test > MAX_TESTS) {
            printf("Error: unknown test %d\t", test);
            exit(EXIT_SUCCESS);
        }
    }


    if (all || test == 1) test1_object_create();
    if (all || test == 2) test2_object_create();
    if (all || test == 3) test1_object_destroy();
    if (all || test == 4) test2_object_destroy();
    if (all || test == 5) test1_object_set_name();
    if (all || test == 6) test2_object_set_name();
    if (all || test == 7) test1_object_set_description();
    if (all || test == 8) test2_object_set_description();
    if (all || test == 9) test1_object_set_location();
    if (all || test == 10) test2_object_set_location();
    if (all || test == 11) test1_object_set_description2();
    if (all || test == 12) test2_object_set_description2();
    if (all || test == 13) test1_object_set_movable();
    if (all || test == 14) test2_object_set_movable();
    if (all || test == 15) test1_object_set_moved();
    if (all || test == 16) test2_object_set_moved();
    if (all || test == 17) test1_object_set_hidden();
    if (all || test == 18) test2_object_set_hidden();
    if (all || test == 19) test1_object_set_open();
    if (all || test == 20) test2_object_set_open();
    if (all || test == 21) test1_object_set_illuminate();
    if (all || test == 22) test2_object_set_illuminate();
    if (all || test == 23) test1_object_set_switchedOn();
    if (all || test == 24) test2_object_set_switchedOn();
    if (all || test == 25) test1_object_get_id();
    if (all || test == 26) test2_object_get_id();
    if (all || test == 27) test1_object_get_name();
    if (all || test == 28) test2_object_get_name();
    if (all || test == 29) test1_object_get_description();
    if (all || test == 30) test2_object_get_description();
    if (all || test == 31) test1_object_get_location();
    if (all || test == 32) test2_object_get_location();
    if (all || test == 33) test1_object_get_description2();
    if (all || test == 34) test2_object_get_description2();
    if (all || test == 35) test1_object_get_movable();
    if (all || test == 36) test2_object_get_movable();
    if (all || test == 37) test1_object_get_moved();
    if (all || test == 38) test2_object_get_moved();
    if (all || test == 39) test1_object_get_hidden();
    if (all || test == 40) test2_object_get_hidden();
    if (all || test == 41) test1_object_get_open();
    if (all || test == 42) test2_object_get_open();
    if (all || test == 43) test1_object_get_illuminate();
    if (all || test == 44) test2_object_get_illuminate();
    if (all || test == 45) test1_object_get_switchedOn();
    if (all || test == 46) test2_object_get_switchedOn();

    PRINT_PASSED_PERCENTAGE;

    return 0;
}

void test1_object_create() {
    Object *i;
    i = object_create(1);
    PRINT_TEST_RESULT(i != NULL);
    object_destroy(i);
}

void test2_object_create() {
    Object *i;
    i = object_create(NO_ID);
    PRINT_TEST_RESULT(i == NULL);
    object_destroy(i);
}

void test1_object_destroy() {
    Object *i;
    i = object_create(1);
    PRINT_TEST_RESULT(object_destroy(i) == OK);
}

void test2_object_destroy() {
    Object *i = NULL;
    PRINT_TEST_RESULT(object_destroy(i) == ERROR);
}

void test1_object_set_name() {
    Object *i;
    i = object_create(1);
    PRINT_TEST_RESULT(object_set_name(i, "test") == OK);
    object_destroy(i);
}

void test2_object_set_name() {
    Object *i;
    i = object_create(1);
    PRINT_TEST_RESULT(object_set_name(i, NULL) == ERROR);
    object_destroy(i);
}

void test1_object_set_description() {
    Object *i;
    i = object_create(1);
    PRINT_TEST_RESULT(object_set_description(i, "test") == OK);
    object_destroy(i);
}

void test2_object_set_description() {
    Object *i;
    i = object_create(1);
    PRINT_TEST_RESULT(object_set_description(i, NULL) == ERROR);
    object_destroy(i);
}

void test1_object_set_location() {
    Object *i;
    i = object_create(1);
    PRINT_TEST_RESULT(object_set_location(i, 4) == OK);
    object_destroy(i);
}

void test2_object_set_location() {
    Object *i = NULL;
    PRINT_TEST_RESULT(object_set_location(i, NO_ID) == ERROR);
}

void test1_object_set_description2() {
    Object *i;
    i = object_create(1);
    PRINT_TEST_RESULT(object_set_description2(i, "test") == OK);
    object_destroy(i);
}

void test2_object_set_description2() {
    Object *i = NULL;
    PRINT_TEST_RESULT(object_set_description2(i, "test") == ERROR);
}

void test1_object_set_movable() {
    Object *i;
    i = object_create(1);
    PRINT_TEST_RESULT(object_set_movable(i, TRUE) == OK);
    object_destroy(i);
}

void test2_object_set_movable() {
    Object *i = NULL;
    PRINT_TEST_RESULT(object_set_movable(i, TRUE) == ERROR);
}

void test1_object_set_moved() {
    Object *i;
    i = object_create(1);
    PRINT_TEST_RESULT(object_set_moved(i, TRUE) == OK);
    object_destroy(i);
}

void test2_object_set_moved() {
    Object *i = NULL;
    PRINT_TEST_RESULT(object_set_moved(i, TRUE) == ERROR);
}

void test1_object_set_hidden() {
    Object *i;
    i = object_create(1);
    PRINT_TEST_RESULT(object_set_hidden(i, TRUE) == OK);
    object_destroy(i);
}

void test2_object_set_hidden() {
    Object *i = NULL;
    PRINT_TEST_RESULT(object_set_hidden(i, TRUE) == ERROR);
}

void test1_object_set_open() {
    Object *i;
    i = object_create(1);
    PRINT_TEST_RESULT(object_set_open(i, 3) == OK);
    object_destroy(i);
}

void test2_object_set_open() {
    Object *i = NULL;
    PRINT_TEST_RESULT(object_set_open(i, 3) == ERROR);
}

void test1_object_set_illuminate() {
    Object *i;
    i = object_create(1);
    PRINT_TEST_RESULT(object_set_illuminate(i, TRUE) == OK);
    object_destroy(i);
}

void test2_object_set_illuminate() {
    Object *i = NULL;
    PRINT_TEST_RESULT(object_set_illuminate(i, TRUE) == ERROR);
}

void test1_object_set_switchedOn() {
    Object *i;
    i = object_create(1);
    PRINT_TEST_RESULT(object_set_switchedOn(i, TRUE) == OK);
    object_destroy(i);
}

void test2_object_set_switchedOn() {
    Object *i = NULL;
    PRINT_TEST_RESULT(object_set_switchedOn(i, TRUE) == ERROR);
}

void test1_object_get_id() {
    Object *i;
    i = object_create(1);
    PRINT_TEST_RESULT(object_get_id(i) == OK);
    object_destroy(i);
}

void test2_object_get_id() {
    Object *i = NULL;
    PRINT_TEST_RESULT(object_get_id(i) == NO_ID);
}

void test1_object_get_name() {
    Object *i;
    i = object_create(1);
    object_set_name(i, "test");
    PRINT_TEST_RESULT(strcmp(object_get_name(i), "test") == 0);
    object_destroy(i);

}

void test2_object_get_name() {
    Object *i = NULL;
    PRINT_TEST_RESULT(object_get_name(i) == NULL);
}

void test1_object_get_location() {
    Object *i;
    i = object_create(1);
    object_set_location(i, 1);
    PRINT_TEST_RESULT(object_get_location(i) == 1);
    object_destroy(i);
}

void test2_object_get_location() {
    Object *i = NULL;
    PRINT_TEST_RESULT(object_get_location(i) == NO_ID);
}

void test1_object_get_description() {
    Object *i;
    i = object_create(1);
    object_set_description(i, "test");
    PRINT_TEST_RESULT(strcmp(object_get_description(i), "test") == 0);
    object_destroy(i);

}

void test2_object_get_description() {
    Object *i = NULL;
    PRINT_TEST_RESULT(object_get_description(i) == NULL);
}

void test1_object_get_description2() {
    Object *i;
    i = object_create(1);
    object_set_description2(i, "test");
    PRINT_TEST_RESULT(strcmp(object_get_description2(i), "test") == 0);
    object_destroy(i);
}

void test2_object_get_description2() {
    Object *i = NULL;
    PRINT_TEST_RESULT(object_get_description2(i) == NULL);
}

void test1_object_get_movable() {
    Object *i;
    i = object_create(1);
    object_set_movable(i, TRUE);
    PRINT_TEST_RESULT(object_get_movable(i) == TRUE);
    object_destroy(i);
}

void test2_object_get_movable() {
    Object *i = NULL;
    PRINT_TEST_RESULT(object_get_movable(i) == FALSE);
}

void test1_object_get_moved() {
    Object *i;
    i = object_create(1);
    object_set_moved(i, TRUE);
    PRINT_TEST_RESULT(object_get_moved(i) == TRUE);
    object_destroy(i);
}

void test2_object_get_moved() {
    Object *i = NULL;
    PRINT_TEST_RESULT(object_get_moved(i) == FALSE);
}

void test1_object_get_hidden() {
    Object *i;
    i = object_create(1);
    object_set_hidden(i, TRUE);
    PRINT_TEST_RESULT(object_get_hidden(i) == TRUE);
    object_destroy(i);
}

void test2_object_get_hidden() {
    Object *i = NULL;
    PRINT_TEST_RESULT(object_get_hidden(i) == FALSE);
}

void test1_object_get_open() {
    Object *i;
    i = object_create(1);
    object_set_open(i, 3);
    PRINT_TEST_RESULT(object_get_open(i) == 3);
    object_destroy(i);
}

void test2_object_get_open() {
    Object *i = NULL;
    PRINT_TEST_RESULT(object_get_open(i) == NO_ID);
}

void test1_object_get_illuminate() {
    Object *i;
    i = object_create(1);
    object_set_illuminate(i, TRUE);
    PRINT_TEST_RESULT(object_get_illuminate(i) == TRUE);
    object_destroy(i);
}

void test2_object_get_illuminate() {
    Object *i = NULL;
    PRINT_TEST_RESULT(object_get_illuminate(i) == FALSE);
}

void test1_object_get_switchedOn() {
    Object *i;
    i = object_create(1);
    object_set_switchedOn(i, TRUE);
    PRINT_TEST_RESULT(object_get_switchedOn(i) == TRUE);
    object_destroy(i);
}

void test2_object_get_switchedOn() {
    Object *i = NULL;
    PRINT_TEST_RESULT(object_get_switchedOn(i) == FALSE);
}
