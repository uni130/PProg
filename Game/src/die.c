 /**
 * @brief It implements the die logic
 * 
 * @file die.h
 * @author Group 1
 * @version 2.0
 * @date 16-02-2018 
 * @copyright GNU Public License
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "../include/die.h"

struct _Die {
    Id id;
    int num;
};

int die_random_num(int a, int b) {
    if (b < a) {
        return -1;
    }

    return (int) (a + (double) rand() / (RAND_MAX - 1)*(b - a + 1));
}



Die* die_create(Id id) {
    Die * d;

    if (id == NO_ID) {
        return NULL;
    }

    d = (Die*) malloc(sizeof (Die));
    if (!d) {
        return NULL;
    }
    d->id = id;
    d->num = 0;
    srand(time(NULL));

    return d;
}

void die_destroy(Die* d) {
    free(d);
}

void die_roll(Die* d) {
    if (!d) {
        return;
    }

    d->num = die_random_num(1, 6);
}

int die_value(Die * d) {
    if (!d) {
        return -1;
    }
    return d->num;
}

STATUS die_print(Die *d) {
    if (!d) {
        return ERROR;
    }
    printf("Id: %ld, number: %d\n", d->id, d->num);
    return OK;
}
