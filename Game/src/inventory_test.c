/** 
 * @brief It tests inventory module
 * 
 * @file inventory_test.c
 * @author Mario Lopez
 * @version 1.0 
 * @date 05-04-2018
 * @copyright GNU Public License
 */

#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
#include "../include/inventory_test.h"

#define MAX_TESTS 11

/** 
 * @brief Main function for INVENTORY unit tests. 
 * 
 * You may execute ALL or a SINGLE test
 *   1.- No parameter -> ALL test are executed 
 *   2.- A number means a particular test (the one identified by that number) 
 *       is executed
 *  
 */
int main(int argc, char** argv) {

    int test = 0;
    int all = 1;

    if (argc < 2) {
        printf("Running all test for module Inventory:\n");
    } else {
        test = atoi(argv[1]);
        all = 0;
        printf("Running test %d:\t", test);
        if (test < 1 && test > MAX_TESTS) {
            printf("Error: unknown test %d\t", test);
            exit(EXIT_SUCCESS);
        }
    }


    if (all || test == 1) test1_inventory_create();
    if (all || test == 2) test1_inventory_destroy();
    if (all || test == 3) test2_inventory_destroy();
    if (all || test == 4) test1_inventory_set_id();
    if (all || test == 5) test2_inventory_set_id();
    if (all || test == 6) test1_inventory_set_maxObjects();
    if (all || test == 7) test2_inventory_set_maxObjects();
    if (all || test == 8) test1_inventory_get_ids();
    if (all || test == 9) test2_inventory_get_ids();
    if (all || test == 10) test1_inventory_get_maxObjects();
    if (all || test == 11) test2_inventory_get_maxObjects();

    PRINT_PASSED_PERCENTAGE;

    return 0;
}

void test1_inventory_create() {
    Inventory *i;
    i = inventory_create();
    PRINT_TEST_RESULT(i != NULL);
    inventory_destroy(i);
}

void test1_inventory_destroy() {
    Inventory *i;
    i = inventory_create();
    PRINT_TEST_RESULT(inventory_destroy(i) == OK);
}

void test2_inventory_destroy() {
    Inventory *i = NULL;
    PRINT_TEST_RESULT(inventory_destroy(i) == ERROR);
}

void test1_inventory_set_id() {
    Inventory *i;
    i = inventory_create();
    PRINT_TEST_RESULT(inventory_set_id(i, 1) == OK);
    inventory_destroy(i);
}

void test2_inventory_set_id() {
    Inventory *i;
    i = inventory_create();
    PRINT_TEST_RESULT(inventory_set_id(i, NO_ID) == ERROR);
    inventory_destroy(i);
}

void test1_inventory_set_maxObjects() {
    Inventory *i;
    i = inventory_create();
    PRINT_TEST_RESULT(inventory_set_maxObjects(i, 4) == OK);
    inventory_destroy(i);
}

void test2_inventory_set_maxObjects() {
    Inventory *i;
    i = inventory_create();
    PRINT_TEST_RESULT(inventory_set_maxObjects(i, -1) == ERROR);
    inventory_destroy(i);
}

void test1_inventory_get_ids() {
    Inventory *i;
    i = inventory_create();
    PRINT_TEST_RESULT(inventory_get_ids(i) != NULL);
    inventory_destroy(i);
}

void test2_inventory_get_ids() {
    Inventory *i = NULL;
    PRINT_TEST_RESULT(inventory_get_ids(i) == ERROR);
}

void test1_inventory_get_maxObjects() {
    Inventory *i;
    i = inventory_create();
    inventory_set_id(i, 2);
    inventory_set_maxObjects(i, 4);
    PRINT_TEST_RESULT(inventory_get_maxObjects(i) == 4);
    inventory_destroy(i);

}

void test2_inventory_get_maxObjects() {
    Inventory *i = NULL;
    PRINT_TEST_RESULT(inventory_get_maxObjects(i) == -1);
}


