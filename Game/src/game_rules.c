/** 
 * @brief It implements the game_rules logic
 * 
 * @file game_rules.c
 * @author Miguel Luque
 * @version 1.0
 * @date 03-05-2018
 * @copyright GNU Public License
 */

#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>

#include "../include/game_rules.h"
char *posibilities[N_POS] = {"No rule","teleport","busted","path","find","lose","night"};
struct _Rule {
    T_Rule rule;
};
Rule* game_rules_ini() {
    Rule* rule;
    rule = (Rule*) malloc(sizeof (Rule));
    if (!rule) {
        return NULL;
    }
    rule->rule = N_CMD;
    return rule;
}

void game_rules_destroy(Rule* rule) {
    free(rule);
}

STATUS game_rules_set(Rule* rule, int i) {
    if(!rule)	return ERROR;
    if(i<0)	return ERROR;
    rule->rule = (i+N_CMD+1);
    return OK;
}
T_Rule game_rules_get(Rule* rule) {
    if (!rule) {
        return N_CMD;
    }
    return rule->rule;
}
Rule* game_rules_copy(const Rule *rule) {
    Rule *aux;
    if (!rule) {
        return NULL;
    }

    aux = (Rule*) malloc(sizeof (Rule));
    if (!aux) {
        return NULL;
    }

    aux->rule = game_rules_get((Rule *) rule);
    return aux;
}
