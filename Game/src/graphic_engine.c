/** 
 * @brief It defines a textual graphic engine
 * 
 * @file graphic_engine.c
 * @author Profesores PPROG Group 1
 * @version 3.0
 * @date 08-04-2018
 * @copyright GNU Public License
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "../include/screen.h"
#include "../include/graphic_engine.h"
#include "../include/object.h"
#include "../include/game.h"
#include "../include/player.h"
#include "../include/inventory.h"
#include "../include/dialogue.h"

/*Defines the components viewed on the screen by the user*/
struct _Graphic_engine {
    Area *map, *descript, *banner, *help, *feedback, *description;
};

Graphic_engine *graphic_engine_create() {
    static Graphic_engine *ge = NULL;

    if (ge)
        return ge;

    screen_init();
    ge = (Graphic_engine *) malloc(sizeof (Graphic_engine));

    ge->map = screen_area_init(1, 1, 101, 23);
    ge->descript = screen_area_init(103, 1, 37, 23);
    ge->banner = screen_area_init(55, 25, 23, 1);
    ge->help = screen_area_init(1, 26, 140, 2);
    ge->feedback = screen_area_init(1, 29, 140, 3);
    ge->description = screen_area_init(1, 33, 140, 3);
   
    return ge;
}

void graphic_engine_destroy(Graphic_engine *ge) {
    if (!ge)
        return;

    screen_area_destroy(ge->map);
    screen_area_destroy(ge->descript);
    screen_area_destroy(ge->banner);
    screen_area_destroy(ge->help);
    screen_area_destroy(ge->feedback);
    screen_area_destroy(ge->description);
    
    screen_destroy();
    free(ge);
}

void graphic_engine_paint_game(Graphic_engine *ge, Game *game, FILE* log) {
    Id id_act = NO_ID, id_back = NO_ID, id_next = NO_ID, obj_loc = NO_ID, current_object = NO_ID;
    Space *space_act = NULL, *space_back = NULL, *space_next = NULL;
    char obj_back[MAX_OBJ + 1][3];
    char obj_act[MAX_OBJ + 1][3];
    char obj_next[MAX_OBJ + 1][3];
    char str[WORD_SIZE];
    char right_sim[4] = "   ";
    char left_sim[4] = "   ";
    char right_name[WORD_SIZE] = " ";
    char left_name[WORD_SIZE] = "         ";
    Command* cmd = NULL;
    T_Command last_cmd = UNKNOWN;
    extern char *cmd_to_str[];
    int i, j;
    int counter = 0;

    /* Paint the in the map area */
    screen_area_clear(ge->map);
    id_act = game_get_player_location(game);
    if ((space_act = game_get_space(game, id_act)) != NULL && ((id_act < 90 || id_act > 99) && (id_act != 63 && id_act != 106))) {
        
	id_back = link_get_space_id(game_get_link(game, space_get_north(space_act)), 1);
        id_next = link_get_space_id(game_get_link(game, space_get_south(space_act)), 2);
	space_back = game_get_space(game, id_back);
	space_next = game_get_space(game, id_next);
        for (i = 0; i < MAX_OBJ; i++) {
            if (game_get_object(game, i) != NULL) {
                if (game_get_object_location(game_get_object(game, i)) == id_back) {
                    strcpy(obj_back[i], object_get_name(game_get_object(game, i)));
                } else {
                    strcpy(obj_back[i], "  ");
                }
            }
        }
        if (id_back != NO_ID) {
            sprintf(str, "                         +-----------+ ");
            screen_area_puts(ge->map, str);
	    sprintf(str, "                         |           |");
            screen_area_puts(ge->map, str);
            sprintf(str, "                         |  %s  |", space_get_gdesc_line(space_back, 0));
            screen_area_puts(ge->map, str);
            sprintf(str, "                         |  %s  |", space_get_gdesc_line(space_back, 1));
            screen_area_puts(ge->map, str);
            sprintf(str, "                         |  %s  |", space_get_gdesc_line(space_back, 2));
            screen_area_puts(ge->map, str);
            sprintf(str, "                         |           |");
            screen_area_puts(ge->map, str);
            sprintf(str, "                         +-----------+");
            screen_area_puts(ge->map, str);
            sprintf(str, "                               ^ %s", space_get_name(game_get_space(game, link_get_space_id(game_get_link(game, space_get_north(space_act)), 1))));;
            screen_area_puts(ge->map, str);
        }
	else {
            sprintf(str, " ");
            screen_area_puts(ge->map, str);
            sprintf(str, "  ");
            screen_area_puts(ge->map, str);
            sprintf(str, "  ");
            screen_area_puts(ge->map, str);
            sprintf(str, "  ");
            screen_area_puts(ge->map, str);
            sprintf(str, "  ");
            screen_area_puts(ge->map, str);
            sprintf(str, "  ");
            screen_area_puts(ge->map, str);
            sprintf(str, "  ");
            screen_area_puts(ge->map, str);
	    sprintf(str, " ");
	    screen_area_puts(ge->map, str);
	    sprintf(str, " ");
        }

        for (i = 0; i < MAX_OBJ; i++) {
            if (game_get_object(game, i) != NULL) {
                if (game_get_object_location(game_get_object(game, i)) == id_act) {
                    strcpy(obj_act[i], object_get_name(game_get_object(game, i)));
                } else {
                    strcpy(obj_act[i], "  ");
                }
            }
        }
        if (link_get_space_id(game_get_link(game, space_get_west(space_act)), 2) > 0) {
            strcpy(right_sim, "-->");
            sprintf(right_name, "%s", space_get_name(game_get_space(game, link_get_space_id(game_get_link(game, space_get_west(space_act)), 2))));
        }
        if (link_get_space_id(game_get_link(game, space_get_east(space_act)), 1) > 0) {
            strcpy(left_sim, "<--");
            sprintf(left_name, "%s", space_get_name(game_get_space(game, link_get_space_id(game_get_link(game, space_get_east(space_act)), 1))));
            
        }
        if (id_act != NO_ID) {
            sprintf(str, "                         +-----------+");
            screen_area_puts(ge->map, str);
            sprintf(str, "            %s %s| 8D        |%s %s", left_name, left_sim, right_sim, right_name);
            screen_area_puts(ge->map, str);
            sprintf(str, "                         |  %s  |", space_get_gdesc_line(space_act, 0));
            screen_area_puts(ge->map, str);
            sprintf(str, "                         |  %s  |", space_get_gdesc_line(space_act, 1));
            screen_area_puts(ge->map, str);
            sprintf(str, "                         |  %s  |", space_get_gdesc_line(space_act, 2));
            screen_area_puts(ge->map, str);
            sprintf(str, "                         |           |");
            screen_area_puts(ge->map, str);
            sprintf(str, "                         +-----------+");
            screen_area_puts(ge->map, str);
        }

        for (i = 0; i < MAX_OBJ; i++) {
            if (game_get_object(game, i) != NULL) {
                if (game_get_object_location(game_get_object(game, i)) == id_next) {
                    strcpy(obj_next[i], object_get_name(game_get_object(game, i)));
                } else {
                    strcpy(obj_next[i], "  ");
                }
            }
        }
        if (id_next != NO_ID) {
            sprintf(str, "                               v %s", space_get_name(game_get_space(game, link_get_space_id(game_get_link(game, space_get_south(space_act)), 2))));
            screen_area_puts(ge->map, str);
           sprintf(str, "                         +-----------+ ");
            screen_area_puts(ge->map, str);
	    sprintf(str, "                         |           |");
            screen_area_puts(ge->map, str);
            sprintf(str, "                         |  %s  |", space_get_gdesc_line(space_next, 0));
            screen_area_puts(ge->map, str);
            sprintf(str, "                         |  %s  |", space_get_gdesc_line(space_next, 1));
            screen_area_puts(ge->map, str);
            sprintf(str, "                         |  %s  |", space_get_gdesc_line(space_next, 2));
            screen_area_puts(ge->map, str);
            sprintf(str, "                         |           |");
            screen_area_puts(ge->map, str);
            sprintf(str, "                         +-----------+");
            screen_area_puts(ge->map, str);
        }
    }
    else if(id_act == 99){
            sprintf(str, "+--------------------------------------------------------------------------------------------------+");
            screen_area_puts(ge->map, str);
            sprintf(str, "+  _____                                                                                   _...--. +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+ / ____|                                                                  _____......----'     .' +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+| |  __  ___   ___  ___  ___                                        _..-''                   .'   +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+| | |_ |/ _ \\ / _ \\/ __|/ _ \\                                     .'                       ./     +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+| |__| | (_) | (_) \\__ \\  __/                             _.--._.'                       .' |     +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+ \\_____|\\___/ \\___/|___/\\___|                          .-'                           .-.'  /      +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                                     .'   _.-.                     .  \\   '       +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+         _____             _                       .'  .'   .'    _    .-.        / `./  :        +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+        / ____|           | |                    .'  .'   .'  .--' `.  |  \\  |`. |     .'         +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+       | (___   ___  _   _| |___              _.'  .'   .' `.'       `-'   \\ / |.'   .'           +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+        \\___ \\ / _ \\| | | | / __|          _.'  .-'   .'     `-.            `      .'             +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+        ____) | (_) | |_| | \\__ \\        .'   .'    .'          `-.._ _ _ _ .-.    :              +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+       |_____/ \\___/ \\__,_|_|___/       /    /o _.-'               .--'   .'   \\   |              +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                      .'-.__..-'                  /..    .`    / .'               +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+ Nicolas Serrano                    .'   . '                       /.'/.'     /  |                +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+ Mario Lopez                       `---'                                   _.'   '                +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+ Miguel Luque                                                            /.'  _.'     IGN: 10/10  +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+ Javier Sanmiguel                                                       /.'/.'   PCGamer:100/100  +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+--------------------------------------------------------------------------------------------------+"); 
            screen_area_puts(ge->map, str);


         
    }
    
    else if(id_act == 98){
	    sprintf(str, "+--------------------------------------------------------------------------------------------------+"); 
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                               ,---------------------------------.-------.        +"); 
            screen_area_puts(ge->map, str);
            sprintf(str, "+  _______ _                                    |   ,-------------------------.   |   .   |        +"); 
            screen_area_puts(ge->map, str);
            sprintf(str, "+ |__   __| |                                   |   |                         |   |   |   |        +"); 
            screen_area_puts(ge->map, str);
            sprintf(str, "+    | |  | |__   ___                           |   |   ,-----------------.   |   |   |   |        +"); 
            screen_area_puts(ge->map, str);
            sprintf(str, "+    | |  | '_ \\ / _ \\                          |   |   |                 |   |   |   |   |        +"); 
            screen_area_puts(ge->map, str);
            sprintf(str, "+    | |  | | | |  __/                          |   |   `---    ,----     |   |   |   |   |        +"); 
            screen_area_puts(ge->map, str);
            sprintf(str, "+    |_|  |_| |_|\\___|                          |   |           | X       |   |   |   |   |        +"); 
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                               |   |   ,-------\"---------:   |   `---'   |        +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+          __  __                               |   |   |                 |   |           |        +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+         |  \\/  |                              |   `---:   ,--------.    |   `-------.   |        +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+         | \\  / | __ _ _______                 |   .   |   |   .    |    |    -------'   |        +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+         | |\\/| |/ _` |_  / _ \\                |   |   |   |   |    |    |               |        +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+         | |  | | (_| |/ /  __/                :---'   |   |   |    |    |   ,-----------:        +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+         |_|  |_|\\__,_/___\\___|                |       |   |   |    |    |   |           |        +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                               |   .   |   `---'    |    |   |    ---.   |        +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                               |   |   |            |    |   |       |   |        +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                               |   `---\"-------     |    |   `-------'   |        +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                               `--------------------'    `---------------'        +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+--------------------------------------------------------------------------------------------------+");
            screen_area_puts(ge->map, str);

    }
    else if(id_act == 97){
	    sprintf(str, "+--------------------------------------------------------------------------------------------------+");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                                                                                  +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+      _______ _                                                ================================   +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+     |__   __| |                                                ||002  || '   ||  '  ||     ||    +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+        | |  | |__   ___                                        ||    _||     ||     ||_    ||    +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+        | |  | '_ \\ / _ \\                                       ||   (__D     ||     C__)   ||    +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+        | |  | | | |  __/                                       ||   (__D     ||     C__)   ||    +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+        |_|  |_| |_|\\___|                                       ||   (__D     ||     C__)   ||    +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                                                ||   (__D     ||     C__)   ||    +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+          _____      _     _                                    ||     ||     ||     ||     ||    +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+         |  __ \\    (_)   (_)                                  ================================   +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+         | |__) | __ _ ___ _  ___  _ __                        ================================   +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+         |  ___/ '__| / __| |/ _ \\| '_ \\                       ||    43696675656e746573    ||     +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+         | |   | |  | \\__ \\ | (_) | | | |                      ||    _||     ||     ||_    ||     +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+         |_|   |_|  |_|___/_|\\___/|_| |_|                      ||   (__D     ||     C__)   ||     +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                                               ||   (__D     ||     C__)   ||     +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                                               ||   (__D     ||     C__)   ||     +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                                               ||   (__D     ||     C__)   ||     +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                                               ||     ||     ||     ||     ||     +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                                               ================================   +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                                                                                  +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                                                                                  +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+--------------------------------------------------------------------------------------------------+");
            screen_area_puts(ge->map, str);

    }
    else if(id_act == 96){
            sprintf(str, "+--------------------------------------------------------------------------------------------------+");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                                                                                  +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+      _______ _                                                                                   +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+     |__   __| |                                               *         *   ,     *            * +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+        | |  | |__   ___                          *            .-----------. ((                   +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+        | |  | '_ \\ / _ \\              *                 *     )`'`'`'`'`'`( ||     *             +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+        | |  | | | |  __/                                     /`'`'`'`'`'`'`\\||                   +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+        |_|  |_| |_|\\___|          *         *       *       /`'`'`'`'`'`'`'`\\| *        *        +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                                            /`'`'`'`'`'`'`'`'`\\      ,            +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+             _____                    *            .-------.`|```````````````|`  .   )       *    +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+            |_   _|               *        *      / ,^, ,^, \\|  ,^^,   ,^^,  |  / \\ ((            +");
            screen_area_puts(ge->map, str);	


            sprintf(str, "+              | |  _ __  _ __                    /  |_| |_|  \\  |__|   |__|  | /   \\||   *        +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+              | | | '_ \\| '_ \\       *       *  /_____________\\ |  |   |  |  |/     \\|         *  +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+             _| |_| | | | | | |                  |  __   __  |  '=='   '=='  /.......\\     *      +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+            |_____|_| |_|_| |_|   *     *        | (  ) (  ) |  //`_```_`\\\\  |,^, ,^,|        *   +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                                 | |--| |--| |  ||(O)|(O)||  ||_| |_||            +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                              *  | |__| |__| |  || \" | \" ||  ||_| |_|| *        * +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                                 |           |  ||   |   ||  |       |    *       +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                       **********'==========='==''==='===''=='======='************+");
            screen_area_puts(ge->map, str);
            sprintf(str, "+--------------------------------------------------------------------------------------------------+");
            screen_area_puts(ge->map, str);


    }
    else if(id_act == 95){
            sprintf(str, "+--------------------------------------------------------------------------------------------------+");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                                         _________________                        +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+      _______ _                                     ____/                 \\____                   +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+     |__   __| |                                ___/                           \\___               +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+        | |  | |__   ___                      _/                                   \\_             +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+        | |  | '_ \\ / _ \\                    /                                       \\            +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+        | |  | | | |  __/                  /                     ___                  \\           +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+        |_|  |_| |_|\\___|                  |                    |__ \\                  |          +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                           |                       ) |                 |          +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+           _____                           |                      / /                  |          +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+          / ____|                          |                     |_|                   |          +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+         | |     __ ___   _____            |                     (_)                   |          +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+         | |    / _` \\ \\ / / _ \\           |                                           |          +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+         | |___| (_| |\\ V /  __/           |                                           |          +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+          \\_____\\__,_| \\_/ \\___|           |                                           |          +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                           |                                           |          +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                           |                                           |          +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                       \\ | /                                           | \\ | /    +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                       _\\|/|                                   \\|/     |__\\|/___  +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+--------------------------------------------------------------------------------------------------+");
            screen_area_puts(ge->map, str);


    }

    else if(id_act == 94){
            sprintf(str, "+--------------------------------------------------------------------------------------------------+");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                                              ...                                 +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+        _______ _____  _______ __________ __    __          ;::::;                                +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+       |  __   \\  ___||       |___    ___|  |  |  |       ;::::; :;                               +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+       | |  |  | |    |   _   |   |  |   |  |__|  |     ;:::::'   :;                              +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+       | |  |  | |___ |  /_\\  |   |  |   |        |    ;:::::; ____ ;                             +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+       | | /  /|  ___||       |   |  |   |        |    :::::' \"    `;                             +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+       | |/  / | |    |   _   |   |  |   |   __   |    ::::::' o\\   \\                             +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+       |    /  | |___ |  / \\  |   |  |   |  |  |  |    ::::::'-..__.-'.                           +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+       |___/   \\_____||__| |__|    \\/    |__|  |__|    ::::::    ` .   '.        _____            +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                                       ::::::       '---\"       /     \\           +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                                      ;:::::;       ;          /       \\          +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                                     ,;::::::;     ;'         / ___     \\         +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                                   ;:::::::::`. ,,,;.        / /   \\__   \\_       +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                                 .';:::::::::::::::::;,     / /       \\_    \\     +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                                ,::::::;::::::;;;;::::;,   / /          \\_  |     +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                               ;`::::::`'::::::;;;::::: ,#/ /             \\ |     +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                               :`:::::::`;::::::;;::: ;::# /              | |     +"); 
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                               ::`:::::::`;:::::::: ;::::#/               |/      +");			
            screen_area_puts(ge->map, str);
            sprintf(str, "+--------------------------------------------------------------------------------------------------+");
            screen_area_puts(ge->map, str);

    }

    else if(id_act == 63){
            sprintf(str, "+--------------------------------------------------------------------------------------------------+");
            screen_area_puts(ge->map, str);
            sprintf(str, "+  .           *               |               /                              0   0                +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+               '.         |    |      '       |   '     *                    |   |                +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                 \\*        \\   \\             /                           ____|___|____            +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+       '          \\     '* |    |  *        |*                *       0  |~ ~ ~ ~ ~ ~|   0        +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+            *      `.       \\   |     *     /    *      '             |  | @ @ @ @ @ |   |        +");
            screen_area_puts(ge->map, str);

            sprintf(str, "+  .                  \\      |   \\          /               *       ___|__|___________|___|__      +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+     *'  *     '      \\      \\   '.       |                        |/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/|      +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+        -._            `                  /         *              | @ @ @ @ @ @ @ @ @ @ @ |      +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+  ' '      ``._   *                           '          .         |/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/|      +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+   *           *\\*          * .   .      *                         |_______________________|      +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+ *  '       *    `-._                       .         _..:='        *                             +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+             .  '      *       *    *   .       _.:--'             U _____ u _   _    ____        + ");
            screen_area_puts(ge->map, str);
            sprintf(str, "+          *           .     .     *         .-'         *          \\| ___\"|/| \\ |\"|  |  _\"\\       +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+   .               '             . '   *           *         .      |  _|\" <|  \\| |>/| | | |      +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+  *       ___.-=--..-._     *                '               '      | |___ U| |\\  |uU| |_| |\\     +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                  *       *                         |_____| |_| \\_|  |____/ u     +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                *        _.'  .'       `.        '  *               <<   >> ||   \\\\,-.|||_        +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+     *              *_.-'   .'            `.               *       (__) (__)(_\")  (_/(__)_)       +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+--------------------------------------------------------------------------------------------------+");
            screen_area_puts(ge->map, str);


    }

    else if(id_act == 106){
            sprintf(str, "+--------------------------------------------------------------------------------------------------+");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                                                                                  +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                                                                                  +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                                                                                  +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                                                                                  +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                                                                                  +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                                _________________________________                                 +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                               /                                 \\                                +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                              /   _____________________________   \\                               +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                             /   / _______ _                   \\   \\                              +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                            /   / |__   __| |                   \\   \\                             +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                           /   /     | |  | |__   ___            \\   \\                            +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                          /   /      | |  | '_ \\ / _ \\            \\   \\                           +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                         /   /       | |  | | | |  __/             \\   \\                          +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                        /   /        |_|  |_| |_|\\___|              \\   \\                         +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                       /   /             ____       _     _          \\   \\                        +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                      /   / ____        |    \\     (_)   | |          \\   \\                       +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                     /   /'`    `;      | |_) |_ __ _  __| | __ _  ___ \\   \\                      +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                    /   / ' o\\   \\      |  _ <| '__| |/ _` |/ _` |/ _ \\ \\   \\                     +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                   /   /  '-..__.-'.    | |_) | |  | | (_| | (_| |  __/  \\   \\                    +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+                  /   /       ` .   '.  |____/|_|  |_|\\__,_|\\__, |\\___|   \\   \\                   +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+       __________/   /           '---'                       |___/         \\   \\__________        +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+--------------------------------------------------------------------------------------------------+");
            screen_area_puts(ge->map, str);

    }
       else if(id_act >= 90 && id_act <= 93 ){
            sprintf(str, "+--------------------------------------------------------------------------------------------------+");
            screen_area_puts(ge->map, str);
            sprintf(str, "+  _______________________________________________________________________________________________ +");
            screen_area_puts(ge->map, str);
            sprintf(str, "+ /          |  GOOSE |       |        |        |  GOOSE  |         |        |  INN   |           \\+");
            screen_area_puts(ge->map, str);
            sprintf(str, "+ |     _____|________|_______|________|________|_________|_________|________|________|_____      |+");
            screen_area_puts(ge->map, str);
            sprintf(str, "+ |    /         |       |       |  GOOSE  |        |         | PRISON  |        |          \\     |+");
            screen_area_puts(ge->map, str);
            sprintf(str, "+ |____|      ___|_______|_______|_________|________|_________|_________|________|____      |_____|+");
            screen_area_puts(ge->map, str);
            sprintf(str, "+ |    |     /                                                                        \\     |     |+");
            screen_area_puts(ge->map, str);
            sprintf(str, "+ |WELL|_____|                                                                        |     |     |+");
            screen_area_puts(ge->map, str);
            sprintf(str, "+ |    |     |            _______ _               ____                      _         |_____|     |+");
            screen_area_puts(ge->map, str);
            sprintf(str, "+ |____|     |           |__   __| |             |  _ \\                    | |        |     |_____|+");
            screen_area_puts(ge->map, str);
            sprintf(str, "+ |    |     |              | |  | |__   ___     | |_) | ___   __ _ _ __ __| |        |     |     |+");
            screen_area_puts(ge->map, str);
            sprintf(str, "+ |    |     |              | |  | '_ \\ / _ \\    |  _ < / _ \\ / _` | '__/ _` |        |     |     |+");
            screen_area_puts(ge->map, str);
            sprintf(str, "+ |    |_____|              | |  | | | |  __/    | |_) | (_) | (_| | | | (_| |        |_____|     |+");
            screen_area_puts(ge->map, str);
            sprintf(str, "+ |____|     |              |_|  |_| |_|\\___|    |____/ \\___/ \\__,_|_|  \\__,_|        |     |_____|+");
            screen_area_puts(ge->map, str);
            sprintf(str, "+ |    |     |_________                                                               |     |     |+");
            screen_area_puts(ge->map, str);
            sprintf(str, "+ |    | DEATH  |      |                                                              |     |     |+");
            screen_area_puts(ge->map, str);
            sprintf(str, "+ |    |        |      |                                                              |_____|GOOSE|+");
            screen_area_puts(ge->map, str); 
            sprintf(str, "+ |____\\________|______|______________________________________________________________/     |_____|+");
            screen_area_puts(ge->map, str);
            sprintf(str, "+ | GOOSE  |        |        |        |  GOOSE |  MAZE  |        |        |  GOOSE |        |     |+");
            screen_area_puts(ge->map, str);
            sprintf(str, "+ \\________|________|________|________|________|________|________|________|________|_______/      |+");
            screen_area_puts(ge->map, str);
            sprintf(str, "+ |  BEGINNING  |        |        |        |        |  GOOSE | BRIDGE |        |        |         |+");
            screen_area_puts(ge->map, str);
            sprintf(str, "+ \\_____________|________|________|________|________|________|________|________|________|_________/+");
            screen_area_puts(ge->map, str);
	    sprintf(str, "+--------------------------------------------------------------------------------------------------+");
            screen_area_puts(ge->map, str);
    }
		
    /* Paint the in the description area */
    screen_area_clear(ge->descript);
    sprintf(str, " Location %s", space_get_name(space_act));
    screen_area_puts(ge->descript, str);
    sprintf(str, "-------------------------------------");
    screen_area_puts(ge->descript, str);
    sprintf(str, " You can see:");
    screen_area_puts(ge->descript, str);
    
    for (i = 0; i < MAX_OBJ; i++) {
        if (game_get_object(game, i) != NULL) {
            if ((obj_loc = game_get_object_location(game_get_object(game, i))) != NO_ID) {
		if(obj_loc == space_get_id(space_act)){
		   if(space_get_illumination(space_act) == TRUE){
		      if(object_get_hidden(game_get_object(game, i)) == FALSE){
          	          sprintf(str, "  %s", object_get_name(game_get_object(game, i)));
          	          screen_area_puts(ge->descript, str);
		      }
		      else{
			counter++;
		      }
		   }
		   else if(space_get_illumination(space_act) == FALSE){
			counter++;
			for (j = 0; j < MAXBACKPACK; j++) {
			        current_object = set_get_id(player_get_object(game_get_player(game)), j);
				printf("%d", object_get_switchedOn(game_get_object(game, current_object + NO_ID)));
				printf("%s", object_get_name(game_get_object(game, current_object + NO_ID)));
			        if (object_get_switchedOn(game_get_object(game, current_object + NO_ID)) == TRUE) {
			            if(object_get_hidden(game_get_object(game, i)) == FALSE){
          	          		sprintf(str, "  %s", object_get_name(game_get_object(game, i)));
          	          		screen_area_puts(ge->descript, str);
					counter--;
		      		    }
				}
			}
		   }
		}
		else{
		    counter++;
	        }
            }
	    else{
		counter++;
	    }
        }
	else{
	    counter++;
	}
    }
    if(counter == MAX_OBJ){
	sprintf(str, " Nothing");
        screen_area_puts(ge->descript, str);
    }
    sprintf(str, "-------------------------------------");
    screen_area_puts(ge->descript, str);   
    sprintf(str, " Player inventory:");
    screen_area_puts(ge->descript, str);
    counter = 0;
    for (i = 0; i < MAXBACKPACK; i++) {

        current_object = set_get_id(player_get_object(game_get_player(game)), i);
        if (current_object != NO_ID) {
            sprintf(str, "  %s", object_get_name(game_get_object(game, current_object + NO_ID)));
            screen_area_puts(ge->descript, str);
        } else {
            counter++;
        }
    }
    if (counter == MAXBACKPACK) {
        sprintf(str, " Empty");
        screen_area_puts(ge->descript, str);
    }

    /*sprintf(str, " Last die value: %d", die_value(game_get_die(game)));
    screen_area_puts(ge->descript, str);*/
    /* Paint the in the banner area */
    screen_area_puts(ge->banner, " Goose Souls ");

    /* Paint the in the help area */
    screen_area_clear(ge->help);
    sprintf(str, " Grasp(g), Drop(d), Exit(e), Check(c), Use(u), Skip(f), Turnon(+), Turnoff(-)");
    screen_area_puts(ge->help, str);
    sprintf(str, " Move(m)[North(n), South(s), East(e), West(w)], Save(s), Load(l)");
    screen_area_puts(ge->help, str);


    /* Paint in the feedback area */
    cmd = game_get_last_command(game);
    last_cmd = command_get_cmd(cmd);
    if (strcmp(cmd_to_str[last_cmd - NO_CMD], "No command") == 0 || strcmp(cmd_to_str[last_cmd - NO_CMD], "Unknown") == 0) {
        sprintf(str, " %s", dialogue_validation(cmd));
	screen_area_puts(ge->feedback, str);
        if (log != NULL) {
            fprintf(log, "%s\n", cmd_to_str[last_cmd - NO_CMD]);
        }
    } else if (game_get_callback_status(game) == OK) {
        sprintf(str, " %s", dialogue_validation(cmd));
	screen_area_puts(ge->feedback, str);
        if (log != NULL) {
            fprintf(log, "%s: OK\n", cmd_to_str[last_cmd - NO_CMD]);
        }
    }  else if (command_compare_to_last(cmd) == TRUE && game_get_preavious_status(game) == ERROR){
	sprintf(str," %s", dialogue_repeat());
	screen_area_puts(ge->feedback, str);
	if (log != NULL) {
            fprintf(log, "%s: ERROR\n", cmd_to_str[last_cmd - NO_CMD]);
	}
    } else {
        sprintf(str, " %s", dialogue_error(cmd));
	screen_area_puts(ge->feedback, str);
        if (log != NULL) {
            fprintf(log, "%s: ERROR\n", cmd_to_str[last_cmd - NO_CMD]);
        }
    }
    /* Paint in the description area */
    if (strcmp(cmd_to_str[last_cmd - NO_CMD], "Check") == 0 && game_get_callback_status(game) == OK) {
        sprintf(str, " Description of %s is:", command_get_object(cmd));
        screen_area_puts(ge->description, str);
        sprintf(str, "  %s", game_get_description(game));
        screen_area_puts(ge->description, str);
        sprintf(str, " ");
	screen_area_puts(ge->description, str);
    }
    else{
	sprintf(str, " %s", space_get_descriptioff(space_act));
	screen_area_puts(ge->description, str);
	sprintf(str, " ");
	screen_area_puts(ge->description, str);
        sprintf(str, " ");
	screen_area_puts(ge->description, str);
    }


    /* Dump to the terminal */
    screen_paint();
    printf("prompt:> ");
}
