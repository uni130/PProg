/** 
 * @brief It declares the tests for the game module
 * 
 * @file game_test.c
 * @author Group 1 
 * @version 1.0 
 * @date 6-04-2018
 * @copyright GNU Public License
 */

#define MAX_TESTS 30

#include "../include/game_test.h"

/** 
 * @brief Main function for GAME unit tests. 
 * 
 * You may execute ALL or a SINGLE test
 *   1.- No parameter -> ALL test are executed 
 *   2.- A number means a particular test (the one identified by that number) 
 *       is executed
 *  
 */
int main(int argc, char** argv) {

    int test = 0;
    int all = 1;

    if (argc < 2) {
        printf("Running all test for module game:\n");
    } else {
        test = atoi(argv[1]);
        all = 0;
        printf("Running test %d:\t", test);
        if (test < 1 && test > MAX_TESTS) {
            printf("Error: unknown test %d\t", test);
            exit(EXIT_SUCCESS);
        }
    }
    if (all || test == 1) test1_game_create();
    if (all || test == 2) test1_game_add_space();
    if (all || test == 3) test2_game_add_space();
    if (all || test == 4) test1_game_add_link();
    if (all || test == 5) test2_game_add_link();
    if (all || test == 6) test1_game_add_object();
    if (all || test == 7) test2_game_add_object();
    if (all || test == 8) test1_game_is_over();
    if (all || test == 9) test2_game_is_over();
    if (all || test == 10) test1_game_get_space();
    if (all || test == 11) test2_game_get_space();
    if (all || test == 12) test1_game_get_object();
    if (all || test == 13) test2_game_get_object();
    if (all || test == 14) test1_game_get_player();
    if (all || test == 15) test2_game_get_player();
    if (all || test == 16) test1_game_get_link();
    if (all || test == 17) test2_game_get_link();
    if (all || test == 18) test1_game_get_die();
    if (all || test == 19) test2_game_get_die();
    if (all || test == 20) test1_game_get_callback_status();
    if (all || test == 21) test2_game_get_callback_status();
    if (all || test == 22) test1_game_get_player_location();
    if (all || test == 23) test2_game_get_player_location();
    if (all || test == 24) test1_game_get_object_location();
    if (all || test == 25) test2_game_get_object_location();
    if (all || test == 26) test1_game_get_description();
    if (all || test == 27) test2_game_get_description();
    if (all || test == 28) test1_game_get_last_command();
    if (all || test == 29) test1_game_get_preavious_status();
    if (all || test == 39) test2_game_get_preavious_status();
 

    PRINT_PASSED_PERCENTAGE;

    return 0;
}

void test1_game_create() {
    Game* g;
    g = game_create();
    PRINT_TEST_RESULT(g != NULL);
    game_destroy(g);
}

void test1_game_add_space() {
    Game *g;
    Space *s;
    g = game_create();
    s = space_create(1);
    game_add_space(g, s);
    PRINT_TEST_RESULT(game_get_space(g, 1) != 0);
    game_destroy(g);
}

void test2_game_add_space() {
    Game *g;
    g = game_create();
    PRINT_TEST_RESULT(game_add_space(g, NULL) == ERROR);
    game_destroy(g);
}

void test1_game_add_link() {
    Game *g;
    Link *l;
    g = game_create();
    l = link_create();
    PRINT_TEST_RESULT(game_add_link(g, l) == OK);
    game_destroy(g);
}

void test2_game_add_link() {
    Game* g;
    g = game_create();
    game_add_link(g, NULL);
    PRINT_TEST_RESULT(game_get_link(g, 1) == ERROR);
    game_destroy(g);
}

void test1_game_add_object() {
    Game* g;
    Object* ob;
    g = game_create();
    ob = object_create(1);
    PRINT_TEST_RESULT(game_add_object(g, ob) == OK);
    game_destroy(g);
}

void test2_game_add_object() {
    Game* g;
    g = game_create();
    PRINT_TEST_RESULT(game_add_object(g, NULL) == ERROR);
    game_destroy(g);
}

void test1_game_is_over() {
    Game *g;
    g = game_create();
    PRINT_TEST_RESULT(game_is_over(g) == FALSE);
    game_destroy(g);
}

void test2_game_is_over() {
    Game *g;
    g = game_create();
    PRINT_TEST_RESULT(game_is_over(NULL) == TRUE);
    game_destroy(g);
}

void test1_game_get_space() {
    Game *g;
    Space *s;
    g = game_create();
    s = space_create(1);
    game_add_space(g, s);
    PRINT_TEST_RESULT(game_get_space(g, 1) != 0);
    game_destroy(g);
}

void test2_game_get_space() {
    Game *g;
    g = game_create();
    PRINT_TEST_RESULT(game_get_space(g, NO_ID) == ERROR);
    game_destroy(g);
}

void test1_game_get_object() {
    Game* g;
    Object* ob;
    g = game_create();
    ob = object_create(1);
    game_add_object(g, ob);
    PRINT_TEST_RESULT(game_get_object(g, 1) == NULL);
    game_destroy(g);
}

void test2_game_get_object() {
    Game* g;
    g = game_create();
    PRINT_TEST_RESULT(game_get_object(g, NO_ID) == NULL);
    game_destroy(g);
}

void test1_game_get_player() {
    Game* g;
    g = game_create();
    game_create_from_file(g, "doc/data.dat");
    PRINT_TEST_RESULT(game_get_player(g) != NULL);
    game_destroy(g);
}

void test2_game_get_player() {
    Game* g = NULL;
    PRINT_TEST_RESULT(game_get_player(g) == NULL);
}

void test1_game_get_link() {
    Game *g = NULL;
    Link *l = NULL;
    g = game_create();
    l = link_create();
    link_set_id(l, 1);
    game_add_link(g, l);
    PRINT_TEST_RESULT(game_get_link(g, 1) != NULL);
    game_destroy(g);
}

void test2_game_get_link() {
    Game *g;
    g = game_create();
    PRINT_TEST_RESULT(game_get_link(g, NO_ID) == NULL);
    game_destroy(g);
}

void test1_game_get_die() {
    Game *g;
    g = game_create();
    PRINT_TEST_RESULT(game_get_die(g) != NULL);
    game_destroy(g);
}

void test2_game_get_die() {
    Game *g;
    g = game_create();
    PRINT_TEST_RESULT(game_get_die(g) != NULL);
    game_destroy(g);
}

void test1_game_get_callback_status() {
    Game *g;
    g = game_create();
    PRINT_TEST_RESULT(game_get_callback_status(g) == ERROR);
    game_destroy(g);
}

void test2_game_get_callback_status() {
    PRINT_TEST_RESULT(game_get_callback_status(NULL) == ERROR);
}

void test1_game_get_player_location() {
    Game *g;
    g = game_create();
    PRINT_TEST_RESULT(game_get_player_location(g) == NO_ID);
    game_destroy(g);
}

void test2_game_get_player_location() {
    PRINT_TEST_RESULT(game_get_player_location(NULL) == NO_ID);
}

void test1_game_get_object_location() {
    Game* g;
    Object* ob;
    g = game_create();
    ob = object_create(1);
    game_add_object(g, ob);
    PRINT_TEST_RESULT(game_get_object_location(ob) == NO_ID);
    game_destroy(g);
}

void test2_game_get_object_location() {
    PRINT_TEST_RESULT(game_get_object_location(NULL) == NO_ID);
}

void test1_game_get_description() {
    Game *g;
    g = game_create();
    PRINT_TEST_RESULT(game_get_description(g) != NULL);
    game_destroy(g);
}

void test2_game_get_description() {
    PRINT_TEST_RESULT(game_get_description(NULL) == NULL);
}

void test1_game_get_last_command() {
    Game *g;
    g = game_create();
    PRINT_TEST_RESULT(game_get_last_command(g) == NULL);
    game_destroy(g);
}


void test1_game_get_preavious_status() {
    Game *g;
    g = game_create();
    PRINT_TEST_RESULT(game_get_preavious_status(g) == ERROR);
    game_destroy(g);
}

void test2_game_get_preavious_status() {
    PRINT_TEST_RESULT(game_get_preavious_status(NULL) == ERROR);
}

