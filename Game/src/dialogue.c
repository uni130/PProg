/** 
 * @brief It implements the dialogue module
 * 
 * @file dialogue.c
 * @author Javier Sanmiguel
 * @version 2.0
 * @date 17-04-2018 
 * @copyright GNU Public License
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "../include/dialogue.h"
#include "../include/die.h"

char sentences_NO_CMD[3][WORD_SIZE] = {"Did you write something?","Do you want to play?","Please tell me to do something master"};
char sentences_UNKNOWN[3][WORD_SIZE] = {"Did you read the guide?","Maybe you should try something different","What are you trying?"};
char sentences_EXIT[3][WORD_SIZE] = {"See you later","Bye bye","Why you quit? Is there anything better than playing?"};
char sentences_FOLLOWING[3][WORD_SIZE] = {"You successfully move south","Do you want to reach the South Pole?","Moving, moving, all the people moving"};
char sentences_PREVIOUS[3][WORD_SIZE] = {"You successfully move north","Do you want to reach the North Pole?","Go go go"};
char sentences_TAKE[3][WORD_SIZE] = {"Let's save this","Maybe this can be useful later","It is dangerous to go alone, I will take this"};
char sentences_DROP[3][WORD_SIZE] = {"I will leave this here","You can drop this object, but don't drop out your objectives","If you didn't want it, why you took it?"};
char sentences_ROLL[3][WORD_SIZE] = {"They see me rolling, they hatin","Did you try doing a barrel roll?","Do you like rock 'n' rolling?"};
char sentences_LEFT[3][WORD_SIZE] = {"You successfully move west","You left the space on the right","If we continue moving west maybe we can find a cowboy"};
char sentences_RIGHT[3][WORD_SIZE] = {"You successfully move east","You moved east, where the sun rises","You moved rightly, understand? right, west ... Okay :("};
char sentences_UP[3][WORD_SIZE] = {"You successfully move up","You climb up successfully","You are ascending like a rocket!"};
char sentences_DOWN[3][WORD_SIZE] = {"You successfully move down","You climb down succesfully","You are falling like the rain"};
char sentences_CHECK[3][WORD_SIZE] = {"This is quite interesting","What is this?","Let's look at this"};
char sentences_LOAD[3][WORD_SIZE] = {"Coming back the party?","We missed you!","After all this time waiting, aliens conquered the world"};
char sentences_SAVE[3][WORD_SIZE] = {"You have save successfully","Checkpoint setted","Already saved, you can now exit without loosing progress"};
char sentences_USE[3][WORD_SIZE] = {"You have used it successfully","This will open my future","After using it i won't need it more"};
char sentences_TURNON[3][WORD_SIZE] = {"You have turned it on successfully","This will illuminate my future","And Lazarus said \"I can see!\""};
char sentences_TURNOFF[3][WORD_SIZE] = {"You have turned it off successfully","You will let this darket than my future","Lets save some battery"};


char error_FOLLOWING[3][WORD_SIZE] = {"You failed moving south","Try not to crash with walls","There is a snake in your boot!"};
char error_PREVIOUS[3][WORD_SIZE] = {"You failed moving north","You can't do that","Sorry, a cockroach blocks your way"};
char error_TAKE[3][WORD_SIZE] = {"Why I would take this?","You can't do that","Offcourse, take that and then take pineapple pizza"};
char error_DROP[3][WORD_SIZE] = {"You failed droping that","I will not drop my treasure","Why would you drop a thing that wants to be next to you?"};
char error_LEFT[3][WORD_SIZE] = {"You failed moving west","You should not pass","A Snorlax is in your path"};
char error_RIGHT[3][WORD_SIZE] = {"You failed moving east","Sorry, there is an invisible wall","You could't move rightly. OK i will stop now"};
char error_UP[3][WORD_SIZE] = {"You failed moving up","Ups, you felt down while climbing","Do you think you have wings?"};
char error_DOWN[3][WORD_SIZE] = {"You failed moving down","If you want to go down lets make a hole first","Your marks went down but you didn't"};
char error_CHECK[3][WORD_SIZE] = {"Nothing to see here","I prefer not to do that","There are things that must remain unknown"};
char error_LOAD[3][WORD_SIZE] = {"No savefile found with that name","There was a segmentation fault in the savefile and it couldn't be loaded","Error while loading"};
char error_SAVE[3][WORD_SIZE] = {"There are enemies nearby","You couldn't save","Not valid name for the savefile"};
char error_USE[3][WORD_SIZE] = {"I can't use this here","It doesn't look effective","Error 404 valid action not found"};
char error_TURNON[3][WORD_SIZE] = {"I can't turn that on","Sorry goverment privatized the light and you are poor","You can't turn it on but it can turn you on ;)"};
char error_TURNOFF[3][WORD_SIZE] = {"I can't turn that off","You can't turn it off but you can turn off the game safely after saving","You can't turn it off but you can take off for a break"};

char repeat[3][WORD_SIZE] = {"Stop doing that, please?","Ok, continue like that but you will achieve nothing","Stop making silly things as in real life"};

char* dialogue_validation(Command* cmd){
	int r;
	r = die_random_num( 0,  2);
	switch (command_get_cmd(cmd))
	{
 	case NO_CMD:
		return sentences_NO_CMD[r];
		break;
	case UNKNOWN: 
		return sentences_UNKNOWN[r];
		break;
        case EXIT:
		return sentences_EXIT[r];		
              	break;
	case SKIP:
		return sentences_FOLLOWING[r];
              	break;
	case PREVIOUS:
		return sentences_PREVIOUS[r];
              	break;
        case TAKE:
		return sentences_TAKE[r];
              	break;
        case DROP:
		return sentences_DROP[r];
              	break;
        case ROLL:
		return sentences_ROLL[r];
              	break;
        case LEFT:		 
		return sentences_LEFT[r];
              	break;
        case RIGHT:
		return sentences_RIGHT[r];
              	break;
        case MOVE:
		if(strcmp(command_get_object(cmd), "n") == 0 || strcmp(command_get_object(cmd), "north") == 0 || strcmp(command_get_object(cmd), "North") == 0){
			return sentences_PREVIOUS[r];
		} else if(strcmp(command_get_object(cmd), "e") == 0 || strcmp(command_get_object(cmd), "east") == 0 || strcmp(command_get_object(cmd), "East") == 0){
			return sentences_RIGHT[r];
		} else if(strcmp(command_get_object(cmd), "w") == 0 || strcmp(command_get_object(cmd), "west") == 0 || strcmp(command_get_object(cmd), "West") == 0){
			return sentences_LEFT[r];
		} else if(strcmp(command_get_object(cmd), "s") == 0 || strcmp(command_get_object(cmd), "south") == 0 || strcmp(command_get_object(cmd), "South") == 0){
			return sentences_FOLLOWING[r];
		} else if(strcmp(command_get_object(cmd), "u") == 0 || strcmp(command_get_object(cmd), "up") == 0 || strcmp(command_get_object(cmd), "Up") == 0){
			return sentences_UP[r];
		} else if(strcmp(command_get_object(cmd), "d") == 0 || strcmp(command_get_object(cmd), "down") == 0 || strcmp(command_get_object(cmd), "Down") == 0){
			return sentences_DOWN[r];
		} else{
			return sentences_UNKNOWN[r];
		}
              	break;
        case CHECK:
		return sentences_CHECK[r];
              	break;
        case LOAD:
		return sentences_LOAD[r];
              	break;
        case SAVE:
		return sentences_SAVE[r];
              	break;
	case USE:
		return sentences_USE[r];
              	break;
	case TURNON:
		return sentences_TURNON[r];
              	break;
	case TURNOFF:
		return sentences_TURNOFF[r];
              	break;
        default:
              	break;
      }
    return NULL;
}



char* dialogue_error(Command* cmd){
	int r;
	r = die_random_num( 0,  2);
	switch (command_get_cmd(cmd))
	{
 	case NO_CMD:
		return sentences_NO_CMD[r];
		break;
	case UNKNOWN: 
		return sentences_UNKNOWN[r];
		break;
        case EXIT:
		return sentences_EXIT[r];		
              	break;
	case SKIP:
		return error_FOLLOWING[r];
              	break;
	case PREVIOUS:
		return error_PREVIOUS[r];
              	break;
        case TAKE:
		return error_TAKE[r];
              	break;
        case DROP:
		return error_DROP[r];
              	break;
        case ROLL:
		return sentences_ROLL[r];
              	break;
        case LEFT:		 
		return error_LEFT[r];
              	break;
        case RIGHT:
		return error_RIGHT[r];
              	break;
        case MOVE:
		if(strcmp(command_get_object(cmd), "n") == 0 || strcmp(command_get_object(cmd), "north") == 0 || strcmp(command_get_object(cmd), "North") == 0){
			return error_PREVIOUS[r];
		} else if(strcmp(command_get_object(cmd), "e") == 0 || strcmp(command_get_object(cmd), "east") == 0 || strcmp(command_get_object(cmd), "East") == 0){
			return error_RIGHT[r];
		} else if(strcmp(command_get_object(cmd), "w") == 0 || strcmp(command_get_object(cmd), "west") == 0 || strcmp(command_get_object(cmd), "West") == 0){
			return error_LEFT[r];
		} else if(strcmp(command_get_object(cmd), "s") == 0 || strcmp(command_get_object(cmd), "south") == 0 || strcmp(command_get_object(cmd), "South") == 0){
			return error_FOLLOWING[r];
		} else if(strcmp(command_get_object(cmd), "u") == 0 || strcmp(command_get_object(cmd), "up") == 0 || strcmp(command_get_object(cmd), "Up") == 0){
			return error_UP[r];
		} else if(strcmp(command_get_object(cmd), "d") == 0 || strcmp(command_get_object(cmd), "down") == 0 || strcmp(command_get_object(cmd), "Down") == 0){
			return error_DOWN[r];
		} else{
			return sentences_UNKNOWN[r];
		}
              	break;
        case CHECK:
		return error_CHECK[r];
              	break;
        case LOAD:
		return error_LOAD[r];
              	break;
        case SAVE:
		return error_SAVE[r];
              	break;
        case USE:
		return error_USE[r];
              	break;
	case TURNON:
		return error_TURNON[r];
              	break;
	case TURNOFF:
		return error_TURNOFF[r];
              	break;
        default:
              	break;
      }
    return NULL;
}

char* dialogue_repeat(){
	int r;
	r = die_random_num(0, 2);
	return repeat[r];
}
