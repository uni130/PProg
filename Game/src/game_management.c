/** 
 * @brief It defines the game management
 * 
 * @file game_management.c
 * @author Group 1
 * @version 3.0
 * @date 04-04-2018 
 * @copyright GNU Public License
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/game_management.h"

STATUS game_management_load_spaces(Game* game, char* filename) {
    FILE* file = NULL;
    char line[WORD_SIZE] = "";
    char name[WORD_SIZE] = "";
    char description[WORD_SIZE] = "";
    char descriptioff[WORD_SIZE] = "";
    char f1[8];
    char f2[8];
    char f3[8];
    char* toks = NULL;
    Id id = NO_ID, north = NO_ID, east = NO_ID, south = NO_ID, west = NO_ID, up = NO_ID, down = NO_ID;
    Space* space = NULL;
    STATUS status = OK;
    BOOL illumination = TRUE;

    if (!filename) {
        return ERROR;
    }

    file = fopen(filename, "r");
    if (file == NULL) {
        return ERROR;
    }

    while (fgets(line, WORD_SIZE, file)) {
        if (strncmp("#s:", line, 3) == 0) {
            toks = strtok(line + 3, "|");
            id = atol(toks);
            toks = strtok(NULL, "|");
            strcpy(name, toks);
            toks = strtok(NULL, "|");
            north = atol(toks);
            toks = strtok(NULL, "|");
            east = atol(toks);
            toks = strtok(NULL, "|");
            south = atol(toks);
            toks = strtok(NULL, "|");
            west = atol(toks);
            toks = strtok(NULL, "|");
            up = atol(toks);
            toks = strtok(NULL, "|");
            down = atol(toks);
            toks = strtok(NULL, "|");
            illumination = atol(toks);
            toks = strtok(NULL, "|");
            strcpy(f1, toks);
            toks = strtok(NULL, "|");
            strcpy(f2, toks);
            toks = strtok(NULL, "|");
            strcpy(f3, toks);
            toks = strtok(NULL, "|");
            strcpy(description, toks);
            toks = strtok(NULL, "|");
            strcpy(descriptioff, toks);
#ifdef DEBUG 
            printf("Leido: %ld|%s|%ld|%ld|%ld|%ld|%ld|%ld|%s|%s|%s|%s\n", id, name, north, east, south, west, up, down f1, f2, f3, description, descriptioff);
#endif

            space = space_create(id);
            if (space != NULL) {
                space_set_name(space, name);
                space_set_north(space, north);
                space_set_east(space, east);
                space_set_south(space, south);
                space_set_west(space, west);
                space_set_up(space, up);
                space_set_down(space, down);
                space_set_illumination(space, illumination);
                space_set_gdesc(space, f1, f2, f3);
                space_set_description(space, description);
                space_set_descriptioff(space, descriptioff);
                game_add_space(game, space);
            }
        }
    }

    if (ferror(file)) {
        status = ERROR;
    }

    fclose(file);

    return status;
}

STATUS game_management_load_objects(Game* game, char* filename) {
    FILE* file = NULL;
    char name[WORD_SIZE] = "";
    char line[WORD_SIZE] = "";
    char description[WORD_SIZE] = "";
    char description2[WORD_SIZE] = "";
    char* toks = NULL;
    Id id = NO_ID, location = NO_ID, open;
    Object* object = NULL;
    STATUS status = OK;
    BOOL movable, moved, hidden, illuminate, switchedOn;

    if (!filename) {
        return ERROR;
    }

    file = fopen(filename, "r");
    if (file == NULL) {
        return ERROR;
    }

    while (fgets(line, WORD_SIZE, file)) {
        if (strncmp("#o:", line, 3) == 0) {
            toks = strtok(line + 3, "|");
            id = atol(toks);
            toks = strtok(NULL, "|");
            strcpy(name, toks);
            toks = strtok(NULL, "|");
            location = atol(toks);
            toks = strtok(NULL, "|");
	    movable = atol(toks);
	    toks = strtok(NULL, "|");
	    moved = atol(toks);
	    toks = strtok(NULL, "|");
	    hidden = atol(toks);
	    toks = strtok(NULL, "|");
	    open = atol(toks);
	    toks = strtok(NULL, "|");
	    illuminate = atol(toks);
	    toks = strtok(NULL, "|");
	    switchedOn = atol(toks);
	    toks = strtok(NULL, "|");
	    strcpy(description, toks);
	    toks = strtok(NULL, "|");
	    strcpy(description2, toks);
#ifdef DEBUG 
            printf("Leido: %ld|%s|%ld|%d|%d|%d|%ld|%d|%d|%s|%s|\n", object->id, object->name, object->location, object->movable, object->moved, object->hidden, object->open, object->illuminate, object->switchedOn, object->description, object->description2);

#endif
            object = object_create(id);
            if (object != NULL) {
                object_set_name(object, name);
                object_set_location(object, location);
		object_set_movable(object, movable);
		object_set_moved(object, moved);
		object_set_hidden(object, hidden);
		object_set_open(object, open);
		object_set_illuminate(object, illuminate);
		object_set_switchedOn(object, switchedOn);
		object_set_description(object, description);
		object_set_description2(object, description2);
                game_add_object(game, object);
            }
        }
    }

    if (ferror(file)) {
        status = ERROR;
    }

    fclose(file);

    return status;
}

STATUS game_management_load_links(Game* game, char* filename) {
    FILE* file = NULL;
    char line[WORD_SIZE] = "";
    char name[WORD_SIZE] = "";
    char* toks = NULL;
    Id id = NO_ID, spaceid1 = NO_ID, spaceid2 = NO_ID;
    Link* link = NULL;
    STATUS linkstat = OK;
    STATUS status = OK;

    if (!filename) {
        return ERROR;
    }

    file = fopen(filename, "r");
    if (file == NULL) {
        return ERROR;
    }

    while (fgets(line, WORD_SIZE, file)) {
        if (strncmp("#l:", line, 3) == 0) {
            toks = strtok(line + 3, "|");
            id = atol(toks);
            toks = strtok(NULL, "|");
            strcpy(name, toks);
            toks = strtok(NULL, "|");
            spaceid1 = atol(toks);
            toks = strtok(NULL, "|");
            spaceid2 = atol(toks);
            toks = strtok(NULL, "|");
            linkstat = atoi(toks);
#ifdef DEBUG 
            printf("Leido: %ld|%s|%ld|%ld|%d|%d\n", id, name, spaceid1, spaceid2, linkstat);
#endif

            link = link_create();
            if (link != NULL) {
                link_set_id(link, id);
                link_set_name(link, name);
                link_set_spaces(link, spaceid1, spaceid2);
                link_set_status(link, linkstat);
                game_add_link(game, link);
            }
        }
    }

    if (ferror(file)) {
        status = ERROR;
    }

    fclose(file);
    return status;
}

STATUS game_management_load_player(Game* game, char* filename) {
    FILE* file = NULL;
    char line[WORD_SIZE] = "";
    char name[WORD_SIZE] = "";
    char* toks = NULL;
    Id id = NO_ID, spaceid = NO_ID;
    Player* player = NULL;
    STATUS status = OK;
    int i;

    if (!game || !filename) {
        return ERROR;
    }

    file = fopen(filename, "r");
    if (file == NULL) {
        return ERROR;
    }

    while (fgets(line, WORD_SIZE, file)) {
        if (strncmp("#p:", line, 3) == 0) {
            toks = strtok(line + 3, "|");
            id = atol(toks);
            toks = strtok(NULL, "|");
            strcpy(name, toks);
            toks = strtok(NULL, "|");
            spaceid = atol(toks);
            
#ifdef DEBUG 
            printf("Leido: %ld|%s|%ld|", id, name, spaceid);
#endif

            player = player_create(id);
            if (player!= NULL) {
                player_set_name(player, name);
                player_set_location(player, spaceid);
		for(i = 0; i<MAX_OBJ; i++){
			if (game_get_object(game, i) != NULL) {
				if(object_get_location(game_get_object(game, i)) == NO_ID){
					player_set_object(player, object_get_id(game_get_object(game, i)));
					
				}
			}
		}
                game_add_player(game, player);
            }
        }
    }

    if (ferror(file)) {
        status = ERROR;
    }

    fclose(file);
    
    return status;
}


STATUS game_management_save(Game* game, char* filename) {
    FILE* file = NULL;

    if(!game || !filename){
	return ERROR;
    }

    file = fopen(filename, "w");
    if (file == NULL) {
        return ERROR;
    }
    
    game_print_data(game, file);

    fclose(file);

    return OK;
}

STATUS game_management_load(Game* game, char* filename) {
    if(!game){
	return ERROR;
    }

    if (game_create_from_file(game, filename) == ERROR) {
        fprintf(stderr, "Error while initializing game.\n");
        game_destroy(game);
        return ERROR;
    }
    return OK;
}
