/** 
 * @brief It defines the TAD player
 * 
 * @file player.c
 * @author Group 1
 * @version 2.0 
 * @date 29-03-2018
 * @copyright GNU Public License
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/player.h"

/*It defines the struct of the TAD Player */
struct _Player {
    Id id;
    char name[WORD_SIZE + 1];
    Id location;
    Inventory* objects;
};

Player* player_create(Id id) {

    Player *newPlayer = NULL;

    if (id == NO_ID)
        return NULL;

    newPlayer = (Player *) malloc(sizeof (Player));

    if (!newPlayer) {
        return NULL;
    }

    newPlayer->id = id;

    newPlayer->name[0] = '\0';

    newPlayer->location = NO_ID;

    newPlayer->objects = inventory_create();

    return newPlayer;
}

STATUS player_destroy(Player* player) {


    if (!player) {
        return ERROR;
    }

    inventory_destroy(player->objects);

    free(player);
    player = NULL;

    return OK;
}

STATUS player_set_name(Player* player, char* name) {
    if (!player || !name) {
        return ERROR;
    }

    if (!strcpy(player->name, name)) {
        return ERROR;
    }

    return OK;
}

STATUS player_set_location(Player* player, Id id) {
    if (!player) {
        return ERROR;
    }

    player->location = id;

    return OK;

}

STATUS player_set_object(Player* player, Id id) {
    if (!player) {
        return ERROR;
    }

    return inventory_set_id(player->objects, id);

}

Id player_get_id(Player* player) {
    if (!player) {
        return (!player);
    }

    return player->id;
}

const char* player_get_name(Player* player) {
    if (!player) {
        return NULL;
    }

    return player->name;
}

Id player_get_location(Player* player) {

    if (!player) {
        return NO_ID;
    }

    return player->location;
}

Inventory* player_get_inventory(Player* player) {

    if (!player) {
        return NULL;
    }

    return player->objects;

}

Set* player_get_object(Player* player) {
    if (!player) {
        return NULL;
    }

    return inventory_get_ids(player->objects);

}

STATUS player_print(Player* player, FILE * f) {

    if (!player) {
        return ERROR;
    }

    fprintf(f, "#p:%ld|%s|%ld|\n", player->id, player->name, player->location);

    return OK;
}
