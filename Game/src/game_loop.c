/** 
 * @brief It defines the game loop 
 * 
 * @file game_loop.c
 * @author Profesores PPROG, Group 1
 * @version 2.0 
 * @date 15-02-2018 
 * @copyright GNU Public License
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/graphic_engine.h"

int main(int argc, char *argv[]) {
    Game *game = NULL;
    Command* command;
    Rule* rule;
    Graphic_engine *gengine;
    FILE* log = NULL;
    if (argc < 2 || argc == 3) {
        fprintf(stderr, "Use: %s <game_data_file> <flag> <log_file>\n", argv[0]);
        return 1;
    }
    if (argc > 2) {
        if (strcmp(argv[2], "-l")) {
            fprintf(stderr, "<flag> shoudl be -l \n");
            return 1;
        }
    }

    game = game_create();
    if (!game) {
        fprintf(stderr, "Error while initializing game.\n");
        return 1;
    }

    if (game_create_from_file(game, argv[1]) == ERROR) {
        fprintf(stderr, "Error while initializing game.\n");
        game_destroy(game);
        return 1;
    }
    if ((gengine = graphic_engine_create()) == NULL) {
        fprintf(stderr, "Error while initializing graphic engine.\n");
        game_destroy(game);
        return 1;
    }
    command = command_ini();
    if (!command) {
        game_destroy(game);
        graphic_engine_destroy(gengine);
        return 1;
    }
    rule = game_rules_ini();
    if (!rule) {
        game_destroy(game);
        graphic_engine_destroy(gengine);
        return 1;
    }

    if (argc == 4) {
        log = fopen(argv[3], "w");
        if (!log) {
            game_destroy(game);
            command_destroy(command);
	    game_rules_destroy(rule);
            graphic_engine_destroy(gengine);
            return 1;
        }
    }


    while ((command_get_cmd(command) != EXIT) && game_is_over(game) == FALSE) {
        graphic_engine_paint_game(gengine, game, log);
        if (command_get_user_input(command) == ERROR) {
            game_destroy(game);
            command_destroy(command);
	    game_rules_destroy(rule);
            graphic_engine_destroy(gengine);
            if (log != NULL) {
                fclose(log);
            }
            return 1;
        }
        game_update(game, command);
	game_rules_set(rule,2);
	game_rule_update(game, rule);
	game_rules_set(rule,3);
	game_rule_update(game, rule);
	game_rules_set(rule,6);
	game_rule_update(game, rule);
    }
    game_destroy(game);
    game_rules_destroy(rule);
    command_destroy(command);
    graphic_engine_destroy(gengine);
    if (log != NULL) {
        fclose(log);
    }
    return 0;
}
