/** 
 * @brief It defines the TAD player
 * 
 * @file player_test.c
 * @author Javier Sanmiguel
 * @version 1.0 
 * @date 06-04-2018
 * @copyright GNU Public License
 */

#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
#include "../include/player_test.h"

#define MAX_TESTS 21

/** 
 * @brief Main function for PLAYER unit tests. 
 * 
 * You may execute ALL or a SINGLE test
 *   1.- No parameter -> ALL test are executed 
 *   2.- A number means a particular test (the one identified by that number) 
 *       is executed
 *  
 */
int main(int argc, char** argv) {

    int test = 0;
    int all = 1;

    if (argc < 2) {
        printf("Running all test for module player:\n");
    } else {
        test = atoi(argv[1]);
        all = 0;
        printf("Running test %d:\t", test);
        if (test < 1 && test > MAX_TESTS) {
            printf("Error: unknown test %d\t", test);
            exit(EXIT_SUCCESS);
        }
    }
    if (all || test == 1) test1_player_create();
    if (all || test == 2) test2_player_create();
    if (all || test == 3) test1_player_set_name();
    if (all || test == 4) test2_player_set_name();
    if (all || test == 5) test3_player_set_name();
    if (all || test == 6) test1_player_set_location();
    if (all || test == 7) test2_player_set_location();
    if (all || test == 8) test3_player_set_location();
    if (all || test == 9) test1_player_set_object();
    if (all || test == 10) test2_player_set_object();
    if (all || test == 11) test3_player_set_object();
    if (all || test == 12) test1_player_get_id();
    if (all || test == 13) test2_player_get_id();
    if (all || test == 14) test1_player_get_name();
    if (all || test == 15) test2_player_get_name();
    if (all || test == 16) test1_player_get_location();
    if (all || test == 17) test2_player_get_location();
    if (all || test == 18) test1_player_get_inventory();
    if (all || test == 19) test2_player_get_inventory();
    if (all || test == 20) test1_player_get_object();
    if (all || test == 21) test2_player_get_object();

    PRINT_PASSED_PERCENTAGE;

    return 0;
}

void test1_player_create() {
    Player* p;
    p = player_create(1);
    PRINT_TEST_RESULT(p != NULL);
    player_destroy(p);
}

void test2_player_create() {
    Player* p;
    p = player_create(1);
    PRINT_TEST_RESULT(player_get_id(p) == 1);
    player_destroy(p);
}

void test1_player_set_name() {
    Player *p;
    p = player_create(1);
    PRINT_TEST_RESULT(player_set_name(p, "jst") == OK);
    player_destroy(p);
}

void test2_player_set_name() {
    Player *p = NULL;
    PRINT_TEST_RESULT(player_set_name(p, "jst") == ERROR);
    player_destroy(p);
}

void test3_player_set_name() {
    Player *p;
    p = player_create(1);
    PRINT_TEST_RESULT(player_set_name(p, NULL) == ERROR);
    player_destroy(p);
}

void test1_player_set_location() {
    Player *p;
    p = player_create(1);
    PRINT_TEST_RESULT(player_set_location(p, 2) == OK);
    player_destroy(p);
}

void test2_player_set_location() {
    Player *p = NULL;
    PRINT_TEST_RESULT(player_set_location(p, 2) == ERROR);
    player_destroy(p);
}

void test3_player_set_location() {
    Player *p;
    p = player_create(1);
    PRINT_TEST_RESULT(player_set_location(p, NO_ID) != ERROR);
    player_destroy(p);
}

void test1_player_set_object() {
    Player* p;
    p = player_create(1);
    PRINT_TEST_RESULT(player_set_object(p, 2) == OK);
    player_destroy(p);
}

void test2_player_set_object() {
    Player* p = NULL;
    PRINT_TEST_RESULT(player_set_object(p, 2) == ERROR);
    player_destroy(p);
}

void test3_player_set_object() {
    Player* p;
    p = player_create(1);
    PRINT_TEST_RESULT(player_set_object(p, NO_ID) == ERROR);
    player_destroy(p);
}

void test1_player_get_id() {
    Player *p;
    p = player_create(1);
    PRINT_TEST_RESULT(player_get_id(p) == 1);
    player_destroy(p);
}

void test2_player_get_id() {
    Player *p = NULL;
    PRINT_TEST_RESULT(player_get_id(p) != ERROR);
    player_destroy(p);
}

void test1_player_get_name() {
    Player *p;
    p = player_create(1);
    player_set_name(p, "Jst");
    PRINT_TEST_RESULT(!strcmp(player_get_name(p), "Jst"));
    player_destroy(p);
}

void test2_player_get_name() {
    Player* p = NULL;
    PRINT_TEST_RESULT(player_get_name(p) == ERROR);
    player_destroy(p);
}

void test1_player_get_location() {
    Player* p;
    p = player_create(1);
    PRINT_TEST_RESULT(player_get_location(p) == NO_ID);
    player_destroy(p);
}

void test2_player_get_location() {
    Player* p = NULL;
    PRINT_TEST_RESULT(player_get_location(p) != ERROR);
    player_destroy(p);
}

void test1_player_get_inventory() {
    Player* p;
    p = player_create(1);
    PRINT_TEST_RESULT(player_get_inventory(p) != NULL);
    player_destroy(p);
}

void test2_player_get_inventory() {
    Player* p = NULL;
    PRINT_TEST_RESULT(player_get_inventory(p) == ERROR);
    player_destroy(p);
}

void test1_player_get_object() {
    Player *p;
    p = player_create(1);
    PRINT_TEST_RESULT(player_get_object(p) != NULL);
    player_destroy(p);
}

void test2_player_get_object() {
    Player *p = NULL;
    PRINT_TEST_RESULT(player_get_inventory(p) == ERROR);
    player_destroy(p);
}
