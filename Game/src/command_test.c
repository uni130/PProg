/** 
 * @brief It implements the command test logic
 * 
 * @file command_test.c
 * @author Nicolas Serrano
 * @version 1.0
 * @date 05-04-2018 
 * @copyright GNU Public License
 */


#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
#include "../include/command_test.h"

#define MAX_TESTS 11

/** 
 * @brief Main function for PLAYER unit tests. 
 * 
 * You may execute ALL or a SINGLE test
 *   1.- No parameter -> ALL test are executed 
 *   2.- A number means a particular test (the one identified by that number) 
 *       is executed
 *  
 */
int main(int argc, char** argv) {

    int test = 0;
    int all = 1;

    if (argc < 2) {
        printf("Running all test for module command:\n");
    } else {
        test = atoi(argv[1]);
        all = 0;
        printf("Running test %d:\t", test);
        if (test < 1 && test > MAX_TESTS) {
            printf("Error: unknown test %d\t", test);
            exit(EXIT_SUCCESS);
        }
    }
    if (all || test == 1) test1_command_ini();
    if (all || test == 2) test1_command_get_cmd();
    if (all || test == 3) test2_command_get_cmd();
    if (all || test == 4) test1_command_get_object();
    if (all || test == 5) test2_command_get_object();
    if (all || test == 6) test1_command_copy();
    if (all || test == 7) test2_command_copy();
    if (all || test == 8) test1_command_get_parameter2();
    if (all || test == 9) test2_command_get_parameter2();
    if (all || test == 10) test1_command_compare_to_last();
    if (all || test == 11) test2_command_compare_to_last();

    PRINT_PASSED_PERCENTAGE;

    return 0;
}

void test1_command_ini() {
    Command* cmd = NULL;
    cmd = command_ini();
    PRINT_TEST_RESULT(cmd != NULL);
    command_destroy(cmd);
}

void test1_command_get_cmd() {
    Command* cmd = NULL;

    cmd = command_ini();
    PRINT_TEST_RESULT(command_get_cmd(cmd) >= NO_CMD && command_get_cmd(cmd) <= CHECK);
    command_destroy(cmd);
}

void test2_command_get_cmd() {
    Command* cmd = NULL;

    PRINT_TEST_RESULT(command_get_cmd(cmd) == NO_CMD);
}

void test1_command_get_object() {
    Command* cmd = NULL;

    cmd = command_ini();
    PRINT_TEST_RESULT(command_get_object(cmd) != NULL);
    command_destroy(cmd);
}

void test2_command_get_object() {
    Command* cmd = NULL;

    PRINT_TEST_RESULT(command_get_object(cmd) == NULL);
    command_destroy(cmd);
}

void test1_command_copy() {
    Command *cmd = NULL, *copy = NULL;

    cmd = command_ini();
    copy = command_copy(cmd);
    PRINT_TEST_RESULT(copy != NULL);
    command_destroy(cmd);
    command_destroy(copy);
}

void test2_command_copy() {
    Command *cmd = NULL, *copy = NULL;

    copy = command_copy(cmd);
    PRINT_TEST_RESULT(copy == NULL);
    command_destroy(cmd);

}

void test1_command_get_parameter2() {
    Command* cmd = NULL;

    cmd = command_ini();
    PRINT_TEST_RESULT(command_get_parameter2(cmd) != NULL);
    command_destroy(cmd);
}

void test2_command_get_parameter2() {
    Command* cmd = NULL;

    PRINT_TEST_RESULT(command_get_parameter2(cmd) == NULL);
    command_destroy(cmd);
}

void test1_command_compare_to_last(){ 
    Command* cmd = NULL;
    cmd = command_ini();
    PRINT_TEST_RESULT(command_compare_to_last(cmd) == TRUE);
    command_destroy(cmd);

}

void test2_command_compare_to_last(){
	PRINT_TEST_RESULT(command_compare_to_last(NULL) == FALSE);
}
