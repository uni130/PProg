/** 
 * @brief It implements the game rules test logic
 * 
 * @file game_rules_test.c
 * @author Miguel Luque
 * @version 1.0
 * @date 03-05-2018 
 * @copyright GNU Public License
 */


#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
#include "../include/game_rules_test.h"

#define MAX_TESTS 7

/** 
 * @brief Main function for Game rules unit tests. 
 * 
 * You may execute ALL or a SINGLE test
 *   1.- No parameter -> ALL test are executed 
 *   2.- A number means a particular test (the one identified by that number) 
 *       is executed
 *  
 */
int main(int argc, char** argv) {

    int test = 0;
    int all = 1;

    if (argc < 2) {
        printf("Running all test for module command:\n");
    } else {
        test = atoi(argv[1]);
        all = 0;
        printf("Running test %d:\t", test);
        if (test < 1 && test > MAX_TESTS) {
            printf("Error: unknown test %d\t", test);
            exit(EXIT_SUCCESS);
        }
    }
    if (all || test == 1) test1_game_rules_ini();
    if (all || test == 2) test1_game_rules_get();
    if (all || test == 3) test2_game_rules_get();
    if (all || test == 4) test1_game_rules_copy();
    if (all || test == 5) test2_game_rules_copy();
    if (all || test == 6) test1_game_rules_set();
    if (all || test == 7) test2_game_rules_set();

    PRINT_PASSED_PERCENTAGE;

    return 0;
}

void test1_game_rules_ini() {
    Rule* rule = NULL;
    rule = game_rules_ini();
    PRINT_TEST_RESULT(rule != NULL);
    game_rules_destroy(rule);
}

void test1_game_rules_get() {
    Rule* rule = NULL;

    rule = game_rules_ini();
    PRINT_TEST_RESULT(game_rules_get(rule) >= N_CMD && game_rules_get(rule) <= (N_POS+N_CMD));
    game_rules_destroy(rule);
}

void test2_game_rules_get() {
    Rule* rule = NULL;

    PRINT_TEST_RESULT(game_rules_get(rule) == N_CMD);
}

void test1_game_rules_copy() {
    Rule* rule = NULL, *copy = NULL;

    rule = game_rules_ini();
    copy = game_rules_copy(rule);
    PRINT_TEST_RESULT(copy != NULL);
    game_rules_destroy(rule);
    game_rules_destroy(copy);
}

void test2_game_rules_copy() {
    Rule* rule = NULL, *copy = NULL;

    copy = game_rules_copy(rule);
    PRINT_TEST_RESULT(copy == NULL);
    game_rules_destroy(rule);

}

void test1_game_rules_set() {
    Rule* rule = NULL;

    rule = game_rules_ini();
    PRINT_TEST_RESULT(game_rules_set(rule, 1) == OK);
    game_rules_destroy(rule);
}

void test2_game_rules_set() {
    Rule* rule = NULL;

    PRINT_TEST_RESULT(game_rules_set(rule, 1) == ERROR);
}
