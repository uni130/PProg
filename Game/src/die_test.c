/** 
 * @brief Tests if the die works correctly
 * 
 * @file die.h
 * @author Group 1
 * @version 2.0
 * @date 16-02-2018 
 * @copyright GNU Public License
 */

#include "../include/die.h"
#include <stdlib.h>
#include <stdio.h>

int main() {
    Die * d = NULL;
    int i;
    int count_1, count_2, count_3, count_4, count_5, count_6;

    d = die_create(1);
    for (i = count_1 = count_2 = count_3 = count_4 = count_5 = count_6 = 0; i < 100; i++) {
        die_roll(d);
        switch (die_value(d)) {
            case 1:
                count_1++;
                break;
            case 2:
                count_2++;
                break;
            case 3:
                count_3++;
                break;
            case 4:
                count_4++;
                break;
            case 5:
                count_5++;
                break;
            case 6:
                count_6++;
                break;

        }
        die_print(d);
    }

    printf("\nThe side 1 was recorded  %d times\nThe side 2 was recorded %d times\nThe side 3 was recorded %d times\nThe side 4 was recorded %d times\nThe side 5 was recorded  %d times\nThe side 6 was recorded  %d times\n", count_1, count_2, count_3, count_4, count_5, count_6);

    die_destroy(d);

    return 0;
}
