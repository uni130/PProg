/** 
 * @brief It implements the link logic
 *
 * @file link.c
 * @author Miguel Luque
 * @version 1.0
 * @date 30-03-2018 
 * @copyright GNU Public License
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/link.h"

struct _Link {
    Id id;
    char name[WORD_SIZE + 1];
    Id spaceid1;
    Id spaceid2;
    STATUS status;
};

Link * link_create() {
    Link * l = NULL;
    l = (Link*) malloc(sizeof (Link));
    if (!l) return NULL;
    return l;
}

STATUS link_destroy(Link * l) {
    if (!l) return ERROR;
    free(l);
    return OK;
}

Id link_get_space_id(Link * l, int n) {
    if (!l) return -1;
    if (n == 1) {
        return l->spaceid1;
    } else if (n == 2) {
        return l->spaceid2;
    } else {
        return -1;
    }
}

Id link_get_id(Link * l) {
    if (!l) return -1;
    return l->id;
}

char * link_get_name(Link * l) {
    if (!l){
	return NULL;
    }
    return l->name;
}

STATUS link_get_status(Link * l) {
    if (!l) {
        return OK;
    } else {
        return l->status;
    }
}

STATUS link_set_status(Link * l, STATUS s) {
    if (!l) return ERROR;
    l->status = s;
    return OK;
}

STATUS link_set_spaces(Link * l, Id sp1, Id sp2) {
    if (!l) return ERROR;
    l->spaceid1 = sp1;
    l->spaceid2 = sp2;
    return OK;
}

STATUS link_set_id(Link * l, Id id) {
    if (!l) return ERROR;
    l->id = id;
    return OK;
}

STATUS link_set_name(Link * l, char * name) {
    if (!l || !name) {
        return ERROR;
    }

    if (!strcpy(l->name, name)) {
        return ERROR;
    }

    return OK;    
}

STATUS link_print(Link * l, FILE * f) {
    if (!l || !f) {
        return ERROR;
    }
    fprintf(f, "#l:%ld|%s|%ld|%ld|%d|\n", l->id, l->name, l->spaceid1, l->spaceid2, l->status);
    return OK;
}
