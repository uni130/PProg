/** 
 * @brief It implements the dialogue test logic
 * 
 * @file dialogue_test.c
 * @author Group 1
 * @version 1.0
 * @date 05-24-2018 
 * @copyright GNU Public License
 */

#include "../include/dialogue_test.h"

#define MAX_TESTS 5


int main(int argc, char** argv) {

    int test = 0;
    int all = 1;

    if (argc < 2) {
        printf("Running all test for module command:\n");
    } else {
        test = atoi(argv[1]);
        all = 0;
        printf("Running test %d:\t", test);
        if (test < 1 && test > MAX_TESTS) {
            printf("Error: unknown test %d\t", test);
            exit(EXIT_SUCCESS);
        }
    }
    if (all || test == 1) test1_dialogue_validation();
    if (all || test == 2) test2_dialogue_validation();
    if (all || test == 3) test1_dialogue_error_validation();
    if (all || test == 4) test2_dialogue_error_validation();
    if (all || test == 5) test1_dialogue_repeat();


    PRINT_PASSED_PERCENTAGE;

    return 0;
}


void test1_dialogue_validation() {
    Command* cmd = NULL;
    cmd = command_ini();
    PRINT_TEST_RESULT(dialogue_validation(cmd) != NULL);
    command_destroy(cmd);
}

void test2_dialogue_validation() {
    Command* cmd = NULL;
    cmd = command_ini();
    PRINT_TEST_RESULT(dialogue_validation(cmd) != NULL);
    command_destroy(cmd);
}

void test1_dialogue_error_validation() {
    Command* cmd = NULL;
    cmd = command_ini();
    PRINT_TEST_RESULT(dialogue_validation(cmd) != NULL);
    command_destroy(cmd);
}

void test2_dialogue_error_validation() {
    Command* cmd = NULL;
    cmd = command_ini();
    PRINT_TEST_RESULT(dialogue_validation(cmd) != NULL);
    command_destroy(cmd);
}

void test1_dialogue_repeat() {
	PRINT_TEST_RESULT(dialogue_repeat() != NULL);
}

