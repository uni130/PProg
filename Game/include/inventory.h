/** 
 * @brief It defines TAD inventory
 * 
 * @file inventory.h
 * @author Mario Lopez
 * @version 1.0
 * @date 15-03-2018
 * @copyright GNU Public License
 */
#ifndef INVENTORY_H
#define INVENTORY_H

#include "../include/types.h"
#include "../include/set.h"

/**
 * @brief Capacity of the inventory
 */
#define MAXBACKPACK 4

/**
 * @brief Inventory
 *
 * Struct for storing Inventory
 */
typedef struct _Inventory Inventory;

/** 
 * @brief Creates Inventory
 * 
 * Reserves memory for a Inventory initialising all the fields
 *
 * @return Inventory* or NULL
 *	if everything worked correctly or not
 *	
 * @author Mario Lopez
 */
Inventory* inventory_create();

/** 
 * @brief Destroy Inventory
 *
 * Frees all the memory reserved for inventory
 *
 * @param inventory Inventory* that will be freed
 * @return OK or ERROR
 *	if everything worked correctly or not
 *	
 * @author Profesores PPROG, Group 1
 */
STATUS inventory_destroy(Inventory* inventory);

/**
 * @brief Add an object to the inventory
 *
 * Saves the Id of the object introduced inside the inventory
 *
 * @param inventory Inventory* where we save the object
 * @param id Id of the object we save
 * @return OK or ERROR
 *	if everything worked correctly or not
 */
STATUS inventory_set_id(Inventory* inventory, Id id);

/**
 * @brief Get the objects of the inventory
 *
 * Get a Set* whit all Ids of the objects in inventory
 *
 * @param inventory Inventory* where we take the objects
 * @return Set* or NULL
 *	if everything worked correctly or not
 */
Set* inventory_get_ids(Inventory* inventory);

/**
 * @brief Set a new capacity of objects 
 *
 * @param inventory Inventory* we want to change its capacity
 * @param num integer that sets the new maximun amount of objects
 * @return Set* or NULL
 *	if everything worked correctly or not
 */
STATUS inventory_set_maxObjects(Inventory* inventory, int num);

/**
 * @brief Get the capacity of the inventory
 *
 * @param inventory Inventory*
 * @return integer higher than 0 or -1
 *	if everything worked correctly or not
 */
int inventory_get_maxObjects(Inventory* inventory);

/**
 * @brief Check if inventory is full
 *
 * @param inventory Inventory*
 * @return BOOL(TRUE or FALSE) or FALSE
 *	if everything worked correctly or not
 */
BOOL inventory_full(Inventory* inventory);

/**
 * @brief Print inventory information
 *
 * @param inventory Inventory*
 * @return OK or ERROR
 *	if everything worked correctly or not
 */
STATUS inventory_print(Inventory* inventory);

#endif
