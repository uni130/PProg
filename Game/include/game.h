/** 
 * @brief It defines the game interface
 * for each command
 * 
 * @file game.h
 * @author Profesores PPROG, Group 1
 * @version 3.0
 * @date 5-04-2018 
 * @copyright GNU Public License
 */

#ifndef GAME_H
#define GAME_H

#include "../include/command.h"
#include "../include/space.h"
#include "../include/player.h"
#include "../include/object.h"
#include "../include/die.h"
#include "../include/link.h"
#include "../include/game_rules.h"

/**
 * @brief Game
 *
 * Struct for storing Game
 */
typedef struct _Game Game;

/** 
 * @brief Creates Game
 * 
 * Reserves memory for a Game initialising all the fields
 *
 * @return Game* or NULL
 *	if everything worked correctly or not
 *	
 * @author Profesores PPROG, Group 1
 */
Game* game_create();

/** 
 * @brief Updates Game with a file info
 *
 * Reads the file initialising the spaces and objects location  inside game and the player to the first space
 * 
 * @param game Game* we update
 * @param filename name of the textfile
 * @return OK or ERROR
 *	if everything worked correctly or not
 *	
 * @author Profesores PPROG, Group 1
 */
STATUS game_create_from_file(Game* game, char* filename);

/** 
 * @brief Updates Game with a Command
 *
 * Sets last_cmd to the one T_Command introduces and calls the corresponding callback to that comand
 * 
 * @param game Game* we update
 * @param cmd command
 * @return OK or ERROR
 *	if everything worked correctly or not
 *	
 * @author Profesores PPROG, Nicolás Serrano
 */
STATUS game_update(Game* game, Command* cmd);
/** 
 * @brief Updates Game with a Rule
 *
 * Sets last_rule to the one T_Rule introduces and calls the corresponding callback to that comand
 * 
 * @param game Game* we update
 * @param rule Rule*
 * @return OK or ERROR
 *	if everything worked correctly or not
 *	
 * @author Miguel Luque
 */
STATUS game_rule_update(Game* game, Rule* rule);
/** 
 * @brief Destroy Game
 *
 * Frees all the memory reserved for game
 *
 * @param game Game* that will be freed
 * @return OK or ERROR
 *	if everything worked correctly or not
 *	
 * @author Profesores PPROG, Group 1
 */
STATUS game_destroy(Game* game);

/** 
 * @brief Add space to game
 *
 * @param game 
 * @param space  
 * @return OK or ERROR
 *	if everything worked correctly or not
 *	
 * @author Profesores PPROG
 */
STATUS game_add_space(Game* game, Space* space);

/** 
 * @brief Add link to game
 *
 * @param game 
 * @param link 
 * @return OK or ERROR
 *	if everything worked correctly or not
 *	
 * @author Profesores PPROG
 */
STATUS game_add_link(Game* game, Link* link);

/** 
 * @brief Add a object to game
 *
 * @param game 
 * @param object
 * @return OK or ERROR
 *	if everything worked correctly or not
 *	
 * @author Miguel Luque
 */

STATUS game_add_object(Game* game, Object* object);

/** 
 * @brief Add player to game
 *
 * @param game 
 * @param player
 * @return OK or ERROR
 *	if everything worked correctly or not
 *	
 * @author Nicolas Serrano
 */


STATUS game_add_player(Game* game, Player* player);

/** 
 * @brief Search if game is over
 *
 * @param game
 * @return FALSE
 *	
 * @author Profesores PPROG
 */
BOOL game_is_over(Game* game);

/** 
 * @brief Print game information
 *
 * @param game 
 * @param f Open file
 *
 * @author Profesores PPROG, Group 1
 */
void game_print_data(Game* game, FILE *f);

/** 
 * @brief Get Space from Game
 *
 * @param game 
 * @param id Id of the Space that will be returned
 * @return Space* or NULL
 *	if everything worked correctly or not
 *	
 * @author Profesores PPROG
 */
Space* game_get_space(Game* game, Id id);

/** 
 * @brief Get Player from Game
 *
 * Get the Object* in that position
 *
 * @param game 
 * @param array_position
 * @return Object* or NULL
 *	if everything worked correctly or not
 *	
 * @author Nicolás Serrano
 */
Object* game_get_object(Game* game, int array_position);

/** 
 * @brief Get Player from Game
 *
 * @param game 
 * @return Player* or NULL
 *	if everything worked correctly or not
 *	
 * @author Nicolás Serrano
 */
Player* game_get_player(Game* game);

/** 
 * @brief Get Link from Game
 *
 * @param game 
 * @param id Id of the link
 * @return Link* or NULL
 *	if everything worked correctly or not
 *	
 * @author Nicolás Serrano
 */
Link* game_get_link(Game* game, Id id);

/** 
 * @brief Get Die from Game
 *
 * @param game 
 * @return Die* or NULL
 *	if everything worked correctly or not
 *	
 * @author Nicolás Serrano
 */
Die* game_get_die(Game* game);

/** 
 * @brief Get last command STATUS from Game
 *
 * @param game 
 * @return OK or ERROR
 *	if everything worked correctly or not
 *	
 * @author Nicolás Serrano
 */
STATUS game_get_callback_status(Game* game);

/** 
 * @brief Get preavious command STATUS from Game
 *
 * @param game 
 * @return OK or ERROR
 *	if everything worked correctly or not
 *	
 * @author Nicolás Serrano
 */
STATUS game_get_preavious_status(Game* game);

/** 
 * @brief Get Player location
 *
 * @param game 
 * @return a long or NO_ID
 *	if everything worked correctly or not
 *	
 * @author Profesores PPROG, Nicolás Serrano
 */
Id game_get_player_location(Game* game);

/** 
 * @brief Get Object location
 *
 * @param object 
 * @return a long or NO_ID
 *	if everything worked correctly or not
 *	
 * @author Profesores PPROG, Nicolás Serrano
 */
Id game_get_object_location(Object * object);

/** 
 * @brief Get Description
 *
 * Get the Description of last object or space checked
 *
 * @param game Game*
 * @return a char* or NULL
 *	if everything worked correctly or not
 *	
 * @author Nicolás Serrano
 */
char* game_get_description(Game* game);

/** 
 * @brief Get last Command
 *
 * @param game 
 * @return a Command*
 *	
 * @author Profesores PPROG, Nicolás Serrano
 */
Command* game_get_last_command(Game* game);
/** 
 * @brief Get last Rule
 *
 * @param game 
 * @return a Rule*
 *	
 * @author Miguel Luque
 */
Rule* game_get_last_rule(Game* game);
#endif
