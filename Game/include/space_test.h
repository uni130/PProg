/** 
 * @brief It declares the tests for the space module
 * 
 * @file space_test.h
 * @author Profesores Pprog updated by Miguel Luque
 * @version 1.0 
 * @date 06-04-2018
 * @copyright GNU Public License
 */

#ifndef SPACE_TEST_H
#define SPACE_TEST_H

/**
 * @test Test space creation
 * @pre Space ID 
 * @post Non NULL pointer to space 
 */
void test1_space_create();
/**
 * @test Test space creation
 * @pre Space ID 
 * @post Space_ID == Supplied Space Id
 */
void test2_space_create();

/**
 * @test Test function for space_name setting
 * @pre String with space name
 * @post Ouput==OK 
 */
void test1_space_set_name();
/**
 * @test Test function for space_name setting
 * @pre pointer to space = NULL 
 * @post Output==ERROR
 */
void test2_space_set_name();
/**
 * @test Test function for space_name setting
 * @pre pointer to space_name = NULL (point to space = NON NULL) 
 * @post Output==ERROR
 */
void test3_space_set_name();

/**
 * @test Test function for space_north setting
 * @pre pointer to space_north = 4 (point to space = NON NULL) 
 * @post Output==OK
 */
void test1_space_set_north();
/**
 * @test Test function for space_north setting
 * @pre pointer to space = NULL 
 * @post Output==ERROR
 */
void test2_space_set_north();

/**
 * @test Test function for space_south setting
 * @pre pointer to space_south = 4 (point to space = NON NULL) 
 * @post Output==OK
 */
void test1_space_set_south();
/**
 * @test Test function for space_south setting
 * @pre pointer to space = NULL 
 * @post Output==ERROR
 */
void test2_space_set_south();

/**
 * @test Test function for space_east setting
 * @pre pointer to space_east = 4 (point to space = NON NULL) 
 * @post Output==OK
 */
void test1_space_set_east();
/**
 * @test Test function for space_east setting
 * @pre pointer to space = NULL 
 * @post Output==ERROR
 */
void test2_space_set_east();

/**
 * @test Test function for space_west setting
 * @pre pointer to space_west = 4 (point to space = NON NULL) 
 * @post Output==OK
 */
void test1_space_set_west();
/**
 * @test Test function for space_west setting
 * @pre pointer to space = NULL 
 * @post Output==ERROR
 */
void test2_space_set_west();

/**
 * @test Test function for space_up setting
 * @pre pointer to space_up = 4 (point to space = NON NULL) 
 * @post Output==OK
 */
void test1_space_set_up();
/**
 * @test Test function for space_up setting
 * @pre pointer to space = NULL 
 * @post Output==ERROR
 */
void test2_space_set_up();

/**
 * @test Test function for space_down setting
 * @pre pointer to space_down = 4 (point to space = NON NULL) 
 * @post Output==OK
 */
void test1_space_set_down();
/**
 * @test Test function for space_down setting
 * @pre pointer to space = NULL 
 * @post Output==ERROR
 */
void test2_space_set_down();

/**
 * @test Test function for id getting
 * @pre pointer to space_id (point to space = NON NULL) 
 * @post Space_ID == Supplied Space Id
 */
void test1_space_get_id();
/**
 * @test Test function for id getting
 * @pre pointer to space = NULL 
 * @post Output==ERROR
 */
void test2_space_get_id();
/**
 * @test Test function for space_object setting
 * @pre pointer to space_object (point to space = NON NULL) 
 * @post Output==OK
 */

void test1_space_set_object();
/**
 * @test Test function for space_object setting
 * @pre pointer to space = NULL 
 * @post Space_ID == NO_ID
 */
void test2_space_set_object();
/**
 * @test Test function for name getting
 * @pre pointer to space_name (point to space = NON NULL) 
 * @post Space_name == Supplied Space name
 */

void test1_space_get_name();
/**
 * @test Test function for space_name setting
 * @pre pointer to space = NULL 
 * @post space_name == NULL
 */
void test2_space_get_name();
/**
 * @test Test function for space_north getting
 * @pre pointer to space_north (point to space = NON NULL) 
 * @post Space_north_id == Supplied Space north id
 */

void test1_space_get_north();
/**
 * @test Test function for space_north getting
 * @pre pointer to space = NULL 
 * @post space_north == NO_ID
 */
void test2_space_get_north();
/**
 * @test Test function for space_south getting
 * @pre pointer to space_south (point to space = NON NULL) 
 * @post Space_south_id == Supplied Space south id
 */

void test1_space_get_south();
/**
 * @test Test function for space_south getting
 * @pre pointer to space = NULL 
 * @post space_south == NO_ID
 */
void test2_space_get_south();
/**
 * @test Test function for space_east getting
 * @pre pointer to space_east (point to space = NON NULL) 
 * @post Space_east_id == Supplied Space east id
 */

void test1_space_get_east();
/**
 * @test Test function for space_east getting
 * @pre pointer to space = NULL 
 * @post space_east == NO_ID
 */
void test2_space_get_east();

/**
 * @test Test function for space_west getting
 * @pre pointer to space_west (point to space = NON NULL) 
 * @post Space_west_id == Supplied Space west id
 */
void test1_space_get_west();
/**
 * @test Test function for space_west getting
 * @pre pointer to space = NULL 
 * @post space_west == NO_ID
 */
void test2_space_get_west();

/**
 * @test Test function for space_up getting
 * @pre pointer to space_up (point to space = NON NULL) 
 * @post Space_up_id == Supplied Space up id
 */
void test1_space_get_up();
/**
 * @test Test function for space_up getting
 * @pre pointer to space = NULL 
 * @post space_up == NO_ID
 */
void test2_space_get_up();

/**
 * @test Test function for space_down getting
 * @pre pointer to space_down (point to space = NON NULL) 
 * @post Space_down_id == Supplied Space down id
 */
void test1_space_get_down();
/**
 * @test Test function for space_down getting
 * @pre pointer to space = NULL 
 * @post space_down == NO_ID
 */
void test2_space_get_down();

/**
 * @test Test function for space_objects getting
 * @pre pointer to space_objects (point to space = NON NULL) 
 * @post space_objects != NULL
 */
void test1_space_get_objects();
/**
 * @test Test function for space_object_set getting
 * @pre pointer to space = NULL 
 * @post space_objects == NULL
 */
void test2_space_get_objects();

/**
 * @test Test function for space_description setting
 * @pre pointer to space_description (point to space = NON NULL) 
 * @post Output==OK
 */
void test1_space_set_description();
/**
 * @test Test function for space_description setting
 * @pre pointer to space = NULL 
 * @post Output==ERROR
 */
void test2_space_set_description();
/**
 * @test Test function for space_description getting
 * @pre pointer to space_description (point to space = NON NULL) 
 * @post Space_description == Supplied Space description
 */
void test1_space_get_description();
/**
 * @test Test function for space_description getting
 * @pre pointer to space = NULL 
 * @post Output==ERROR
 */
void test2_space_get_description();

/**
 * @test Test function for space_descriptioff setting
 * @pre pointer to space_descriptioff (point to space = NON NULL) 
 * @post Output==OK
 */
void test1_space_set_descriptioff();
/**
 * @test Test function for space_descriptioff setting
 * @pre pointer to space = NULL 
 * @post Output==ERROR
 */
void test2_space_set_descriptioff();
/**
 * @test Test function for space_descriptioff getting
 * @pre pointer to space_descriptioff (point to space = NON NULL) 
 * @post Space_descriptioff == Supplied Space description
 */
void test1_space_get_descriptioff();
/**
 * @test Test function for space_descriptioff getting
 * @pre pointer to space = NULL 
 * @post Output==ERROR
 */
void test2_space_get_descriptioff();

/**
 * @test Test function for space_gdesc setting
 * @pre pointer to space_gdesc (point to space = NON NULL) 
 * @post Output==OK
 */
void test1_space_set_gdesc();
/**
 * @test Test function for space_graphic_description setting
 * @pre pointer to space = NULL 
 * @post Output==ERROR
 */
void test2_space_set_gdesc();
/**
 * @test Test function for space_gdesc getting
 * @pre pointer to space_gdesc (point to space = NON NULL) 
 * @post Space_gdesc selected line == Supplied Space gdesc line
 */
void test1_space_get_gdesc();
/**
 * @test Test function for space_graphic_description getting
 * @pre pointer to space = NULL 
 * @post Output==ERROR
 */
void test2_space_get_gdesc();

/**
 * @test Test function for space_illumination setting
 * @pre pointer to space (point to space = NON NULL) and BOOL  
 * @post Output==OK
 */
void test1_space_set_illumination();
/**
 * @test Test function for space_illumination setting
 * @pre pointer to space = NULL and BOOL  
 * @post Output==ERROR
 */
void test2_space_set_illumination();
/**
 * @test Test function for space_illumination setting
 * @pre pointer to space (point to space = NON NULL) and NON BOOL  
 * @post Output==OK
 */
void test3_space_set_illumination();
/**
 * @test Test function for space_illumination getting
 * @pre pointer to space (point to space = NON NULL)  
 * @post space_illumination==TRUE
 */
void test1_space_get_illumination();
/**
 * @test Test function for space_illumination setting
 * @pre pointer to space  = NULL  
 * @post Output==ERROR
 */
void test2_space_get_illumination();
#endif
