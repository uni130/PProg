/** 
 * @brief It defines the game management
 * 
 * @file game_management.h
 * @author Group 1
 * @version 3.0
 * @date 04-04-2018 
 * @copyright GNU Public License
 */

#ifndef GAME_management_H
#define GAME_management_H

#include "../include/game.h"

/** 
 * @brief Load Spaces from a file
 * 
 * Reads the textfile and interpretates the map for you to be able to move between the spaces
 *
 * @param game Game* where we load the spaces
 * @param filename name of the textfile
 * @return OK or ERROR
        if everything worked correctly or not
 *
 * @author Profesores PPROG 
 */
STATUS game_management_load_spaces(Game* game, char* filename);

/** 
 * @brief Load Objects from a file
 * 
 * Reads the textfile and interpretates objects for you to be able to interact with them
 *
 * @param game Game* where we load the objects
 * @param filename name of the textfile
 * @return OK or ERROR
        if everything worked correctly or not
 *
 * @author Miguel Luque 
 */
STATUS game_management_load_objects(Game* game, char* filename);

/** 
 * @brief Load Links from a file
 * 
 * Reads the textfile and interpretates links for you to be able to interact with them
 *
 * @param game Game* where we load the objects
 * @param filename name of the textfile
 * @return OK or ERROR
        if everything worked correctly or not
 *
 * @author Miguel Luque 
 */

STATUS game_management_load_links(Game* game, char* filename);

/** 
 * @brief Load Player from a file
 * 
 * Reads the textfile and interpretates player
 *
 * @param game Game* where we load the objects
 * @param filename name of the textfile
 * @return OK or ERROR
        if everything worked correctly or not
 *
 * @author Nicolas Serrano
 */
STATUS game_management_load_player(Game* game, char* filename);

/** 
 * @brief Save Game
 * 
 * Write a textfile with all game information
 *
 * @param game Game* 
 * @param filename name of the textfile
 * @return OK or ERROR
        if everything worked correctly or not
 *
 * @author Nicolas Serrano
 */
STATUS game_management_save(Game* game, char* filename);

/** 
 * @brief Load Game
 * 
 * Reads the textfile loading all game information
 *
 * @param game Game* 
 * @param filename name of the textfile
 * @return OK or ERROR
        if everything worked correctly or not
 *
 * @author Nicolas Serrano
 */
STATUS game_management_load(Game* game, char* filename);


#endif
