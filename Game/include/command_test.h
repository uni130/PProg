/** 
 * @brief It implements the command test logic
 * 
 * @file command_test.h
 * @author Nicolas Serrano
 * @version 1.0
 * @date 05-04-2018 
 * @copyright GNU Public License
 */

#ifndef COMMAND_TEST_H
#define COMMAND_TEST_H

#include "../include/command.h"
#include "../include/test.h"

/**
 * @test Test command creation 
 * @post Command != NULL 
 */
void test1_command_ini();

/**
 * @test Test function for getting command->cmd
 * @pre Initialized Command
 * @post T_Command == NO_CMD
 */
void test1_command_get_cmd();

/**
 * @test Test function for getting command->cmd
 * @pre Uninitialized Command
 * @post T_Command == NO_CMD
 */
void test2_command_get_cmd();

/**
 * @test Test function for getting command->object 
 * @pre Initialized Command
 * @post char* != NULL
 */
void test1_command_get_object();

/**
 * @test Test function for getting command->object 
 * @pre NULL
 * @post char* == NULL
 */
void test2_command_get_object();

/**
 * @test Test function for copy command
 * @pre Initialized Command
 * @post Copy != NULL 
 */
void test1_command_copy();

/**
 * @test Test function for copy command
 * @pre NULL
 * @post Copy == NULL 
 */
void test2_command_copy();

/**
 * @test Test function for getting command->parameter2
 * @pre Initialized Command
 * @post char* != NULL
 */
void test1_command_get_parameter2();

/**
 * @test Test function for getting command->parameter2 
 * @pre NULL
 * @post char* == NULL
 */
void test2_command_get_parameter2();

/**
 * @test Test function command_compare_to_last
 * @pre Initialized Command
 * @post char* == FALSE
 */
void test1_command_compare_to_last();

/**
 * @test Test function command_compare_to_last 
 * @pre NULL
 * @post char* == FALSE
 */
void test2_command_compare_to_last();

#endif

