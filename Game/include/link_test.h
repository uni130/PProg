/** 
 * @brief It declares the tests for the link module
 * 
 * @file link_test.h
 * @author Miguel Luque
 * @version 1.0 
 * @date 07-04-2018
 * @copyright GNU Public License
 */
#ifndef LINK_TEST_H
#define LINK_TEST_H
#include "../include/link.h"
#include "../include/test.h"
/**
 * @test Test link creation
 * @pre none
 * @post Non NULL pointer to link 
 */
void test1_link_create();
/**
 * @test Test function for space id getting
 * @pre pointer to link_space_id(1 or 2) (point to link = NON NULL) 
 * @post link selected space id == supplied id
 */
void test1_link_get_space_id();
/**
 * @test Test function for space id setting
 * @pre pointer to link = NULL 
 * @post Output==NO_ID
 */
void test2_link_get_space_id();
/**
 * @test Test function for id getting
 * @pre pointer to link_id (point to link = NON NULL) 
 * @post link_ID == Supplied link Id
 */
void test1_link_get_id();
/**
 * @test Test function for id setting
 * @pre pointer to link = NULL 
 * @post Output==ERROR
 */
void test2_link_get_id();
/**
 * @test Test function for name getting
 * @pre pointer to link_name
 (point to link = NON NULL) 
 * @post link_name == Supplied link name
 */
void test1_link_get_name();
/**
 * @test Test function for link_name getting
 * @pre pointer to link = NULL 
 * @post Output==ERROR
 */
void test2_link_get_name();
/**
 * @test Test function for status getting
 * @pre pointer to link_status (point to link = NON NULL) 
 * @post link_status == supplied status
 */
void test1_link_get_status();
/**
 * @test Test function for status getting
 * @pre pointer to link_status (point to link = NON NULL) 
 * @post link_status == supplied status
 */
void test2_link_get_status();
/**
 * @test Test function for link_status setting
 * @pre pointer to link_status (point to link = NON NULL) 
 * @post Output==OK
 */
void test1_link_set_status();
/**
 * @test Test function for link_status setting
 * @pre pointer to link = NULL 
 * @post Output==ERROR
 */
void test2_link_set_status();
/**
 * @test Test function for link_spaces setting
 * @pre pointer to link_spaces (point to link = NON NULL) 
 * @post Output==OK
 */
void test1_link_set_spaces();
/**
 * @test Test function for link_spaces setting
 * @pre pointer to link = NULL 
 * @post Output==ERROR
 */
void test2_link_set_spaces();
/**
 * @test Test function for link_id setting
 * @pre pointer to link_id (point to link = NON NULL) 
 * @post Output==OK
 */
void test1_link_set_id();
/**
 * @test Test function for link_id setting
 * @pre pointer to link = NULL 
 * @post Output==ERROR
 */
void test2_link_set_id();
/**
 * @test Test function for link_name setting
 * @pre pointer to link_name (point to link = NON NULL) 
 * @post Output==OK
 */
void test1_link_set_name();
/**
 * @test Test function for link_name setting
 * @pre pointer to link = NULL 
 * @post Output==ERROR
 */
void test2_link_set_name();

#endif
