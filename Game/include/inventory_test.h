/** 
 * @brief It declares the tests for the inventory module
 * 
 * @file inventory_test.h
 * @author Mario Lopez
 * @version 1.0 
 * @date 05-04-2018
 * @copyright GNU Public License
 */

#ifndef INVENTORY_TEST_H
#define INVENTORY_TEST_H
#include "../include/inventory.h"
#include "../include/test.h"

/**
 * @test Test inventory creation
 * @pre Inventory 
 * @post Non NULL pointer to inventory 
 */
void test1_inventory_create();

/**
 * @test Test inventory destruction
 * @pre created inventory
 * @post OK
 */
void test1_inventory_destroy();

/**
 * @test Test inventory destruction
 * @pre Inventory without memory
 * @post ERROR
 */
void test2_inventory_destroy();

/**
 * @test Test the introduction of an id in an inventory
 * @pre Inventory and an id
 * @post OK
 */
void test1_inventory_set_id();

/**
 * @test Test the introduction of an id in an inventory
 * @pre Inventory and a NO_ID
 * @post ERROR
 */
void test2_inventory_set_id();

/**
 * @test Test the introduction of the maxObjects int in an inventory
 * @pre Inventory and an int
 * @post OK
 */
void test1_inventory_set_maxObjects();

/**
 * @test Test the introduction of the maxObjects int in an inventory
 * @pre Inventory and a negative number
 * @post ERROR
 */
void test2_inventory_set_maxObjects();

/**
 * @test Test the return of the set of ids from an inventory
 * @pre Inventory
 * @post the set of ids
 */
void test1_inventory_get_ids();

/**
 * @test Test the return of the set of ids from an inventory
 * @pre uninitialliced inventory
 * @post ERROR
 */
void test2_inventory_get_ids();

/**
 * @test Test the return of the maxObjects int from an inventory
 * @pre Inventory
 * @post the maxObjects int
 */
void test1_inventory_get_maxObjects();

/**
 * @test Test the return of the maxObjects int from an inventory
 * @pre uninitialliced inventory
 * @post ERROR
 */
void test2_inventory_get_maxObjects();


#endif
