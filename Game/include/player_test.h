/** 
 * @brief It declares the tests for the player module
 * 
 * @file player_test.h
 * @author Javier Sanmiguel
 * @version 1.0 
 * @date 6-04-2018
 * @copyright GNU Public License
 */


#ifndef PLAYER_TEST_H
#define PLAYER_TEST_H

#include "../include/player.h"
#include "../include/test.h"

/**
 * @test Test player creation
 * @pre PLAYER ID
 * @post Non NULL pointer to player 
 */
void test1_player_create();
/**
 * @test Test player creation
 * @pre PLAYER ID
 * @post Correct player ID 
 */
void test2_player_create();
/**
 * @test Test player name setting
 * @pre PLAYER name
 * @post OK
 */
void test1_player_set_name();
/**
 * @test Test player name setting
 * @pre NULL PLAYER 
 * @post ERROR
 */
void test2_player_set_name();
/**
 * @test Test player name setting
 * @pre NULL name
 * @post ERROR
 */
void test3_player_set_name();
/**
 * @test Test player set location
 * @pre location ID
 * @post OK
 */
void test1_player_set_location();
/**
 * @test Test player set location
 * @pre NULL PLAYER
 * @post ERROR
 */
void test2_player_set_location();
/**
 * @test Test player set location
 * @pre location NO_ID
 * @post ERROR
 */
void test3_player_set_location();
/**
 * @test Test player set object
 * @pre object ID
 * @post OK
 */
void test1_player_set_object();
/**
 * @test Test player set object
 * @pre NULL PLAYER
 * @post ERROR
 */
void test2_player_set_object();
/**
 * @test Test player set object
 * @pre NO_ID
 * @post ERROR
 */
void test3_player_set_object();
/**
 * @test Test player get id
 * @pre PLAYER
 * @post 1
 */
void test1_player_get_id(); 
/**
 * @test Test player get id
 * @pre NULL PLAYER
 * @post ERROR
 */
void test2_player_get_id(); 
/**
 * @test Test player get name
 * @pre PLAYER
 * @post "Jst"
 */
void test1_player_get_name();
/**
 * @test Test player get name
 * @pre NULL PLAYER
 * @post ERROR
 */
void test2_player_get_name();
/**
 * @test Test player get location
 * @pre PLAYER
 * @post NO_ID
 */
void test1_player_get_location();
/**
 * @test Test player get location
 * @pre NULL PLAYER
 * @post ERROR
 */
void test2_player_get_location();
/**
 * @test Test player get inventory
 * @pre PLAYER
 * @post !NULL
 */
void test1_player_get_inventory();
/**
 * @test Test player get inventory
 * @pre NULL PLAYER
 * @post ERROR
 */
void test2_player_get_inventory();
/**
 * @test Test player get object
 * @pre PLAYER
 * @post !NULL
 */
void test1_player_get_object();
/**
 * @test Test player get object
 * @pre NULL PLAYER
 * @post ERROR
 */
void test2_player_get_object();


#endif
