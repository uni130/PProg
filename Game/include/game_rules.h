/** 
 * @brief It implements the game_rules logic
 * 
 * @file game_rules.h
 * @author Miguel Luque
 * @version 1.0
 * @date 03-05-2018
 * @copyright GNU Public License
 */

#ifndef GAME_RULES_H
#define GAME_RULES_H

#include "../include/types.h"
#include "../include/command.h"
#define N_POS 7
typedef enum enum_Rule {
    NO_RULE = (N_CMD-1), /*!< No rule */
    TELEPORT, /*!< Teleport */
    BUSTED, /*!< Busted */
    PATH, /*!< Path */
    FIND, /*!< Find */
    LOSE, /*!< Lose */
    NIGHT /*!< Night */
} T_Rule;
/**
 * @brief rule
 *
 * Struct for storing rule
 */
typedef struct _Rule Rule;


/** 
 * @brief Creates Rule
 * 
 * Reserves memory for a Rule initialising all the fields
 *
 * @return Rule* or NULL
 *	if everything worked correctly or not
 *
 * @author Miguel Luque 
 */
Rule* game_rules_ini();

/** 
 * @brief Destroy Rule
 * 
 * Frees all the memory reserved for cmd
 *
 * @param cmd Rule* that will be freed
 *
 * @author Miguel Luque
 */
void game_rules_destroy(Rule* rule);

/** 
 * @brief Get user imput
 * 
 * @param cmd Rule* where we save the last rule introduced 
 * @return OK or ERROR,
 *	if everything worked correctly or not
 *
 * @author: Miguel Luque
 */
STATUS game_rules_set(Rule* cmd,int i);

/** 
 * @brief Get rule alredy saved
 * 
 * @param cmd Rule* where we take the last rule introduced and an integer
 * @return The corresponding T_Rule saved or N_CMD
 *	if everything worked correctly or not
 *
 * @author: Miguel Luque
 */
T_Rule game_rules_get(Rule* rule);


/** 
 * @brief Copy Rule*
 * 
 * @param cmd Rule* we copy 
 * @return Rule* or NULL,
 *	if the copy was done correctly or not
 *
 * @author: Nicolás Serrano 
 */
Rule* game_rules_copy(const Rule *rule);

#endif
