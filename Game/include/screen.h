/** 
 * @brief It defines a screen
 * 
 * @file screen.h
 * @author Profesores PPROG
 * @version 1.0 
 * @date 11-01-2017
 * @copyright GNU Public License
 */

#ifndef __SCREEN__
#define __SCREEN__

/**
* @brief Maximun string lenght
*/
#define SCREEN_MAX_STR 80

/**
* @brief Area
*
* Struct for storing Area
*/
typedef struct _Area Area;

/** 
* @brief Creates Screen
* 
* Initialises the interface for the user.
*
* @author Profesores PPROG
*/
void screen_init();

/** 
* @brief Destroy Screen
* 
* Releases the data used for the user's interface
*
* @author Profesores PPROG
*/
void screen_destroy();

/** 
* @brief Paint Screen information
* 
* Fills the terminal with the data-
*
* @author Profesores PPROG
*/
void screen_paint();

/** 
* @brief Get char* information
* 
* It receives a char*, reads the new string of columns and replaces the old one
*
* @param str char*
*
* @author Profesores PPROG
*/
void screen_gets(char *str);

/** 
* @brief Create area
* 
* Create Area*, initializates the area limits for the interface
*
* @param x integer
* @param y integer
* @param width integer
* @param height integer 
* @return Area* or NULL
*	if everything worked correctly or not
*
* @author Profesores PPROG
*/
Area* screen_area_init(int x, int y, int width, int height);

/** 
* @brief Destroy area
*
* Frees all the memory reserved for area
*
* @param area Area* that will be freed
* @return OK or ERROR
*	if everything worked correctly or not
*	
* @author Profesores PPROG
*/
void screen_area_destroy(Area* area);

/** 
* @brief Clean area
*
* Resets the area
*
* @param area Area* that will be reseted
* 	
* @author Profesores PPROG
*/
void screen_area_clear(Area* area);

/** 
* @brief Reset area cursor
*
* Stablishes the dimensions of the area of the cursor
*
* @param area Area* that will be stablished
* 	
* @author Profesores PPROG
*/
void screen_area_reset_cursor(Area* area);

/** 
* @brief Centers area
*
* Centers the area by the stablished dimensions
* 
* @param area Area* that will be stablished
* @param str char* 
*	
* @author Profesores PPROG
*/
void screen_area_puts(Area* area, char *str);

#endif
