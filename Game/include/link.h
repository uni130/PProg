/** 
 * @brief It implements the link logic
 *
 * @file link.h
 * @author Miguel Luque
 * @version 1.0
 * @date 30-03-2018 
 * @copyright GNU Public License
 */

#ifndef LINK_H
#define LINK_H

#include <stdio.h>
#include <stdlib.h>

#include "../include/types.h"

/**
 * @brief Link
 *
 * Struct for storing Link
 */

typedef struct _Link Link;

/** 
 * @brief Creates Link
 * 
 * Reserves memory for a Link initialising all the fields
 *
 * @return Link* or NULL
 *	if everything worked correctly or not
 *
 * @author Miguel Luque 
 */
Link* link_create();

/** 
 * @brief Destroy Link
 * 
 * Frees all the memory reserved for link
 *
 * @param link Link* that will be freed
 * @return OK or ERROR,
 *	if everything worked correctly or not
 *
 * @author Miguel Luque
 */
STATUS link_destroy(Link* link);


/** 
 * @brief Get Id of space n
 * 
 * Gets the id of the space in n position
 *
 * @param link Link*
 * @param n integer 
 * @return Id or NO_ID,
 *	if everything worked correctly or not
 *
 * @author: Miguel Luque
 */
Id link_get_space_id(Link* link, int n);

/** 
 * @brief Get link Id
 * 
 * @param link Link*
 * @return Id or NO_ID,
 *	if everything worked correctly or not
 *
 * @author: Miguel Luque
 */
Id link_get_id(Link * link);

/** 
 * @brief Get link name
 * 
 * @param link Link*
 * @return char* or NULL,
 *	if everything worked correctly or not
 *
 * @author: Miguel Luque
 */
char* link_get_name(Link* link);

/** 
 * @brief Get link STATUS
 * 
 * Get the position of the link open(OK) or closed(ERROR)
 *
 * @param link Link*
 * @return STATUS or ERROR,
 *	if everything worked correctly or not
 *
 * @author: Miguel Luque
 */
STATUS link_get_status(Link* link);

/** 
 * @brief Set link STATUS to status
 * 
 * Set the position of the link open(OK) or closed(ERROR)
 *
 * @param link Link*
 * @param status STATUS
 * @return OK or ERROR,
 *	if everything worked correctly or not
 *
 * @author: Miguel Luque
 */
STATUS link_set_status(Link* link, STATUS status);

/** 
 * @brief Set link spaces
 * 
 * Set the spaces 1 and 2 of the link with the ids given(in order)
 *
 * @param link Link*
 * @param sp1 Id of first space
 * @param sp2 Id of second space
 * @return OK or ERROR,
 *	if everything worked correctly or not
 *
 * @author: Miguel Luque
 */
STATUS link_set_spaces(Link* link, Id sp1, Id sp2);

/** 
 * @brief Set link Id
 * 
 * @param link Link*
 * @param id Id 
 * @return OK or ERROR,
 *	if everything worked correctly or not
 *
 * @author: Miguel Luque
 */
STATUS link_set_id(Link* link, Id id);

/** 
 * @brief Set link name
 * 
 * @param link Link*
 * @param name char*
 * @return OK or ERROR,
 *	if everything worked correctly or not
 *
 * @author: Miguel Luque
 */
STATUS link_set_name(Link* link, char * name);

/** 
 * @brief Print link information
 * 
 * Prints the link content in the file given
 *
 * @param link Link*
 * @param filename FILE*
 * @return OK or ERROR,
 *	if everything worked correctly or not
 *
 * @author: Miguel Luque
 */
STATUS link_print(Link * link, FILE * filename);
#endif
