/** 
 * @brief It defines the TAD object
 * 
 * @file object.h
 * @author Group 1
 * @version 3.0 
 * @date 03-04-2018
 * @copyright GNU Public License
 */

#ifndef OBJECT_H
#define OBJECT_H

#include "../include/types.h"

/**
 * @brief Object
 *
 * Struct for storing object
 */
typedef struct _Object Object;

/** 
 * @brief Create Object
 * 
 * Reserves memory for an Object initialising all the fields
 *
 * @param id Id of the object
 * @return Object* or NULL
 *	if everything worked correctly or not
 *	
 * @author Group 1
 */
Object* object_create(Id id);

/** 
 * @brief Destroy object
 * 
 * Frees all the memory reserved for object
 *
 * @param object Object* that will be freed
 * @return OK or ERROR
 *	if everything worked correctly or not
 *	
 * @author Miguel Luque
 */
STATUS object_destroy(Object* object);

/** 
 * @brief Set object name
 * 
 * Sets the name of the object as the introduced char*
 *
 * @param object Object*
 * @param name char* with the new name of the object
 * @return OK or ERROR
 *	if everything worked correctly or not
 *	
 * @author Miguel Luque
 */
STATUS object_set_name(Object* object, char* name);

/** 
 * @brief Set object description
 * 
 * Sets the description of the object as the introduced char*
 *
 * @param object Object*
 * @param description char* with the new description of the object
 * @return OK or ERROR
 *	if everything worked correctly or not
 *	
 * @author Nicolas Serrano
 */
STATUS object_set_description(Object* object, char* description);

/** 
 * @brief Set object description2
 * 
 * Sets the description2 of the object as the introduced char*
 *
 * @param object Object*
 * @param description char* with the new description of the object
 * @return OK or ERROR
 *	if everything worked correctly or not
 *	
 * @author Mario Lopez
 */
STATUS object_set_description2(Object* object, char* description);

/** 
 * @brief Set object location
 * 
 * Sets the location of the object as the introduced one
 *
 * @param object Object*
 * @param id Id with the new location of the object
 * @return OK or ERROR
 *	if everything worked correctly or not
 *	
 * @author Miguel Luque
 */
STATUS object_set_location(Object* object, Id id);

STATUS object_set_movable(Object* object, BOOL b);

STATUS object_set_moved(Object* object, BOOL b);

STATUS object_set_hidden(Object* object, BOOL b);

STATUS object_set_open(Object* object, Id id);

STATUS object_set_illuminate(Object* object, BOOL b);

STATUS object_set_switchedOn(Object* object, BOOL b);

/** 
 * @brief Get object Id
 *
 * @param object Object*
 * @return Id or NO_ID
 *	if everything worked correctly or not
 *	
 * @author Miguel Luque
 */
Id object_get_id(Object* object);

/** 
 * @brief Get object name
 *
 * @param object Object*
 * @return char* with the name or NULL
 *	if everything worked correctly or not
 *	
 * @author Miguel Luque
 */
const char* object_get_name(Object* object);

/** 
 * @brief Get object description
 *
 * @param object Object*
 * @return char* with the description or NULL
 *	if everything worked correctly or not
 *	
 * @author Nicolas Serrano
 */
char* object_get_description(Object* object);

/** 
 * @brief Get object description2
 *
 * @param object Object*

 * @return char* with the description or NULL
 *	if everything worked correctly or not
 *	
 * @author Mario Lopez
 */
char* object_get_description2(Object* object);

/** 
 * @brief Get object location Id
 * 
 * Get the Id of the location of object
 *
 * @param object Object*
 * @return Id or NO_ID
 *	if everything worked correctly or not
 *	
 * @author Miguel Luque
 */
Id object_get_location(Object* object);

BOOL object_get_movable(Object* object);

BOOL object_get_moved(Object* object);

BOOL object_get_hidden(Object* object);

Id object_get_open(Object* object);

BOOL object_get_illuminate(Object* object);

BOOL object_get_switchedOn(Object* object);
/** 
 * @brief Print object information in a file
 *
 * @param object Object*
 * param f FILE*
 * @return OK or ERROR
 *	if everything worked correctly or not
 *	
 * @author Miguel Luque
 */
STATUS object_print(Object* object, FILE *f);


#endif
