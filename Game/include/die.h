/** 
 * @brief It implements the die logic
 * 
 * @file die.h
 * @author Group 1
 * @version 2.0
 * @date 16-02-2018 
 * @copyright GNU Public License
 */

#ifndef DIE_H
#define DIE_H

#include "types.h"

/**
 * @brief Die
 *
 * Struct for storing Die
 */
typedef struct _Die Die;

/** 
 * @brief Creates Die
 * 
 * Reserves memory for a Die initialising all the fields
 *
 * @param id Die Id
 * @return Die* or NULL
        if everything worked correctly or not
 *
 * @author Miguel Luque, updated by Nicolás Serrano 
 */
Die* die_create(Id id);

/** 
 * @brief Get random number
 * 
 * Returns a random number between 2 ints
 *
 * @param a integer
 * @para  b integer
 * @return int between a and b or -1
        if everything worked correctly or not
 *
 * @author Miguel Luque
 */
int die_random_num(int a, int b);
/** 
 * @brief Destroy Die
 * 
 * Frees all the memory reserver for die
 *
 * @param d Die* that will be freed
 * 
 * @author Miguel Luque 
 */
void die_destroy(Die * d);

/** 
 * @brief Roll Die
 * 
 * Set a random number between 1 and 6 for the die
 *
 * @param d Die* will be rolled
 *
 * @author Miguel Luque 
 */
void die_roll(Die * d);

/** 
 * @brief Get Die value
 *
 * @param d Die* where we get its value
 * @return integer between 1 and 6 or -1
        if everything worked correctly or not
 * 
 * @author Miguel Luque 
 */
int die_value(Die *d);

/** 
 * @brief Print Die info
 *
 * @param d Die* we wants to print its info
 * @return OK or ERROR
        if everything worked correctly or not
 * 
 * @author Miguel Luque 
 */
STATUS die_print(Die *d);
#endif
