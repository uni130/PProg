/** 
 * @brief It defines the TAD player
 * 
 * @file player.h
 * @author Group 1
 * @version 1.0 
 * @date 15-02-2018
 * @copyright GNU Public License
 */

#ifndef PLAYER_H
#define PLAYER_H

#include "../include/space.h"
#include "../include/object.h"
#include "../include/inventory.h"
#include <stdio.h>

/**
 * @brief Player
 *
 * Struct for storing Player
 */typedef struct _Player Player;

/** 
 * @brief Creates Player
 * 
 * Reserves memory for a player initialising all the fields
 *
 * @param id Id of the player
 * @return Player* or NULL
 *	if everything worked correctly or not
 *	
 * @author Nicolas Serrano
 */
Player* player_create(Id id);

/** 
 * @brief Destroy Player
 *
 * Frees all the memory reserved for player
 *
 * @param player Player* that will be freed
 * @return OK or ERROR
 *	if everything worked correctly or not
 *	
 * @author Nicolás Serrano
 */
STATUS player_destroy(Player* player);

/** 
 * @brief Set player name
 * 
 * Sets the name of the player as the introduced char*
 *
 * @param player Player*
 * @param name char* with the new name of the player
 * @return OK or ERROR
 *	if everything worked correctly or not
 *	
 * @author Nicolas Serrano
 */
STATUS player_set_name(Player* player, char* name);

/** 
 * @brief Set player location
 * 
 * Sets the location of the player as the introduced one
 *
 * @param player Player*
 * @param id Id with the new location of the player
 * @return OK or ERROR
 *	if everything worked correctly or not
 *	
 * @author Nicolas Serrano
 */
STATUS player_set_location(Player* player, Id id);

/** 
 * @brief Set player object
 * 
 * Sets the object in the player as the introduced one
 *
 * @param player Player*
 * @param id Id with the new location of the player
 * @return OK or ERROR
 *	if everything worked correctly or not
 *	
 * @author Nicolas Serrano
 */
STATUS player_set_object(Player* player, Id id);

/** 
 * @brief Get player Id
 *
 * @param player Player*
 * @return Id or NO_ID
 *	if everything worked correctly or not
 *	
 * @author Nicolas Serrano
 */
Id player_get_id(Player* player);

/** 
 * @brief Get player name
 *
 * @param player Player*
 * @return char* with the name or NULL
 *	if everything worked correctly or not
 *	
 * @author Nicolas Serrano
 */
const char* player_get_name(Player* player);

/** 
 * @brief Get player location Id
 * 
 * Get the Id of the location of player
 *
 * @param player Player*
 * @return Id or NO_ID
 *	if everything worked correctly or not
 *	
 * @author Nicolas Serrano
 */
Id player_get_location(Player* player);

/** 
 * @brief Get player inventory
 * 
 * @param player Player*
 * @return Inventory* or NULL
 *	if everything worked correctly or not
 *	
 * @author Nicolas Serrano
 */
Inventory* player_get_inventory(Player* player);

/** 
 * @brief Get player objects
 * 
 * Get the Set of the objects inside player_inventory
 *
 * @param player Player*
 * @return Set* or NULL 
 *	if everything worked correctly (or there is no object) or not
 *	
 * @author Nicolas Serrano
 */
Set* player_get_object(Player* player);

/** 
 * @brief Print player information in a file
 *
 * @param player Player*
 * @param f FILE*
 * @return OK or ERROR
 *	if everything worked correctly or not
 *	
 * @author Nicolas Serrano
 */
STATUS player_print(Player* player, FILE *f);

#endif

