/** 
 * @brief It defines TAD space
 * 
 * @file space.h
 * @author Profesores PPROG, Group 1
 * @version 3.0
 * @date 03-04-2018
 * @copyright GNU Public License
 */

#ifndef SPACE_H
#define SPACE_H

#include "../include/types.h"
#include "../include/set.h"
#include <stdio.h>

/**
 * @brief Space
 *
 * Struct for storing spaces
 */
typedef struct _Space Space;

/**
 * @brief Number of colums of space graphical description
 */
#define NCOLS 8

/**
 * @brief Number of rows of space graphical description
 */
#define NROWS 3

/** 
 * @brief Create space
 * 
 * Reserves memory for a Space initialising all the fields
 *
 * @param id Id of the space
 * @return Object* or NULL
 *	if everything worked correctly or not
 *	
 * @author Profesores PPROG, updated by Group 1
 */
Space* space_create(Id id);

/** 
 * @brief Destroy space
 * 
 * Frees all the memory reserved for space
 *
 * @param space Space* that will be freed
 * @return OK or ERROR
 *	if everything worked correctly or not
 *	
 * @author Profesores PPROG
 */
STATUS space_destroy(Space* space);

/** 
 * @brief Get space Id
 *
 * @param space Space*
 * @return Id or NO_ID
 *	if everything worked correctly or not
 *	
 * @author Profesores PPROG
 */
Id space_get_id(Space* space);

/** 
 * @brief Set space name
 * 
 * Sets the name of the space as the introduced char*
 *
 * @param space Space*
 * @param name char* with the new name of the space
 * @return OK or ERROR
 *	if everything worked correctly or not
 *	
 * @author Profesores PPROG
 */
STATUS space_set_name(Space* space, char* name);

/** 
 * @brief Get space name
 *
 * @param space Space*
 * @return char* with the name or NULL
 *	if everything worked correctly or not
 *	
 * @author Profesores PPROG
 */
const char* space_get_name(Space* space);

/** 
 * @brief Set space iluminated description
 * 
 * Sets the description of the space when it is iluminated, as the introduced char*
 *
 * @param space Space*
 * @param description char* with the new description of the space
 * @return OK or ERROR
 *	if everything worked correctly or not
 *	
 * @author Nicolas Serrano
 */
STATUS space_set_description(Space* space, char* description);

/** 
 * @brief Get space iluminated description
 *
 * @param space Space*
 * @return char* with the description or NULL
 *	if everything worked correctly or not
 *	
 * @author Nicolas Serrano
 */
char* space_get_description(Space* space);

/** 
 * @brief Set space uniluminated description
 * 
 * Sets the description of the space when it is not iluminated, as the introduced char*
 *
 * @param space Space*
 * @param description char* with the new description of the space
 * @return OK or ERROR
 *	if everything worked correctly or not
 *	
 * @author Nicolas Serrano
 */
STATUS space_set_descriptioff(Space* space, char* description);

/** 
 * @brief Get space uniluminated description
 *
 * @param space Space*
 * @return char* with the description or NULL
 *	if everything worked correctly or not
 *	
 * @author Nicolas Serrano
 */
char* space_get_descriptioff(Space* space);

/** 
 * @brief Set space graphical description
 * 
 * Sets the grapchical description of the space as the introduced chars*
 *
 * @param space Space*
 * @param f1 first row
 * @param f2 second row
 * @param f3 third row
 * @return OK or ERROR
 *	if everything worked correctly or not
 *	
 * @author Miguel Luque
 */
STATUS space_set_gdesc(Space* space, char f1[NCOLS], char f2[NCOLS], char f3[NCOLS]);

/** 
 * @brief Get space graphical description
 * 
 * Gets the row i of the grapchical description of space 
 *
 * @param space Space*
 * @param line integer with the number of the row we want
 * @return char* or NULL
 *	if everything worked correctly or not
 *	
 * @author Miguel Luque
 */
char* space_get_gdesc_line(Space* space, const int line);


/** 
 * @brief Set space north
 * 
 * Sets the Id of the link at north of space  
 *
 * @param space Space*
 * @param id Id 
 * @return OK or ERROR
 *	if everything worked correctly or not
 *	
 * @author Profesores PPROG
 */
STATUS space_set_north(Space* space, Id id);

/** 
 * @brief Get space north
 * 
 * Gets the Id of the link at north of space 
 *
 * @param space Space*
 * @return Id* or NO_ID
 *	if everything worked correctly or not
 *	
 * @author Profesores PPROG
 */
Id space_get_north(Space* space);

/** 
 * @brief Set space south
 * 
 * Sets the Id of the link at south of space  
 *
 * @param space Space*
 * @param id Id 
 * @return OK or ERROR
 *	if everything worked correctly or not
 *	
 * @author Profesores PPROG
 */
STATUS space_set_south(Space* space, Id id);

/** 
 * @brief Get space south
 * 
 * Gets the Id of the link at south of space 
 *
 * @param space Space*
 * @return Id* or NO_ID
 *	if everything worked correctly or not
 *	
 * @author Profesores PPROG
 */
Id space_get_south(Space* space);

/** 
 * @brief Set space east
 * 
 * Sets the Id of the link at east of space  
 *
 * @param space Space*
 * @param id Id 
 * @return OK or ERROR
 *	if everything worked correctly or not
 *	
 * @author Profesores PPROG
 */
STATUS space_set_east(Space* space, Id id);

/** 
 * @brief Get space east
 * 
 * Gets the Id of the link at east of space 
 *
 * @param space Space*
 * @return Id* or NO_ID
 *	if everything worked correctly or not
 *	
 * @author Profesores PPROG
 */
Id space_get_east(Space* space);

/** 
 * @brief Set space west
 * 
 * Sets the Id of the link at west of space  
 *
 * @param space Space*
 * @param id Id 
 * @return OK or ERROR
 *	if everything worked correctly or not
 *	
 * @author Profesores PPROG
 */
STATUS space_set_west(Space* space, Id id);

/** 
 * @brief Get space west
 * 
 * Gets the Id of the link at west of space 
 *
 * @param space Space*
 * @return Id* or NO_ID
 *	if everything worked correctly or not
 *	
 * @author Profesores PPROG
 */
Id space_get_west(Space* space);

/** 
 * @brief Set space up
 * 
 * Sets the Id of the link above space  
 *
 * @param space Space*
 * @param id Id 
 * @return OK or ERROR
 *	if everything worked correctly or not
 *	
 * @author Profesores PPROG
 */
STATUS space_set_up(Space* space, Id id);

/** 
 * @brief Get space up
 * 
 * Gets the Id of the link above space 
 *

 * @param space Space*
 * @return Id* or NO_ID
 *	if everything worked correctly or not
 *	
 * @author Nicolas Serrano
 */
Id space_get_up(Space* space);

/** 
 * @brief Set space down
 * 
 * Sets the Id of the link under space  
 *
 * @param space Space*
 * @param id Id 
 * @return OK or ERROR
 *	if everything worked correctly or not
 *	
 * @author Nicolas Serrano
 */
STATUS space_set_down(Space* space, Id id);

/** 
 * @brief Get space down
 * 
 * Gets the Id of the link under space 
 *
 * @param space Space*
 * @return Id* or NO_ID
 *	if everything worked correctly or not
 *	
 * @author Nicolas Serrano
 */
Id space_get_down(Space* space);

/** 
 * @brief Set space object
 * 
 * Set a new object inside space 
 *
 * @param space Space*
 * @param id Id of the object
 * @return OK or ERROR
 *	if everything worked correctly or not
 *	
 * @author Nicolas Serrano
 */
STATUS space_set_object(Space* space, Id id);

/** 
 * @brief Get space objects
 * 
 * Get all objects inside space 
 *
 * @param space Space*
 * @return Set* or NULL
 *	if everything worked correctly or not
 *	
 * @author Profesores PPROG, updated by Miguel Luque
 */
Set* space_get_objects(Space* space);

/** 
 * @brief Set space illumination
 * 
 * @param space Space*
 * @param illumination BOOL
 * @return OK or ERROR
 *	if everything worked correctly or not
 *	
 * @author Nicolas Serrano
 */
STATUS space_set_illumination(Space* space, BOOL illumination);

/** 
 * @brief Get space illumination 
 *
 * @param space Space*

 * @return BOOL or FALSE
 *	if everything worked correctly or not

 *	
 * @author Nicolas Serrano
 */
BOOL space_get_illumination(Space* space);

/**  
 * @brief Print space information in a file
 *
 * @param space Space*
 * @param f FILE*
 * @return OK or ERROR
 *	if everything worked correctly or not
 *	
 * @author Profesores PPROG, Nicolás Serrano
 */
STATUS space_print(Space* space, FILE *f);

#endif
