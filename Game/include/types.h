/**
 * @brief It defines common types
 *
 * @file types.h
 * @author Profesores PPROG Group 1
 * @version 2.0
 * @date 16-02-2018
 * @copyright GNU Public License
 */

#ifndef TYPES_H
#define TYPES_H

/**
 * @brief Defines the maximun size a char* can have
 */
#define WORD_SIZE 1000

/**
 * @brief Defines a value for an ID when there is no ID
 */
#define NO_ID -1

/**
 * @brief Defines a standar value to the ID
 */
#define STD_ID 1

/**
 * @brief Defines the maximun number of objects in the game
 */
#define MAX_OBJ 30

/**
 * @brief Defines the maximun number of spaces
 */
#define MAX_SPACES 130

/**
 * @brief Defines the maximun number of links
 */
#define MAX_LINKS 200

/**
 * @brief Defines the maximun number of Ids in Set*
 */
#define MAX_ID 50

/**
 * @brief Defines the type Id as a long
 */
typedef long Id;

/**
 * @brief Defines the type BOOL
 */
typedef enum {
    FALSE, TRUE
} BOOL;

/**
 * @brief Defines the type STATUS
 *
 * Give information of the functions when they return us an ERROR or an OK
 */
typedef enum {
    ERROR, OK
} STATUS;

/**
 * @brief Defines the type DIRECTION
 */
typedef enum {
    N, /*!< North */
    S, /*!< South */
    E, /*!< East */
    W /*!< West */
} DIRECTION;
#endif
