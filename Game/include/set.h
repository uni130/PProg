/** 
 * @brief It defines the TAD set
 * 
 * @file set.h
 * @author Group 1
 * @version 1.0 
 * @date 15-02-2018
 * @copyright GNU Public License
 */

#ifndef SET_H
#define SET_H

#include "../include/types.h"

/**
* @brief Set
*
* Struct for storing Ids
*/
typedef struct _Set Set;

/** 
* @brief Creates Set
* 
* Reserves memory for a Set
*
* @return Set* or NULL
*	if everything worked correctly or not
*	
* @author Miguel Luque
*/
Set* set_create();

/** 
* @brief Destroy Set
*
* Frees all the memory reserved for set
*
* @param set Set* that will be freed
* @return OK or ERROR
*	if everything worked correctly or not
*	
* @author Miguel Luque
*/
void set_destroy(Set* set);

/** 
* @brief Add id to set
*
* @param set Set*
* @param id Id
* @return OK or ERROR
*	if everything worked correctly or not
*	
* @author Miguel Luque
*/
STATUS set_add_value(Set* set, Id id);

/** 
* @brief Deletes id from set
*
* @param set Set*
* @param id Id
* @return OK or ERROR
*	if everything worked correctly or not
*	
* @author Miguel Luque
*/
STATUS set_del_value(Set* set, Id id);

/** 
* @brief Get id from set
* 
* Get the id of the object in posArray of Set
*
* @param set Set*
* @param posArray 
* @return Id or NO_ID
*	if everything worked correctly or not
*	
* @author Mario Lopez
*/
Id set_get_id(Set* s, int posArray);

/** 
* @brief Print Set information
*
* Prints all of the Ids in set 
*
* @param set Set*
* @return OK or ERROR
*	if everything worked correctly or not
*	
* @author Group 1
*/
STATUS set_print_values(Set* set);
#endif
