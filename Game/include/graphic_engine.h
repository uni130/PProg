/** 
 * @brief It defines a textual graphic engine
 * 
 * @file graphic_engine.h
 * @author Profesores PPROG
 * @version 1.0 
 * @date 18-01-2017
 * @copyright GNU Public License
 */

#ifndef __GRAPHIC_ENGINE__
#define __GRAPHIC_ENGINE__

#include "../include/game.h"
/**
 * @brief Graphic engine
 *
 * Struct for storing Graphic engine
 */

typedef struct _Graphic_engine Graphic_engine;

/** 
 * @brief Creates Graphic engine
 * 
 * Saves dinamic memory for the graphic engine and distrubutes the screen space around its components
 *
 * @return Graphic_engine* or NULL
 *	if everything worked correctly or not
 *	
@author Profesores PPROG*/
Graphic_engine* graphic_engine_create();

/** 
 * @brief Destroy Graphic engine
 *
 * Frees all the memory reserved for graphic engine and frees the screen space used for its components
 *
 * @param ge Graphic_engine* that will be freed
 * @return OK or ERROR
 *	if everything worked correctly or not
 *	
 * @author Profesores PPROG
 */
void graphic_engine_destroy(Graphic_engine *ge);

/** 
 * @brief Prints graphical interface
 *
 * Prints the map viewed by the user and prints the command and it STATUS in the log file if added
 * 
 * @param ge Graphic_engine*
 * @param game Game*
 * @param log FILE (optional)
 * 	
 * @author Profesores PPROG, Group 1*/
void graphic_engine_paint_game(Graphic_engine *ge, Game *game, FILE* log);

#endif
