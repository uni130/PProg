/** 
 * @brief It declares the tests for the object module
 * 
 * @file object_test.h
 * @author Mario Lopez
 * @version 1.0 
 * @date 06-04-2018
 * @copyright GNU Public License
 */

#ifndef OBJECT_TEST_H
#define OBJECT_TEST_H

#include "../include/object.h"
#include "../include/test.h"

/**
 * @test Test object creation
 * @pre object 
 * @post Non NULL pointer to object 
 */
void test1_object_create();

/**
 * @test Test object creation
 * @pre object with invalid argument 
 * @post NULL
 */
void test2_object_create();

/**
 * @test Test object destruction
 * @pre created object
 * @post OK
 */
void test1_object_destroy();

/**
 * @test Test object destruction
 * @pre object without memory
 * @post ERROR
 */
void test2_object_destroy();

/**
 * @test Test the introduction of a name in an object
 * @pre object and a name
 * @post OK
 */
void test1_object_set_name();

/**
 * @test Test the introduction of a name in an object
 * @pre object and a NULL
 * @post ERROR
 */
void test2_object_set_name();

/**
 * @test Test the introduction of the description of an object
 * @pre object and and char*
 * @post OK
 */
void test1_object_set_description();

/**
 * @test Test the introduction of the description of an object
 * @pre object and and NULL
 * @post ERROR
 */
void test2_object_set_description();

/**
 * @test Test the introduction of the location int in an object
 * @pre object and an int
 * @post OK
 */
void test1_object_set_location();

/**
 * @test Test the introduction of the location int in an object
 * @pre object and a NULL
 * @post ERROR
 */
void test2_object_set_location();

void test1_object_set_description2();

void test2_object_set_description2();

void test1_object_set_movable();

void test2_object_set_movable();

void test1_object_set_moved();

void test2_object_set_moved();

void test1_object_set_hidden();

void test2_object_set_hidden();

void test1_object_set_open();

void test2_object_set_open();

void test1_object_set_illuminate();

void test2_object_set_illuminate();

void test1_object_set_switchedOn();

void test2_object_set_switchedOn();

/**
 * @test Test the return of the id of an object
 * @pre object
 * @post the id of the object
 */
void test1_object_get_id();

/**
 * @test Test the return of the id of an object
 * @pre uninitialliced object
 * @post ERROR
 */
void test2_object_get_id();

/**
 * @test Test the return of the name of an object
 * @pre object
 * @post the name of the object
 */
void test1_object_get_name();

/**
 * @test Test the return of the name of an object
 * @pre uninitialliced object
 * @post ERROR
 */
void test2_object_get_name();

/**
 * @test Test the return of the location int from an object
 * @pre object
 * @post the location int
 */
void test1_object_get_location();

/**
 * @test Test the return of the location int from an object
 * @pre uninitialliced object
 * @post ERROR
 */
void test2_object_get_location();

/**
 * @test Test the return of the description of an object
 * @pre object
 * @post the description
 */
void test1_object_get_description();

/**
 * @test Test the return of the description of an object
 * @pre uninitialliced object
 * @post ERROR
 */
void test2_object_get_description();

void test1_object_get_description2();

void test2_object_get_description2();

void test1_object_get_movable();

void test2_object_get_movable();

void test1_object_get_moved();

void test2_object_get_moved();

void test1_object_get_hidden();

void test2_object_get_hidden();

void test1_object_get_open();

void test2_object_get_open();

void test1_object_get_illuminate();

void test2_object_get_illuminate();

void test1_object_get_switchedOn();

void test2_object_get_switchedOn();

#endif

