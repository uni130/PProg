/** 
 * @brief It declares the tests for the game_management module
 * 
 * @file game_management_test.h
 * @author Nicolás Serrano
 * @version 1.0
 * @date 19-04-2018
 * @copyright GNU Public License
 */

#ifndef GAME_MANAGEMENT_H
#define GAME_MANAGEMENT_H

#define MAX_TESTS 6

/**
 * @test Test the load player
 * @pre Game and valid filename
 * @post OK
 */
void test1_game_management_load_player();

/**
 * @test Test the load player
 * @pre NULL and valid filename
 * @post ERROR
 */
void test2_game_management_load_player();

/**
 * @test Test the save option
 * @pre Game and valid filename
 * @post OK
 */
void test1_game_management_save();

/**
 * @test test the save option
 * @pre  NULL and valid filename
 * @post ERROR
 */
void test2_game_management_save();

/**
 * @test Test the load option
 * @pre  Game and valid filename
 * @post OK
 */
void test1_game_management_load();

/**
 * @test Test the load option
 * @pre NULL and valid filename  
 * @post ERROR
 */
void test2_game_management_load();


#endif
