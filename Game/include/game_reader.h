/** 
 * @brief It defines the game reader
 * 
 * @file game_reader.h
 * @author Group 1
 * @version 3.0
 * @date 04-04-2018 
 * @copyright GNU Public License
 */

#ifndef GAME_READER_H
#define GAME_READER_H

#include "../include/game.h"

/** 
 * @brief Load Spaces from a file
 * 
 * Reads the textfile and interpretates the map for you to be able to move between the spaces
 *
 * @param game Game* where we load the spaces
 * @param filename name of the textfile
 * @return OK or ERROR
        if everything worked correctly or not
 *
 * @author Profesores PPROG 
 */
STATUS game_reader_load_spaces(Game* game, char* filename);

/** 
 * @brief Load Objects from a file
 * 
 * Reads the textfile and interpretates objects for you to be able to interact with them
 *
 * @param game Game* where we load the objects
 * @param filename name of the textfile
 * @return OK or ERROR
        if everything worked correctly or not
 *
 * @author Miguel Luque 
 */
STATUS game_reader_load_objects(Game* game, char* filename);

/*It receives a Game* and char*, 
reads the textfile and interpretates the map to
put the links on it ERROR if there was a problem	

@author Miguel Luque*/
STATUS game_reader_load_links(Game* game, char* filename);
#endif
