/** 
 * @brief It implements the dialogue test logic
 * 
 * @file dialogue_test.h
 * @author Javier Sanmiguel
 * @version 1.0
 * @date 05-24-2018 
 * @copyright GNU Public License
 */

#ifndef DIALOGUE_TEST_H
#define DIALOGUE_TEST_H

#include "../include/dialogue.h"
#include "../include/command.h"
#include "../include/test.h"

/**
 * @test Test dialogue validation
 * @pre Command != NULL
 * @post Char* != NULL
 */
void test1_dialogue_validation();

/**
 * @test Test dialogue validation
 * @pre Command == NULL
 * @post Char* == NULL 
 */
void test2_dialogue_validation();

/**
 * @test Test dialogue error validation 
 * @pre Command != NULL
 * @post Char* != NULL
 */
void test1_dialogue_error_validation();

/**
 * @test Test dialogue error validation
 * @pre Command == NULL
 * @post Char* == NULL 
*/
void test2_dialogue_error_validation();

/**
 * @test Test dialogue repeat
 * @post Char* != NULL 
*/
void test1_dialogue_repeat();



#endif
