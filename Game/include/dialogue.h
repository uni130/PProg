/** 
 * @brief It implements the dialogue module
 * 
 * @file dialogue.h
 * @author Javier Sanmiguel
 * @version 1.0
 * @date 17-04-2018 
 * @copyright GNU Public License
 */

#include <stdio.h>
#include <stdlib.h>
#include "../include/command.h"

/** 
 * @brief Validates message of Dialogue
 * 
 * returns a random string if the cmd was a correct one
 * 
 * @param Command* command used
 * @return Char* or NULL
 *	if everything worked correctly or not
 *
 * @author Javier Sanmiguel 
 */
char* dialogue_validation(Command*);

/** 
 * @brief Error message of Dialogue
 * 
 * returns a random string if the cmd was an error one
 *
 * @param Command* command used
 * @return Char* or NULL
 *	if everything worked correctly or not
 *
 * @author Nicolas Serrano
 */
char* dialogue_error(Command* cmd);


/** 
 * @brief Error message of Dialogue when repeated
 * 
 * returns a random string if the cmd was an error one and it repeats
 *
 * @return Char* or NULL
 *	if everything worked correctly or not
 *
 * @author Nicolas Serrano
 */
char *dialogue_repeat();
