/** 
 * @brief It declares the tests for the game module
 * 
 * @file game_test.h
 * @author Javier Sanmiguel
 * @version 1.0 
 * @date 6-04-2018
 * @copyright GNU Public License
 */

#ifndef GAME_TEST_H
#define GAME_TEST_H

#include "../include/command.h"
#include "../include/space.h"
#include "../include/player.h"
#include "../include/object.h"
#include "../include/die.h"
#include "../include/link.h"
#include "../include/game.h"
#include "../include/test.h"

/**
 * @test Test game creation
 * @pre GAME
 * @post Non NULL pointer to game 
 */
void test1_game_create();

/**
 * @test Test game add space
 * @pre SPACE id
 * @post !NO_ID space 
 */
void test1_game_add_space();
/**
 * @test Test game add space
 * @pre NULL SPACE
 * @post ERROR space 
 */
void test2_game_add_space();
/**
 * @test Test game add link
 * @pre LINK
 * @post OK
 */
void test1_game_add_link();
/**
 * @test Test game add link
 * @pre NULL LINK
 * @post ERROR
 */
void test2_game_add_link();
/**
 * @test Test game add object
 * @pre OBJECT ID
 * @post OK
 */
void test1_game_add_object();
/**
 * @test Test game add object
 * @pre NULL OBJECT
 * @post ERROR
 */
void test2_game_add_object();
/**
 * @test Test game is over
 * @pre GAME
 * @post FALSE
 */
void test1_game_is_over();
/**
 * @test Test game add object
 * @pre NULL GAME
 * @post TRUE
 */
void test2_game_is_over();
/**
 * @test Test game get space
 * @pre SPACE ID
 * @post !0
 */
void test1_game_get_space();
/**
 * @test Test game get space
 * @pre NO_ID SPACE
 * @post ERROR
 */
void test2_game_get_space();
/**
 * @test Test game get object
 * @pre OBJECT ID
 * @post !NULL
 */
void test1_game_get_object();
/**
 * @test Test game get object
 * @pre NO_ID OBJECT
 * @post NULL
 */
void test2_game_get_object();
/**
 * @test Test game get player
 * @pre GAME
 * @post !NULL
 */
void test1_game_get_player();
/**
 * @test Test game get player
 * @pre NULL GAME
 * @post NULL
 */
void test2_game_get_player();
/**
 * @test Test game get link
 * @pre LINK ID
 * @post !NULL
 */
void test1_game_get_link();
/**
 * @test Test game get link
 * @pre NO_ID LINK
 * @post NULL
 */
void test2_game_get_link();
/**
 * @test Test game get die
 * @pre DIE ID
 * @post !NULL
 */
void test1_game_get_die();
/**
 * @test Test game get die
 * @pre NO_ID DIE
 * @post NULL
 */
void test2_game_get_die();
/**
 * @test Test game callback status
 * @pre GAME
 * @post ERROR
 */
void test1_game_get_callback_status();
/**
 * @test Test game callback status
 * @pre NULL GAME
 * @post ERROR
 */
void test2_game_get_callback_status();
/**
 * @test Test game get player location
 * @pre GAME
 * @post NO_ID
 */
void test1_game_get_player_location();
/**
 * @test Test game get player location
 * @pre NULL GAME
 * @post NO_ID
 */
void test2_game_get_player_location();
/**
 * @test Test game get object location
 * @pre OBJECT
 * @post NO_ID
 */
void test1_game_get_object_location();
/**
 * @test Test game get player location
 * @pre NULL OBJECT
 * @post NO_ID
 */
void test2_game_get_object_location();
/**
 * @test Test game get description
 * @pre GAME
 * @post !NULL
 */
void test1_game_get_description();
/**
 * @test Test game get description
 * @pre NULL GAME
 * @post NULL
 */
void test2_game_get_description();
/**
 * @test Test game get last command
 * @pre GAME
 * @post NULL
 */
void test1_game_get_last_command();
/**
 * @test Test game callback status
 * @pre GAME
 * @post ERROR
 */
void test1_game_get_preavious_status();
/**
 * @test Test game callback status
 * @pre NULL GAME
 * @post ERROR
 */
void test2_game_get_preavious_status();
#endif
