/** 
 * @brief It implements the command interpreter
 * 
 * @file command.h
 * @author Profesores PPROG, Group 1
 * @version 3.0
 * @date 04-04-2018 
 * @copyright GNU Public License
 */

#ifndef COMMAND_H
#define COMMAND_H

#include "../include/types.h"
/*Defines the maximun number of comands at 17*/
#define N_CMD 17
/**
 * @brief Comands that can be used.
 */
typedef enum enum_Command {
    NO_CMD = -1, /*!< No command */
    UNKNOWN, /*!< Unknown command */
    EXIT, /*!< Exit */
    SKIP, /*!< Skip */
    PREVIOUS, /*!< Previous */
    TAKE, /*!< Grasp */
    DROP, /*!< Drop */
    ROLL, /*!< Roll */
    LEFT, /*!< Left */
    RIGHT, /*!< Right */
    MOVE, /*!< Move */
    CHECK, /*!< Check */
    SAVE, /*!< Save */
    LOAD, /*!< Load */
    USE, /*!< Use */
    TURNON,/*!< Turnon */
    TURNOFF/*!< Turnoff */
} T_Command;

/**
 * @brief Command
 *
 * Struct for storing Command
 */
typedef struct _Command Command;


/** 
 * @brief Creates Command
 * 
 * Reserves memory for a Command initialising all the fields
 *
 * @return Command* or NULL
 *	if everything worked correctly or not
 *
 * @author Nicolás Serrano 
 */
Command* command_ini();

/** 
 * @brief Destroy Command
 * 
 * Frees all the memory reserved for cmd
 *
 * @param cmd Command* that will be freed
 *
 * @author Nicolás Serrano 
 */
void command_destroy(Command* cmd);

/** 
 * @brief Get user imput
 * 
 * @param cmd Command* where we save the last command introduced 
 * @return OK or ERROR,
 *	if everything worked correctly or not
 *
 * @author: Profesores PPROG, uptaded by Nicolás Serrano 
 */
STATUS command_get_user_input(Command* cmd);

/** 
 * @brief Get command alredy saved
 * 
 * @param cmd Command* where we take the last command introduced 
 * @return The corresponding T_Command saved or NO_CMD
 *	if everything worked correctly or not
 *
 * @author: Nicolás Serrano 
 */
T_Command command_get_cmd(Command* cmd);

/** 
 * @brief Get object alredy saved
 * 
 * @param cmd Command* where we save the object introduced 
 * @return char* with the object name saved or NULL,
 *	if there was an object saved or not
 *
 * @author: Nicolás Serrano 
 */
char* command_get_object(Command* cmd);

/** 
 * @brief Get object alredy saved
 * 
 * @param cmd Command* where we save the second command introduced 
 * @return char* with the command name saved or NULL,
 *	if there was command wrote or not
 *
 * @author: Miguel Luque 
 */
char* command_get_parameter2(Command* cmd);


/** 
 * @brief Copy Command*
 * 
 * @param cmd Command* we copy 
 * @return Command* or NULL,
 *	if the copy was done correctly or not
 *
 * @author: Nicolás Serrano 
 */
Command* command_copy(const Command *cmd);

/** 
 * @brief Compares Command* with the last_cmd
 * 
 * @param command Command* we want to compare 
 * @return FALSE or BOOL,
 *	if was done correctly or not
 *
 * @author: Javier Sanmiguel 
 */
BOOL command_compare_to_last(Command*command);
#endif
