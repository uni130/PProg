/** 
 * @brief It implements the game_rules test logic
 * 
 * @file game_rules_test.h
 * @author Miguel Luque
 * @version 1.0
 * @date 03-05-2018
 * @copyright GNU Public License
 */

#ifndef GAME_RULES_TEST_H
#define GAME_RULES_TEST_H
#include "../include/game_rules.h"
#include "../include/test.h"

/**
 * @test Test rule creation 
 * @post Rule != NULL 
 */
void test1_game_rules_ini();

/**
 * @test Test function for getting rule->rule
 * @pre Initialized Rule
 * @post T_Rule == N_CMD
 */
void test1_game_rules_get();

/**
 * @test Test function for getting rule->rule
 * @pre Uninitialized Rule
 * @post T_Rule == N_CMD
 */
void test2_game_rules_get();

/**
 * @test Test function for copy rule
 * @pre Initialized Rule
 * @post Copy != NULL 
 */
void test1_game_rules_copy();

/**
 * @test Test function for copy rule
 * @pre NULL
 * @post Copy == NULL 
 */
void test2_game_rules_copy();

/**
 * @test Test function for setting rule->rule
 * @pre Initialized Rule
 * @post return value == OK
 */
void test1_game_rules_set();

/**
 * @test Test function for setting rule->rule
 * @pre NULL
 * @post return value == ERROR
 */
void test2_game_rules_set();

#endif
