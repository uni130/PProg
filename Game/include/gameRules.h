#ifndef GAMERULES_H
#define GAMERULES_H

#include "../include/types.h"
#include "../include/command.h"
#define N_POS 2
typedef enum enum_Rule {
    NO_RULE = N_CMD, /*!< No rule */
    TELEPORT /*!< Teleport */
} T_Rule;
/**
 * @brief rule
 *
 * Struct for storing rule
 */
typedef struct _Rule Rule;


/** 
 * @brief Creates Rule
 * 
 * Reserves memory for a Rule initialising all the fields
 *
 * @return Rule* or NULL
 *	if everything worked correctly or not
 *
 * @author Miguel Luque 
 */
Rule* rule_ini();

/** 
 * @brief Destroy Rule
 * 
 * Frees all the memory reserved for cmd
 *
 * @param cmd Rule* that will be freed
 *
 * @author Miguel Luque
 */
void rule_destroy(Rule* rule);

/** 
 * @brief Get user imput
 * 
 * @param cmd Rule* where we save the last rule introduced 
 * @return OK or ERROR,
 *	if everything worked correctly or not
 *
 * @author: Miguel Luque
 */
STATUS rule_set_rule(Rule* cmd,int i);

/** 
 * @brief Get rule alredy saved
 * 
 * @param cmd Rule* where we take the last rule introduced and an integer
 * @return The corresponding T_Rule saved or N_CMD
 *	if everything worked correctly or not
 *
 * @author: Miguel Luque
 */
T_Rule get_rule(Rule* rule);


/** 
 * @brief Copy Rule*
 * 
 * @param cmd Rule* we copy 
 * @return Rule* or NULL,
 *	if the copy was done correctly or not
 *
 * @author: Nicolás Serrano 
 */
Rule* rule_copy(const Rule *rule);

#endif
